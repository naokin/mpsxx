//
/// \file TSubArray.h
/// \brief Dense sub-array class and its copy semantics

#ifndef __BTAS_DENSE_TARRAY_H
#include <btas/DENSE/TArray.h>
#endif

#ifndef __BTAS_CXX11_TSUBARRAY_H
#define __BTAS_CXX11_TSUBARRAY_H

#include <functional>
#include <numeric>

#include <blas/wrappers.h>

namespace btas {

//####################################################################################################
// Sub-array class for dense array
//####################################################################################################

/// Sub-array of dense array TArray
/// Expression template providing copy semantics between TArray and TSubArray
/// \param T value type
/// \param N array rank
template<typename T, size_t N>
class TSubArray : protected TArray<T,N> {

  typedef TArray<T,N> base_;

public:

  // Typedefs

  using typename base_::value_type;
  using typename base_::extent_type;
  using typename base_::stride_type;
  using typename base_::shape_type;
  using typename base_::index_type;

private:

  /// Any TArray classes being friend of TSubArray<T,N>
  template<typename U, size_t M> friend class TArray;

  /// deleted default constructor
  TSubArray () = delete;

public:

  /// Constructor, from dense array object and lower/upper boundary indices
  TSubArray (const TArray<T,N>& a, const index_type& lb, const index_type& ub)
  : lower_bound_(lb), upper_bound_(ub) { this->reference(a); }

  /// Copy from array to sub-array
  template<size_t M>
  void copy (const TArray<T,M>& a)
  {
    // Calc. sub-array shape
    extent_type ext;
    for(size_t i = 0; i < N; ++i)
      ext[i] = upper_bound_[i]-lower_bound_[i]+1;

    size_t n = std::accumulate(ext.begin(),ext.end(),1ul,std::multiplies<size_t>());
    assert(a.size() == n);

    // If 0-dim. array
    if(n == 0) return;

    // Striding
    const auto& str = this->stride();
    size_t lda = ext[N-1];

    // Get bare pointers
    const T* a_ptr = a.data();
          T* t_ptr = this->data();

    // Copying elements
    index_type index(lower_bound_);
    size_t nrows = n/lda;
    for(size_t j = 0; j < nrows; ++j, a_ptr += lda) {
      size_t offset = dot(str,index);
      btas::copy(lda,a_ptr,1,t_ptr+offset,1);

      if(N > 1) {
        size_t i = N-2;
        for(; i > 0; --i) {
          if(++index[i] <= upper_bound_[i]) break;
          index[i] = lower_bound_[i];
        }
        if(i == 0) ++index[0];
      }
    }
  }

  /// Copy assignment operator from TArray
  template<size_t M>
  void operator= (const TArray<T,M>& x) { this->copy(x); }

private:
  /// lower boundary indices
  index_type
    lower_bound_;
  /// upper boundary indices
  index_type
    upper_bound_;
};

}; // namespace btas

#endif // __BTAS_CXX11_TSUBARRAY_H
