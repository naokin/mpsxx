#ifndef __BTAS_DENSE_TARRAY_H
#define __BTAS_DENSE_TARRAY_H 1

#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <numeric>
#include <memory>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/complex.hpp>

#include <btas/common/TVector.h>
#include <btas/DENSE/BLAS_STL_vector.h>

namespace btas {

//####################################################################################################
// Forward declaration of TSubArray
//####################################################################################################

template<typename T, size_t N> class TArray;
template<typename T, size_t N> class TSubArray;

//template<typename T, size_t M, size_t N>
//void copy_subarray(const TSubArray<T, M>&, TArray<T, N>&);

//template<typename T, size_t M, size_t N>
//void copy_subarray(const TArray<T, M>&, TSubArray<T, N>&);

/*! \class TArray
 *  \brief Dense array class
 *
 *  Fixed-rank array class implemented in terms of std::array and std::vector
 *  Since using C++11 features, not compatible for C++03 compiler
 *
 *  \param T value type
 *  \param N array rank
 */

template<typename T, size_t N>
class TArray {

public:

  // Typedefs

  /// value type
  typedef T value_type;

  /// storage type
  typedef std::vector<T> storage_type;

  /// iterator for elements
  typedef typename storage_type::iterator iterator;

  /// iterator for elements with const qualifier
  typedef typename storage_type::const_iterator const_iterator;

  typedef TVector<size_t,N> extent_type;

  typedef TVector<size_t,N> stride_type;

  typedef TVector<size_t,N> shape_type;

  typedef TVector<size_t,N> index_type;

private:

  friend class boost::serialization::access;

  /// Any TSubArray classes being friend of TArray<T, N>
  template<typename U, size_t M>
  friend class TSubArray;

  /// Any TArray classes being friend of TArray<T, N>
  template<typename U, size_t M>
  friend class TArray;

  /// Save object via boost::serialization
  template<class Archive>
  void save (Archive& ar, const unsigned int version) const
  {
    ar & extent_;
    ar & stride_;
    ar & store_->size();
    if(store_) {
      for(auto it = store_->begin(); it != store_->end(); ++it) ar & *it;
    }
  }

  /// Load object via boost::serialization
  template<class Archive>
  void load (Archive& ar, const unsigned int version)
  {
    ar & extent_;
    ar & stride_;
    size_t n;
    ar & n;
    if(n > 0) {
      store_.reset(new storage_type(n));
      for(auto it = store_->begin(); it != store_->end(); ++it) ar & *it;
    }
  }

  BOOST_SERIALIZATION_SPLIT_MEMBER ()

public:

//####################################################################################################
// Member Functions
//####################################################################################################

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Constructor, Destructor, and Assignment operators
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  /// default constructor
  TArray() : store_(new storage_type())
  {
    std::fill(extent_.begin(),extent_.end(),0);
    std::fill(stride_.begin(),stride_.end(),0);
  }

  /// destructor
 ~TArray() { }

  /// copy constructor
  TArray(const TArray& x) : store_(new storage_type()) { this->copy(x); }

  /// copy assignment operator
  TArray& operator= (const TArray& x) { this->copy(x); return *this; }

  /// addition assignment operator
  TArray& operator+= (const TArray& x) { this->add(x); return *this; }

  /// take a deep copy of x
  void copy (const TArray& x)
  {
    extent_ = x.extent_;
    stride_ = x.stride_;
    Copy(*x.store_,*store_);
  }

  /// return deep copy of this
  TArray copy () const
  {
    TArray x;
    x.copy(*this);
    return std::move(x);
  }

  //! Copy from sub-array to array
  template<size_t M>
  void copy (const TSubArray<T,M>& x)
  {
    // Calc. sub-array shape
    typename TSubArray<T,M>::extent_type x_ext;
    for(size_t i = 0; i < M; ++i)
      x_ext[i] = x.upper_bound_[i]-x.lower_bound_[i]+1;

    size_t x_size = std::accumulate(x_ext.begin(),x_ext.end(),1ul,std::multiplies<size_t>());
    assert(store_->size() == x_size);

    // If 0-dim. array
    if(x_size == 0) return;

    // Striding
    const auto& x_str = x.stride();
    size_t ldt = x_ext[M-1];

    // Get bare pointers
          T* t_ptr = store_->data();
    const T* x_ptr = x.data();

    // Copying elements
    auto index = x.lower_bound_;
    size_t nrows = x_size/ldt;
    for(size_t j = 0; j < nrows; ++j, t_ptr += ldt) {
      size_t offset = dot(x_str,index);
      btas::copy(ldt,x_ptr+offset,1,t_ptr,1);
      if(M > 1) {
        size_t i = M-2;
        for(; i > 0; --i) {
          if(++index[i] <= x.upper_bound_[i]) break;
          index[i] = x.lower_bound_[i];
        }
        if(i == 0) ++index[0];
      }
    }
  }

  /// scale by const value
  void scale (const T& alpha)
  {
    Scal(alpha,*store_);
  }

  /// adding  from x to this
  void add (const TArray& x)
  {
    assert(extent_ == x.extent_);
    assert(stride_ == x.stride_);
    Axpy(static_cast<T>(1),*x.store_,*store_);
  }

  /// copy constructor from sub-array
  template<size_t M>
  explicit
  TArray (const TSubArray<T,M>& x) { this->copy(x); }

  /// copy assignment from sub-array
  template<size_t M>
  TArray& operator= (const TSubArray<T,M>& x) { this->copy(x); return *this; }

  /// move constructor
  explicit
  TArray (TArray&& x)
  : extent_(std::move(x.extent_)),
    stride_(std::move(x.stride_)),
    store_ (std::move(x.store_ ))
  { x.store_.reset(); }

  /// move assignment
  TArray& operator= (TArray&& x) { this->swap(x); return *this; }

  /// take data reference from x
  void reference (const TArray& x)
  {
    extent_ = x.extent_;
    stride_ = x.stride_;
    store_  = x.store_ ;
  }

  /// return data reference of this
  TArray reference() const
  {
    TArray x;
    x.reference(*this);
    return std::move(x);
  }

  /// convenient constructor with array shape, for N = 1
  template<typename... Args>
  explicit
  TArray (Args... args) : store_(new storage_type()) { this->resize(args...); }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Resizing functions
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

private:

  void construct ()
  {
    // calculate stride
    size_t str = 1;
    for(size_t i = N-1; i > 0; --i) {
      stride_[i] = str;
      str *= extent_[i];
    }
    stride_[0] = str;
    // allocate memory
    store_->resize(extent_[0]*str);
  }

  void construct (const T& value)
  {
    this->construct();
    // fill elements
    this->fill(value);
  }

  template<class Generator>
  void construct (Generator gen)
  {
    this->construct();
    // fill elements
    this->generate(gen);
  }

  template<size_t I, typename... Args>
  void resize_helper_ (const size_t& n, Args... args)
  { extent_[I-1] = n; resize_helper_<I+1>(args...); }

  template<size_t I>
  void resize_helper_ (const size_t& n)
  { static_assert(I == N, "rank mismatched upon calling TArray::resize."); extent_[I-1] = n; this->construct(); }

  template<size_t I, typename... Args>
  void index_helper_ (index_type& idx, const typename index_type::value_type& n, Args... args) const
  { idx[I-1] = n; index_helper_<I+1>(idx,args...); }

  template<size_t I>
  void index_helper_ (index_type& idx, const typename index_type::value_type& n) const
  { static_assert(I == N, "rank mismatched upon calling TArray::operator() or TArray::at()."); idx[I-1] = n; }

public:

  /// resize by arguments
  template<typename... Args>
  void resize (const size_t& n, Args... args) { resize_helper_<1>(n,args...); }

  /// resize by extent
  void resize (const extent_type& ext) { extent_ = ext; this->construct(); }

  /// resize by extent
  void resize (const extent_type& ext, const T& value) { extent_ = ext; this->construct(value); }

  /// resize by extent
  template<class Generator>
  void resize (const extent_type& ext, Generator gen) { extent_ = ext; this->construct(gen); }

  /// return shared reference reshaped
  /// (1) implaced reshape can be done as
  /// `y.swap(x.reshape(shape(n1,n2,...)));`
  ///
  /// (2) take a deep copy can be done as
  /// `y.copy(x.reshape(shape(n1,n2,...)));`
  template<size_t M>
  TArray<T,M> reshape (const typename TArray<T,M>::extent_type& ext)
  {
    TArray<T,M> x(ext);
    assert(x.size() == this->size());
    x.store_ = this->store_; // shallow copy
    return x;
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // Data Accessing
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  /// returns first iterator position (const)
  const_iterator begin() const
  { return store_->begin(); }

  /// returns first iterator position
  iterator begin()
  { return store_->begin(); }

  /// returns last iterator position (const)
  const_iterator end() const
  { return store_->end(); }

  /// returns last iterator position
  iterator end()
  { return store_->end(); }

  /// returns array extent
  const extent_type& extent () const
  { return extent_; }

  /// returns array extent for rank i
  const typename extent_type::value_type& extent (size_t i) const
  { return extent_[i]; }

  /// returns array shape (backward compatibility)
  const shape_type& shape () const
  { return extent_; }

  /// returns array shape for rank i (backward compatibility)
  const typename shape_type::value_type& shape (size_t i) const
  { return extent_[i]; }

  /// returns array stride
  const stride_type& stride() const
  { return stride_; }

  /// returns array stride for rank i
  const typename stride_type::value_type& stride (size_t i) const
  { return stride_[i]; }

  /// returns allocated size
  size_t size () const { return store_->size(); }

  /// is empty?
  bool empty () const { return (this->size() == 0); }

  /// returns array element without range check
  template<typename... Args>
  const value_type& operator() (const Args&... args) const
  {
    index_type i; index_helper_<1>(i,args...);
    return (*this)(i);
  }

  /// returns array element (arbitrary N) without range check
  const value_type& operator() (const index_type& i) const
  { return (*store_)[dot(i,stride_)]; }

  /// returns array element without range check
  template<typename... Args>
  value_type& operator() (const Args&... args)
  {
    index_type i; index_helper_<1>(i,args...);
    return (*this)(i);
  }

  /// returns array element (arbitrary N) without range check
  value_type& operator() (const index_type& i)
  { return (*store_)[dot(i,stride_)]; }

  /// returns array element with range check
  template<typename... Args>
  const value_type& at (const Args&... args) const
  {
    index_type i; index_helper_<1>(i,args...);
    return this->at(i);
  }

  /// returns array element with range check
  const value_type& at (const index_type& i) const
  { return store_->at(dot(i,stride_)); }

  /// returns array element with range check
  template<typename... Args>
  value_type& at (const Args&... args)
  {
    index_type i; index_helper_<1>(i,args...);
    return this->at(i);
  }

  /// returns array element with range check
  value_type& at (const index_type& i)
  { return store_->at(dot(i,stride_)); }

  /// slice array to return sub-array object
  /// sub-array is constructed from array elements [lb[0]:ub[0], lb[1]:ub[1], ...] */
  TSubArray<T,N> subarray (const index_type& lb, const index_type& ub) const
  { return TSubArray<T,N>(*this,lb,ub); }

  /// returns the first pointer of array elements
  const value_type* data() const
  { return store_->data(); }

  /// returns the first pointer of array elements
  value_type* data()
  { return store_->data(); }

  /// fills elements by constant value
  void fill (const T& value)
  { std::fill(store_->begin(),store_->end(),value); }

  /// fills elements by constant value
  void operator= (const T& value)
  { this->fill(value); }

  /// generates array elements by function gen
  /// Generator is either default constructible class or function pointer, which can be called by gen() */
  template<class Generator>
  void generate (Generator gen)
  { std::generate(store_->begin(),store_->end(),gen); }

  /// clear element
  /// NOTE that std::vector<T>::clear doesn't deallocate memory space
  void clear ()
  {
    std::fill(extent_.begin(),extent_.end(),0);
    std::fill(stride_.begin(),stride_.end(),0);
    store_->clear();
  }

  /// swap object
  /// `x.swap(TArray<T,N>())` works to deallocate memory space
  void swap (TArray&& x)
  {
    std::swap(this->extent_,x.extent_);
    std::swap(this->stride_,x.stride_);
    this->store_.swap(x.store_);
  }

  /// returns number of shared objects
  size_t use_count () const
  { return store_.use_count(); }

private:

  //####################################################################################################
  // Member Variables
  //####################################################################################################

  /// array shape
  extent_type extent_;

  /// array stride
  stride_type stride_;

  /// array storage
  std::shared_ptr<storage_type> store_;

}; // class TArray

template<typename T, size_t N>
struct TArrayOut
{
  static std::ostream& FormattedOut(std::ostream& ost, const TArray<T,N>& x)
  {
    using std::setw;
    using std::endl;

    // detect ostream status for floating point value
    size_t width = ost.precision()+4;
    if(ost.flags() & std::ios::scientific)
      width += 4;
    else
      ost.setf(std::ios::fixed,std::ios::floatfield);

    // printing array extent
    const auto& x_ext = x.extent();
    ost << "extent [ "; for(size_t i = 0; i < N-1; ++i) ost << x_ext[i] << " x "; ost << x_ext[N-1] << " ] " << endl;
    ost << "----------------------------------------------------------------------------------------------------" << endl;

    // printing array elements
    size_t stride = x.extent(N-1);
    size_t n = 0;
    for(auto it = x.begin(); it != x.end(); ++it, ++n)
    {
      if(n % stride == 0) ost << endl << "\t";

      ost << setw(width) << *it;
    }
    ost << endl;

    return ost;
  }
};

template<typename T, size_t N>
struct TArrayOut<std::complex<T>,N>
{
  static std::ostream& FormattedOut(std::ostream& ost, const TArray<std::complex<T>,N>& x)
  {
    using std::setw;
    using std::endl;

    // detect ostream status for floating point value
    size_t width = ost.precision()+4;
    if(ost.flags() & std::ios::scientific)
      width += 4;
    else
      ost.setf(std::ios::fixed,std::ios::floatfield);

    // printing array extent
    const auto& x_ext = x.extent();
    ost << "extent [ "; for(size_t i = 0; i < N-1; ++i) ost << x_ext[i] << " x "; ost << x_ext[N-1] << " ] " << endl;
    ost << "----------------------------------------------------------------------------------------------------" << endl;

    // printing array elements
    size_t stride = x.extent(N-1);
    size_t n = 0;
    for(auto it = x.begin(); it != x.end(); ++it, ++n)
    {
      if(n % stride == 0) ost << endl << "\t";

      if(it->imag() < 0)
        ost << setw(width) << it->real() << " - " << setw(width) << fabs(it->imag()) << "i";
      else
        ost << setw(width) << it->real() << " + " << setw(width) << fabs(it->imag()) << "i";
    }
    ost << endl;

    return ost;
  }
};

} // namespace btas

/// C++ style printing function
template<typename T, size_t N>
std::ostream& operator<< (std::ostream& ost, const btas::TArray<T,N>& x)
//{ return ost << btas::TArrayOut<T,N>::FormattedOut(ost,x); }
{ return btas::TArrayOut<T,N>::FormattedOut(ost,x); }

#include <btas/DENSE/TSubArray.h>

#include <btas/DENSE/TBLAS.h>
#include <btas/DENSE/TLAPACK.h>
#include <btas/DENSE/TREINDEX.h>
#include <btas/DENSE/TCONTRACT.h>

#include <btas/DENSE/TConj.h>

#endif // __BTAS_DENSE_TARRAY_H
