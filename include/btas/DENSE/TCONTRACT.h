#ifndef __BTAS_DENSE_TCONTRACT_H
#define __BTAS_DENSE_TCONTRACT_H

#include <btas/common/btas.h>
#include <btas/common/btas_contract_shape.h>

#include <btas/DENSE/TArray.h>
#include <btas/DENSE/TBLAS.h>
#include <btas/DENSE/TREINDEX.h>

namespace btas
{

/// Contract Arrays
template<typename T, typename Indx_t, size_t L, size_t M, size_t K>
void Contract (
  const T& alpha,
  const TArray<T,L>& a, const TVector<Indx_t,K>& contractA,
  const TArray<T,M>& b, const TVector<Indx_t,K>& contractB,
  const T& beta,
        TArray<T,L+M-K-K>& c)
{
  TVector<size_t,L> reorderA;
  TVector<size_t,M> reorderB;

  unsigned int jobs = get_contract_jobs(a.extent(),contractA,reorderA,b.extent(),contractB,reorderB);

  TArray<T,L> a_ref;

  if(jobs & JOBMASK_A_PMUTE)
    Permute(a,reorderA,a_ref);
  else
    a_ref.reference(a);

  CBLAS_TRANSPOSE transa;

  if(jobs & JOBMASK_A_TRANS)
    transa = CblasTrans;
  else
    transa = CblasNoTrans;

  TArray<T,M> b_ref;

  if(jobs & JOBMASK_B_PMUTE)
    Permute(b,reorderB,b_ref);
  else
    b_ref.reference(b);

  CBLAS_TRANSPOSE transb;

  if(jobs & JOBMASK_B_TRANS)
    transb = CblasTrans;
  else
    transb = CblasNoTrans;

  BlasContract(transa,transb,alpha,a_ref,b_ref,beta,c);
}

/// Contract Arrays by symbols
template<typename T, typename Symbol, size_t L, size_t M, size_t N>
void Contract (
  const T& alpha,
  const TArray<T,L>& a, const TVector<Symbol,L>& symbolA,
  const TArray<T,M>& b, const TVector<Symbol,M>& symbolB,
  const T& beta,
        TArray<T,N>& c, const TVector<Symbol,N>& symbolC)
{
  const size_t K = (L+M-N)/2;

  TVector<size_t,K> contractA;
  TVector<size_t,K> contractB;
  TVector<Symbol,N> symbolAxB;
  indexed_contract_shape(symbolA,contractA,symbolB,contractB,symbolAxB);

  if(symbolC == symbolAxB)
  {
    Contract(alpha,a,contractA,b,contractB,beta,c);
  }
  else
  {
    TArray<T,N> axb;

    if(c.size() > 0) Permute(c,symbolC,axb,symbolAxB);

    Contract(alpha,a,contractA,b,contractB,beta,axb);

    Permute(axb,symbolAxB,c,symbolC);
  }
}

} // namespace btas

#endif // __BTAS_DENSE_TCONTRACT_H
