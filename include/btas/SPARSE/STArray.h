#ifndef __BTAS_SPARSE_STARRAY_H
#define __BTAS_SPARSE_STARRAY_H

#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <algorithm>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>

#include <btas/common/btas.h>
#include <btas/common/TVector.h>

#include <btas/DENSE/TArray.h>

namespace btas {

// Binary comparison for an implementation of Map in terms of std::vector<std::pair<T,U>>

template<typename Index, typename T>
bool __pair_comp_less_l (const std::pair<Index,T>& x, const Index& i) { return x.first < i; }

template<typename Index, typename T>
bool __pair_comp_less_u (const Index& i, const std::pair<Index,T>& x) { return i < x.first; }

template<typename Index, typename T>
bool __pair_comp_pair_less (const std::pair<Index,T>& x, const std::pair<Index,T>& y) { return x.first < y.first; }

/// Block-sparse array class
template<typename T, size_t N>
class STArray {

private:

  typedef std::shared_ptr<TArray<T,N>> data_t;

public:

  typedef size_t ordinal_type;

  typedef T value_type;

  // Alias to storage type
  typedef std::vector<std::pair<ordinal_type,data_t>> storage_type;

  // Alias to iterator
  typedef typename storage_type::iterator iterator;

  // Alias to const iterator
  typedef typename storage_type::const_iterator const_iterator;

  typedef TVector<size_t,N> extent_type;

  typedef TVector<size_t,N> stride_type;

  typedef TVector<size_t,N> shape_type;

  typedef TVector<size_t,N> index_type;

private:

  // Boost serialization
  friend class boost::serialization::access;

  /// Save object via boost::serialization
  template<class Archive>
  void save (Archive& ar, const unsigned int version) const
  {
    ar & extent_;
    ar & m_dn_shape;
    ar & stride_;

    if(!m_order) this->mf_sort();

    ar & m_store.size();
    for(auto it = m_store.begin(); it != m_store.end(); ++it) {
      if(it->second)
        ar & it->first & *(it->second);
    }
  }

  /// Load object via boost::serialization
  template<class Archive>
  void load (Archive& ar, const unsigned int version)
  {
    ar & extent_;
    ar & m_dn_shape;
    ar & stride_;
    size_t n;
    ar & n;
    m_store.resize(n);
    for(auto it = m_store.begin(); it != m_store.end(); ++it) {
      it->second.reset(new TArray<T,N>());
      ar & it->first & *(it->second);
    }

    m_order = true; // this is because m_store is sorted before saving.
  }

  BOOST_SERIALIZATION_SPLIT_MEMBER ()

protected:

  //! Checking non-zero block
  /*! This should be overridden so that non-zero block can be determined from STArray class */
  virtual bool mf_check_allowed (const index_type& _index) const { return true; }

  // KEEP FOR INSERTION CHECK

  void mf_check_dshape (const index_type& _index, const extent_type& _shape)
  {
    extent_type _chk_shape = m_dn_shape & _index;
    for(size_t i = 0; i < N; ++i) {
      if(_chk_shape[i] == _shape[i]) continue;
      BTAS_THROW(_chk_shape[i] == 0, "btas::STArray::mf_check_dshape: requested shape is inconsistent");
      m_dn_shape[i][_index[i]] = _shape[i]; // update dense-block shape
    }
  }

  /// Force sorting storage.
  void mf_sort () const
  {
    std::sort(m_store.begin(),m_store.end(),__pair_comp_pair_less<ordinal_type,data_t>);
    m_order = true;
  }

public:

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Constructors
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  //! Default constructor
  STArray()
  : m_order(false)
  {
    std::fill(extent_.begin(),extent_.end(),0);
    std::fill(stride_.begin(),stride_.end(),0);
  }

  //! Destructor
  virtual ~STArray() { }

  //! Construct by sparse-block shape
  STArray(const extent_type& _shape) : m_order(false) { resize(_shape); }

  //! Construct by dense-block shapes
  STArray(const TVector<Dshapes, N>& _dn_shape, bool _allocate = true) : m_order(false) { resize(_dn_shape, _allocate); }

  //! Construct by dense-block shapes and fill elements by value
  STArray(const TVector<Dshapes, N>& _dn_shape, const T& value) : m_order(false) { resize(_dn_shape, value); }

  //! Construct by dense-block shapes and fill elements by gen()
  template<class Generator>
  STArray(const TVector<Dshapes, N>& _dn_shape, Generator gen) : m_order(false) { resize(_dn_shape, gen); }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Copy semantics
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  //! Copy constructor
  STArray(const STArray& other) { copy(other); }

  //! Copy assignment operator
  STArray& operator= (const STArray& other) { copy(other); return *this; }

  //! Take deep copy of other
  void copy(const STArray& other) {
    extent_    = other.extent_;
    m_dn_shape = other.m_dn_shape;
    stride_   = other.stride_;
    m_store.clear();
    m_store.reserve(other.m_store.size());
    for(const_iterator it = other.m_store.begin(); it != other.m_store.end(); ++it) {
      if(!it->second) continue; // remove NULL element upon copying
      m_store.push_back(std::make_pair(it->first,data_t(new TArray<T,N>(*(it->second)))));
    }
    if(!other.m_order) this->mf_sort();
  }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Move semantics
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  //! Move constructor
  STArray(STArray&& other) {
    extent_     = std::move(other.extent_);
    m_dn_shape  = std::move(other.m_dn_shape);
    stride_    = std::move(other.stride_);
    m_store     = std::move(other.m_store);
    m_order     = std::move(other.m_order);
  }

  //! Move assignment operator
  STArray& operator= (STArray&& other) {
    extent_     = std::move(other.extent_);
    m_dn_shape  = std::move(other.m_dn_shape);
    stride_    = std::move(other.stride_);
    m_store     = std::move(other.m_store);
    m_order     = std::move(other.m_order);
    return *this;
  }

  //! make reference to other
  /*! not complete reference, since elements in m_store are only shared.
   *  so, even if extent_ or stride_ is changed, it won't be affected.
   */
  void reference(const STArray& other) {
    extent_     = other.extent_;
    m_dn_shape  = other.m_dn_shape;
    stride_    = other.stride_;
    m_store     = other.m_store;
    m_order     = other.m_order;
  }

  //! Make subarray reference
  /*! \param _indxs contains subarray indices
   *  e.g.
   *  sparse shape = { 4, 4 }
   *  _indxs = { { 1, 3 }, { 0, 2, 3} }
   *
   *     0  1  2  3           0  2  3
   *    +--+--+--+--+        +--+--+--+
   *  0 |  |  |  |  |  ->  1 |**|**|**|
   *    +--+--+--+--+        +--+--+--+
   *  1 |**|  |**|**|      3 |**|**|**|
   *    +--+--+--+--+        +--+--+--+
   *  2 |  |  |  |  |
   *    +--+--+--+--+
   *  3 |**|  |**|**|
   *    +--+--+--+--+
   *
   *  ** blocks are only kept to make subarray
   */
  STArray subarray(const TVector<Dshapes, N>& sub_indxs) const {
    TVector<Dshapes, N> sub_indx_map;
    TVector<Dshapes, N> sub_dn_shape;
    for(size_t i = 0; i < N; ++i) {
      sub_indx_map[i].resize(extent_[i],0xffffffff);
      sub_dn_shape[i].reserve(sub_indxs[i].size());
      size_t n = 0;
      for(size_t j = 0; j < sub_indxs[i].size(); ++j) {
        sub_indx_map[i].at(sub_indxs[i][j]) = n++;
        sub_dn_shape[i].push_back(m_dn_shape[i].at(sub_indxs[i][j]));
      }
    }

    STArray subref_(sub_dn_shape, false);

    subref_.m_store.reserve(m_store.size());
    for(const_iterator it = m_store.begin(); it != m_store.end(); ++it) {
      index_type sub_index = sub_indx_map & index(it->first);
      bool kept = true;
      for(size_t i = 0; kept && i < N; ++i) {
        kept &= (sub_index[i] != 0xffffffff);
      }
      if(kept) subref_.m_store.push_back(std::make_pair(subref_.tag(sub_index),it->second));
    }
    subref_.m_order = m_order;

    return std::move(subref_);
  }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Resizing functions
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  //! Resize by sparse-block shape
  void resize(const extent_type& _shape) {
    extent_ = _shape;
    for(size_t i = 0; i < N; ++i) {
      m_dn_shape[i] = Dshapes(extent_[i], 0);
    }
    size_t stride = 1;
    for(size_t i = N-1; i > 0; --i) {
      stride_[i] = stride;
      stride *= extent_[i];
    }
    stride_[0] = stride;
    m_store.clear();
  }

  //! Resize by dense-block shapes using this->mf_check_allowed(index)
  void resize(const TVector<Dshapes, N>& _dn_shape, bool _allocate = true) {
    // calc. sparse-block shape
    extent_type _shape;
    for(size_t i = 0; i < N; ++i)
       _shape[i] = _dn_shape[i].size();

    resize(_shape);
    m_dn_shape = _dn_shape;

    if(_allocate) allocate();
  }

  //! Allocate all allowed blocks (existed blocks are collapsed)
  void allocate() {
    index_type index_; std::fill(index_.begin(),index_.end(),0);

    m_store.clear();
    for(size_t ib = 0; ib < size(); ++ib) {
      // assume derived mf_check_allowed being called
      if(this->mf_check_allowed(index_)) {
        if(m_dn_shape * index_ > 0) // check non-zero size
          m_store.push_back(std::make_pair(ib,data_t(new TArray<T,N>(m_dn_shape & index_))));
      }
      // index increment
      size_t id = N-1;
      for(; id > 0; --id) {
        if(++index_[id] < extent_[id]) break;
        index_[id] = 0;
      }
      if(id == 0) ++index_[0];
    }
    m_order = true;
  }

  //! Resize by dense-block shapes and fill all elements by value
  void resize(const TVector<Dshapes, N>& _dn_shape, const T& value) { resize(_dn_shape); fill(value); }

  //! Resize by dense-block shapes and fill all elements by gen()
  template<class Generator>
  void resize(const TVector<Dshapes, N>& _dn_shape, Generator gen) { resize(_dn_shape); generate(gen); }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Fill and Generage elements
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  //! fills all elements by value
  void fill(const T& value) {
    for(iterator it = m_store.begin(); it != m_store.end(); ++it) it->second->fill(value);
  }

  //! fills all elements by value
  void operator= (const T& value) { fill(value); }

  //! generates all elements by gen()
  template<class Generator>
  void generate(Generator gen) {
    for(iterator it = m_store.begin(); it != m_store.end(); ++it) it->second->generate(gen);
  }

////! generates all elements by gen()
//template<class Generator>
//void operator= (Generator gen) { generate(gen); } // this is ambiguous with copy assignment operaotr

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Clear and Erase sparse blocks
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  //! Erase certain block by index
  iterator erase (const index_type& index_)
  {
    return this->erase(this->tag(index_));
  }

  //! Erase certain block by tag
  iterator erase (const ordinal_type& tag_)
  {
    auto it = this->find(tag_);
    if(it != m_store.end())
      return m_store.erase(it);
    else
      return it;
  }

  //! Deallocation
  virtual void clear()
  {
    std::fill(extent_.begin(),extent_.end(),0);
    std::fill(stride_.begin(),stride_.end(),0);
    for(size_t i = 0; i < N; ++i) m_dn_shape[i].clear();
    m_store.clear();
    m_order = false;
  }

  //! Erase blocks of which have certain index
  /*!
   *  \param _rank  rank in which index is associated
   *  \param _index index to be removed
   */
  virtual void erase(size_t _rank, size_t _index_erase) {
    assert(_index_erase >= 0 && _index_erase < extent_[_rank]);
    extent_type _shape = extent_; --_shape[_rank];
    STArray redref_(_shape);
    redref_.m_store.reserve(m_store.size());
    for(iterator it = m_store.begin(); it != m_store.end(); ++it) {
      index_type _index = index(it->first);
      if(_index[_rank] == _index_erase) continue;
      if(_index[_rank] >  _index_erase) --_index[_rank];
      redref_.m_store.push_back(std::make_pair(redref_.tag(_index),it->second));
    }
    redref_.m_order = m_order;
    *this = std::move(redref_);
  } //				FIXME: This might not be good. Duplicated with subarray function

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Index <--> Tag conversion
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  //! convert tag to index
  index_type index(ordinal_type tag_) const {
    index_type _index;
    for(size_t i = 0; i < N; ++i) {
      _index[i] = tag_ / stride_[i];
      tag_      = tag_ % stride_[i];
    }
    return std::move(_index);
  }

  //! convert index to tag
  ordinal_type tag(const index_type& _index) const { return dot(_index, stride_); }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Access member variables
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  //! Returns sparse-block extent
  const extent_type& extent () const { return extent_; }

  //! Returns sparse-block extent for rank i
  const typename extent_type::value_type& extent (size_t i) const { return extent_[i]; }

  //! Returns sparse-block shape
  const extent_type& shape () const { return extent_; }

  //! Returns sparse-block shape for rank i
  const typename extent_type::value_type& shape (size_t i) const { return extent_[i]; }

  //! Returns sparse-block stride
  const stride_type& stride () const { return stride_; }

  //! Returns sparse-block stride for rank i
  const typename stride_type::value_type& stride (size_t i) const { return stride_[i]; }

  //! Returns number of non-zero sparse-blocks
  size_t nnz() const { return m_store.size(); }

  //! Returns total number of sparse-blocks (includes zero blocks)
  size_t size() const { return stride_[0]*extent_[0]; }

  //! Returns dense-block shapes
  const TVector<Dshapes, N>& dshape() const { return m_dn_shape; }

  //! Returns dense-block shapes for rank i
  const Dshapes& dshape(size_t i) const { return m_dn_shape[i]; }

  //! Check and update dense-block shapes
  const TVector<Dshapes, N>& check_dshape() {
    for(const_iterator it = m_store.begin(); it != m_store.end(); ++it)
      mf_check_dshape(index(it->first), it->second->shape());
    return m_dn_shape;
  }

  //! Calc. and return net dense-block shapes
  TVector<Dshapes, N> check_net_dshape() const {
    TVector<Dshapes, N> _net_dshape;
    for(size_t i = 0; i < N; ++i) _net_dshape[i].resize(extent_[i], 0);
    for(const_iterator it = m_store.begin(); it != m_store.end(); ++it) {
      index_type _index = index(it->first);
      for(size_t i = 0; i < N; ++i) {
        if(_net_dshape[i][_index[i]] > 0) {
          BTAS_THROW(_net_dshape[i][_index[i]] == it->second->shape(i), "btas::STArray::check_net_dshape: found mismatched dense-block shape");
        }
        else {
          _net_dshape[i][_index[i]] = it->second->shape(i);
        }
      }
    }
    return std::move(_net_dshape);
  }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Iterators: Definitions are related to std::map
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  const_iterator begin() const
  { if(!m_order) this->mf_sort(); return m_store.begin(); }

  iterator begin()
  { if(!m_order) this->mf_sort(); return m_store.begin(); }

  const_iterator end() const
  { if(!m_order) this->mf_sort(); return m_store.end(); }

  iterator end()
  { if(!m_order) this->mf_sort(); return m_store.end(); }

  const_iterator find(const index_type& _index) const
  { return m_store.find(tag(_index)); }

  iterator find(const index_type& _index)
  { return m_store.find(tag(_index)); }

  const_iterator lower_bound(const index_type& _index) const
  { return this->lower_bound(tag(_index)); }

  iterator lower_bound(const index_type& _index)
  { return this->lower_bound(tag(_index)); }

  const_iterator upper_bound(const index_type& _index) const
  { return this->upper_bound(tag(_index)); }

  iterator upper_bound(const index_type& _index)
  { return this->upper_bound(tag(_index)); }

  const_iterator find(const ordinal_type& tag_) const
  {
    if(!m_order) this->mf_sort();
    auto it = std::lower_bound(m_store.begin(),m_store.end(),tag_,__pair_comp_less_l<ordinal_type,data_t>);
    if(it != m_store.end() && !(tag_ < it->first)) // found
      return it;
    else
      return m_store.end();
  }

  iterator find(const ordinal_type& tag_)
  {
    if(!m_order) this->mf_sort();
    auto it = std::lower_bound(m_store.begin(),m_store.end(),tag_,__pair_comp_less_l<ordinal_type,data_t>);
    if(it != m_store.end() && !(tag_ < it->first)) // found
      return it;
    else
      return m_store.end();
  }

  const_iterator lower_bound(const ordinal_type& tag_) const
  { if(!m_order) this->mf_sort(); return std::lower_bound(m_store.begin(),m_store.end(),tag_,__pair_comp_less_l<ordinal_type,data_t>); }

  iterator lower_bound(const ordinal_type& tag_)
  { if(!m_order) this->mf_sort(); return std::lower_bound(m_store.begin(),m_store.end(),tag_,__pair_comp_less_l<ordinal_type,data_t>); }

  const_iterator upper_bound(const ordinal_type& tag_) const
  { if(!m_order) this->mf_sort(); return std::upper_bound(m_store.begin(),m_store.end(),tag_,__pair_comp_less_u<ordinal_type,data_t>); }

  iterator upper_bound(const ordinal_type& tag_)
  { if(!m_order) this->mf_sort(); return std::upper_bound(m_store.begin(),m_store.end(),tag_,__pair_comp_less_u<ordinal_type,data_t>); }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Insert dense-block
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  //! return true if the requested block is non-zero, called by block tag
  bool allowed(const ordinal_type& tag_) const { return this->mf_check_allowed(index(tag_)); }
  //! return true if the requested block is non-zero, called by block index
  bool allowed(const index_type& _index) const { return this->mf_check_allowed(_index); }

  //! reserve non-zero block and return its iterator, by block tag
  /*! if the requested block already exists:
   *  - return its iterator
   *  - or, return error if it's not allowed
   *  if the requested block hasn't allocated
   *  - allocate dense-array block and return its iterator
   *  - or, return last iterator if it's not allowed, with warning message (optional)
   */
  iterator reserve(const ordinal_type& tag_) {
    index_type _index = index(tag_);
    // check if the requested block can be non-zero
    iterator it = this->find(tag_);
    if(this->mf_check_allowed(_index)) {
      if(it == this->end()) {
        it = this->upper_bound(tag_);
        it = m_store.insert(it,std::make_pair(tag_,data_t(new TArray<T,N>(m_dn_shape & _index))));
      }
      else {
        BTAS_THROW((m_dn_shape & _index) == it->second->shape(), "btas::STArray::reserve: existed block has inconsistent shape");
      }
    }
    else {
      if(it != this->end()) {
        BTAS_THROW(false, "btas::STArray::reserve: non-zero block already exists despite it must be zero");
      }
#ifdef _PRINT_WARNINGS
      else {
        BTAS_DEBUG("WARNING: btas::STArray::reserve: requested block must be zero, returns end()");
      }
#endif
    }
    return it;
  }

  //! reserve non-zero block and return its iterator, by block index
  iterator reserve(const index_type& _index) {
    ordinal_type tag_ = tag(_index);
    // check if the requested block can be non-zero
    iterator it = this->find(tag_);
    if(this->mf_check_allowed(_index)) {
      if(it == this->end()) {
        it = this->upper_bound(tag_);
        it = m_store.insert(it,std::make_pair(tag_,data_t(new TArray<T,N>(m_dn_shape & _index))));
      }
      else {
        BTAS_THROW((m_dn_shape & _index) == it->second->shape(), "btas::STArray::reserve: existed block has inconsistent shape");
      }
    }
    else {
      if(it != this->end()) {
        BTAS_THROW(false, "btas::STArray::reserve; non-zero block already exists despite it must be zero");
      }
#ifdef _PRINT_WARNINGS
      else {
        BTAS_DEBUG("WARNING: btas::STArray::reserve: requested block must be zero, returns end()");
      }
#endif
    }
    return it;
  }

  //! insert dense-array block and return its iterator, by block tag
  /*! if the requested block already exists:
   *  - add array to it, return its iterator
   *  if the requested block hasn't allocated
   *  - insert dense-array block and return its iterator
   *  - or, return last iterator if it's not allowed, with warning message (optional)
   */
  iterator insert(const ordinal_type& tag_, const TArray<T, N>& block) {
    index_type _index = index(tag_);
    // check if the requested block can be non-zero
    iterator it = this->end();
    if(this->mf_check_allowed(_index)) {
      this->mf_check_dshape(_index, block.shape());
      it = this->find(tag_);
      if(it != this->end()) {
        it->second->add(block);
      }
      else {
        it = this->upper_bound(tag_);
        it = m_store.insert(it, std::make_pair(tag_,data_t(new TArray<T, N>(block))));
      }
    }
#ifdef _PRINT_WARNINGS
    else {
      BTAS_DEBUG("WARNING: btas::STArray::insert: requested block must be zero, unable to be inserted");
    }
#endif
    return it;
  }

  //! insert dense-array block and return its iterator, by block index
  iterator insert(const index_type& _index, const TArray<T, N>& block) {
    ordinal_type tag_ = tag(_index);
    // check if the requested block can be non-zero
    iterator it = this->end();
    if(this->mf_check_allowed(_index)) {
      this->mf_check_dshape(_index, block.shape());
      it = this->find(tag_);
      if(it != this->end()) {
        it->second->add(block);
      }
      else {
        it = this->upper_bound(tag_);
        it = m_store.insert(it, std::make_pair(tag_,data_t(new TArray<T, N>(block))));
      }
    }
#ifdef _PRINT_WARNINGS
    else {
      BTAS_DEBUG("WARNING: btas::STArray::insert: requested block must be zero, unable to be inserted");
    }
#endif
    return it;
  }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Transposed and Permuted references
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  //! return reference in which the sparse-blocks are transposed
  /*! dense-arrays are not transposed
   *  \param K is a rank to be transposed
   *  e.g. N = 6, K = 4
   *  [i,j,k,l,m,n] -> [m,n,i,j,k,l]
   *  |1 2 3 4|5 6|    |5 6|1 2 3 4|
   */
  STArray transposed_view(size_t K) const {
    STArray trans;
    if(K == N) {
      trans.reference(*this);
    }
    else {
      TVector<Dshapes, N> t_dn_shape = transpose(m_dn_shape, K);
      trans.resize(t_dn_shape, false);

      size_t oldstr = stride_[K-1];
      size_t newstr = size() / oldstr;

      trans.m_store.reserve(m_store.size());
      for(const_iterator it = m_store.begin(); it != m_store.end(); ++it) {
        ordinal_type oldtag = it->first;
        ordinal_type newtag = oldtag / oldstr + (oldtag % oldstr)*newstr;
        trans.m_store.push_back(std::make_pair(newtag, it->second));
      }
      this->mf_sort();
    }
    return std::move(trans);
  }

  //! return reference in which the sparse-blocks are permuted by pindex
  /*! dense-arrays are not permuted */
  STArray permuted_view(const index_type& pindex) const {
    STArray pmute;
    if(pindex == sequence<N>(0, 1)) {
      pmute.reference(*this);
    }
    else {
      TVector<Dshapes, N> p_dn_shape = permute(m_dn_shape, pindex);
      pmute.resize(p_dn_shape, false);

      stride_type p_stride;
      for(size_t i = 0; i < N; ++i) p_stride[pindex[i]] = pmute.stride_[i];

      pmute.m_store.reserve(m_store.size());
      for(const_iterator it = m_store.begin(); it != m_store.end(); ++it) {
        index_type _index(index(it->first));
        pmute.m_store.push_back(std::make_pair(dot(_index, p_stride), it->second));
      }
      this->mf_sort();
    }
    return pmute;
  }

protected:

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Member variables
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  /// sparse-block shape
  extent_type extent_;

  /// dense-block shapes
  TVector<Dshapes, N>
    m_dn_shape;

  /// stride for sparse-block
  stride_type stride_;

  /// non-zero data array mapped by tag
  mutable storage_type
    m_store;

  mutable bool
    m_order;

}; // class STArray

}; // namespace btas

//! C++ style printing function
template<typename T, size_t N>
std::ostream& operator<< (std::ostream& ost, const btas::STArray<T, N>& a) {
  using std::endl;
  // print out sparsity information
  const auto& a_shape = a.shape();
  ost << "block shape = [ ";
  for(size_t i = 0; i < N-1; ++i) ost << a_shape[i] << " x ";
  ost << a_shape[N-1] << " ] ( sparsity = " << a.nnz() << " / " << a.size() << " ) " << endl;

  for(typename btas::STArray<T, N>::const_iterator ib = a.begin(); ib != a.end(); ++ib) {
    ost << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    auto b_index = a.index(ib->first);
    ost << "\tindex = [ ";
    for(size_t i = 0; i < N-1; ++i) ost << b_index[i] << ", ";
    ost << b_index[N-1] << " ] : " << *ib->second << endl;
  }
  return ost;
}

#include <btas/SPARSE/STBLAS.h>
#include <btas/SPARSE/STREINDEX.h>
#include <btas/SPARSE/STCONTRACT.h>

#include <btas/SPARSE/STdsum.h>

#endif // __BTAS_SPARSE_STARRAY_H
