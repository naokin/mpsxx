/// \file TVector.h
/// \brief Utility functions for fixed and variable size vectors.
///
/// Boost array or STL array (C++11) class is used as a fixed size vector TVector
/// Convenient shaping function, dot product, sequence constructor, transposition,
/// and permutation functions are provided.
///
/// C/C++ interfaces of fast copying, scaling, and adding are provided, which are
/// specialized for each variable type to call corresponding BLAS function.

#ifndef __BTAS_CXX11_TVECTOR_H
#define __BTAS_CXX11_TVECTOR_H

#include <vector>
#include <complex>
#include <algorithm>
#include <type_traits>

#include <boost/version.hpp>
#include <boost/serialization/serialization.hpp>

#include <btas/common/btas.h>

#if ((BOOST_VERSION / 100) % 100) > 54

#include <boost/array.hpp>
#include <boost/serialization/array.hpp>

#else

#include <array>

namespace boost {
namespace serialization {

//####################################################################################################
// Boost serialization for std::array
//####################################################################################################

/// Enables to use boost serialization
template<class Archive, typename T, size_t N>
void serialize(Archive& ar, std::array<T, N>& vec, const unsigned int version) {
  for(size_t i = 0; i < N; ++i) ar & vec[i];
}

} // namespace serialization
} // namespace boost

#endif

namespace btas {

/// Alias to dense shape
typedef std::vector<size_t> Dshapes;

//####################################################################################################
// Template aliases to fixed-rank array
//####################################################################################################

#if BOOST_VERSION / 100 % 100 > 54
/// Template aliases to std::array<T, N>, for convenience
template<typename T, size_t N>
using TVector = std::array<T, N>;
#else
/// Template aliases to boost::array<T, N>, for convenience
template<typename T, size_t N>
using TVector = boost::array<T, N>;
#endif

/// Template aliases to TVector<int, N>, for convenience
template<size_t N>
using IVector = TVector<int, N>;

/// Convenient constructor of TVector with const value
template<typename T, size_t N>
TVector<T,N> uniform (const T& value)
{
  TVector<T,N> vec;
  std::fill(vec.begin(),vec.end(),value);
  return vec;
}

//####################################################################################################
// Convenient TVector constructor: make_array
//####################################################################################################

template<size_t I, size_t N>
struct __make_array_helper {
  template<typename T, typename U, typename... Args>
  static void call (TVector<T,N>& x, const U& value, const Args&... args)
  { x[I-1] = value; __make_array_helper<I+1,N>::call(x,args...); }
};

template<size_t N>
struct __make_array_helper<N,N> {
  template<typename T, typename U>
  static void call (TVector<T,N>& x, const U& value)
  { x[N-1] = value; }
};

/// Convenient TVector constructor for N = 1
template<typename T, typename... Args>
TVector<T,1+sizeof...(Args)> make_array (const T& value, const Args&... args)
{
  TVector<T,1+sizeof...(Args)> x;
  __make_array_helper<1,1+sizeof...(Args)>::call(x,value,args...);
  return x;
}

//####################################################################################################
// Dot product: dot
//####################################################################################################

template<size_t I, size_t N>
struct __dot_helper {
  template<typename T>
  static T call (const TVector<T,N>& x, const TVector<T,N>& y)
  { return x[I-1]*y[I-1]+__dot_helper<I+1,N>::call(x,y); }
};

template<size_t N>
struct __dot_helper<N,N> {
  template<typename T>
  static T call (const TVector<T,N>& x, const TVector<T,N>& y)
  { return x[N-1]*y[N-1]; }
};

/// Dot product of two TVector's
template<typename T, size_t N>
T dot (const TVector<T,N>& x, const TVector<T,N>& y)
{ return __dot_helper<1,N>::call(x,y); }


//####################################################################################################
// IVector constructor: shape
//####################################################################################################

/// Alias to make_array of integer
template<typename T, typename... Args>
auto shape (const T& value, const Args&... args)
-> TVector<typename std::enable_if<std::is_integral<T>::value,T>::type,1+sizeof...(Args)>
{ return make_array(value,args...); }

/// Convenient IVector constructor to return sequence
/// e.g. sequence<5>(0, 1) returns { 0, 1, 2, 3, 4 }
/// \param first starting value of sequence
/// \param incl  increments of sequence */
template<typename T, size_t N>
auto sequence (T first = 0, T incl = 1)
-> TVector<typename std::enable_if<std::is_integral<T>::value,T>::type,N>
{
  TVector<T,N> x;
  for(size_t i = 0; i < N; ++i) {
    x[i] = first;
    first += incl;
  }
  return x;
}

//####################################################################################################
// Transpose and Permute vector elements
//####################################################################################################

/// Return transposed vector.
/// e.g. N = 6, K = 4
/// [i,j,k,l,m,n] -> [m,n,i,j,k,l]
/// |1 2 3 4|5 6|    |5 6|1 2 3 4| */
template<typename T, size_t N>
TVector<T,N> transpose (const TVector<T,N>& x, size_t K) {
  assert(K >= 0 && K <= N);
  TVector<T,N> y;
  for(size_t i = 0; i < N-K; ++i) y[i] = x[i+K];
  for(size_t i = N-K; i < N; ++i) y[i] = x[i+K-N];
  return y;
}

/// Return permuted vector
template<typename T, typename Indx_t, size_t N>
TVector<T,N> permute(const TVector<T,N>& x, const TVector<Indx_t,N>& p) {
  TVector<T,N> y;
  for(size_t i = 0; i < N; ++i) y[i] = x[p[i]];
  return y;
}

//####################################################################################################
// Fast copying and adding function: fast_copy, fast_add
//####################################################################################################

/// Fast copy for general type
template<typename T>
inline void _fast_copy(size_t n, const T* x, T* y) { std::copy(x, x+n, y); }

/// Fast scale function for general type (this must be specialized).
template<typename T>
inline void _fast_scal(size_t n, const T& alpha, T* x) { }

/// Fast addition for general type (this must be specialized).
template<typename T>
inline void _fast_add (size_t n, const T* x, T* y) { }

/// Fast copy function for std::vector<T>
template<typename T>
void fast_copy(const std::vector<T>& v1, std::vector<T>& v2) {
  size_t n = v1.size(); v2.resize(n);
  // specialized by T
  _fast_copy(n, v1.data(), v2.data());
}

/// Fast scale function for std::vector<T>
template<typename T>
void fast_scal(const T& alpha, std::vector<T>& v) {
  size_t n = v.size();
  // specialized by T
  _fast_scal(n, alpha, v.data());
}

/// Fast copy function for std::vector<T>
template<typename T>
void fast_add(const std::vector<T>& v1, std::vector<T>& v2) {
  size_t n1 = v1.size();
  size_t n2 = v2.size();
  assert(n1 == n2);
  // specialized by T
  _fast_add(n1, v1.data(), v2.data());
}

}; // namespace btas

//####################################################################################################
// Direct product of Dshapes ( aka std::vector<int> ) as operator*
//####################################################################################################

/// Direct product of Dshapes
inline btas::Dshapes operator* (const btas::Dshapes& ds1, const btas::Dshapes& ds2) {
  btas::Dshapes dsx;
  dsx.reserve(ds1.size()*ds2.size());
  for(const int& di : ds1)
    for(const int& dj : ds2) dsx.push_back(di*dj);
  return dsx;
}

//####################################################################################################
// Overloaded operator for index product of TVector<std::vector<T>, N>
//####################################################################################################

/// Indexed product of TVector<std::vector<T>, N>
/// e.g. index = { i, j, k, l }
/// return x[0][i] * x[1][j] * x[2][k] * x[3][l] */
template<typename T, typename Indx_t, size_t N>
T operator* (const btas::TVector<std::vector<T>,N>& x, const btas::TVector<Indx_t,N>& indx) {
  T value = x[0][indx[0]];
  for(size_t i = 1; i < N; ++i) value = value*x[i][indx[i]];
  return value;
}

/// Indexed product of TVector<std::vector<T>, N>
/// e.g. index = { i, j, k, l }
/// return x[0][i] * x[1][j] * x[2][k] * x[3][l] */
template<typename T, typename Indx_t, size_t N>
T operator* (const btas::TVector<Indx_t,N>& indx, const btas::TVector<std::vector<T>,N>& x) {
  T value = x[0][indx[0]];
  for(size_t i = 1; i < N; ++i) value = value*x[i][indx[i]];
  return value;
}

/// Abstract TVector<T, N> from TVector<std::vector<T>, N> by index
/// e.g. index = { i, j, k, l }
/// return { x[0][i], x[1][j], x[2][k], x[3][l] } */
template<typename T, typename Indx_t, size_t N>
btas::TVector<T,N> operator& (const btas::TVector<std::vector<T>,N>& x, const btas::TVector<Indx_t,N>& indx) {
  btas::TVector<T,N> y;
  for(int i = 0; i < N; ++i) y[i] = x[i][indx[i]];
  return std::move(y);
}

/// Abstract TVector<T, N> from TVector<std::vector<T>, N> by index
/// e.g. index = { i, j, k, l }
/// return { x[0][i], x[1][j], x[2][k], x[3][l] } */
template<typename T, typename Indx_t, size_t N>
btas::TVector<T,N> operator& (const btas::TVector<Indx_t,N>& indx, const btas::TVector<std::vector<T>,N>& x) {
  btas::TVector<T,N> y;
  for(int i = 0; i < N; ++i) y[i] = x[i][indx[i]];
  return std::move(y);
}

//####################################################################################################
// Stream printing operator for std::vector<T>
//####################################################################################################

#include <iostream>

/// Printing elements in array as " [ v[0], v[1], v[2], ... ] "
template<typename T, size_t N>
std::ostream& operator<< (std::ostream& ost, const btas::TVector<T,N>& x) {
  ost << "[ ";
  for(size_t i = 0; i < N-1; ++i) ost << x[i] << ", ";
  ost << x[N-1] << " ]";
  return ost;
}

/// Printing elements in vector as " [ v[0], v[1], v[2], ... ] "
template<typename T>
std::ostream& operator<< (std::ostream& ost, const std::vector<T>& x) {
  size_t n = x.size();
  if(n == 0) {
    ost << "[ ]";
  }
  else {
    ost << "[ ";
    for(size_t i = 0; i < n-1; ++i) ost << x[i] << ", ";
    ost << x[n-1] << " ]";
  }
  return ost;
}

#endif // __BTAS_CXX11_TVECTOR_H
