#ifndef __BTAS_CXX11_HEADER_INCLUDED
#define __BTAS_CXX11_HEADER_INCLUDED

#include <vector>
#include <cassert>

#include <iostream>
#define BTAS_DEBUG(msg)\
{ std::cout << "BTAS_DEBUG: " << msg << std::endl; }

#include <stdexcept>
#define BTAS_THROW(truth, msg)\
{ if (!(truth)) { throw std::runtime_error(msg); } }

namespace btas {

/// Null-deleter
struct null_deleter {
  void operator() (void const *) const { }
};

} // namespace btas

#endif // __BTAS_CXX11_HEADER_INCLUDED
