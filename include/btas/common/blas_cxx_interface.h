#ifndef _BLAS_CXX_INTERFACE_H
#define _BLAS_CXX_INTERFACE_H 1

#ifdef _HAS_INTEL_MKL

#include <mkl_cblas.h>

#else  // _HAS_INTEL_MKL

#include <cblas.h>

#endif // _HAS_INTEL_MKL

#endif // _BLAS_CXX_INTERFACE_H
