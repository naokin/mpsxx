
/// \file  prime_operator.h
/// \brief Primitive site operator generator
/// \par Usage:
/// prime_op_generator<mpsxx::mpogen::_cre_a>(op,l_index,r_index,1.0);

#ifndef __MPSXX_MPOGEN_PRIME_OPERATOR_H
#define __MPSXX_MPOGEN_PRIME_OPERATOR_H 1

//#include <btas/TVector.h>
#include <btas/QSPARSE/QSTArray.h>

#include "mpsite.h"
#include "fermion.h"

namespace mpsxx {
namespace mpogen {

/// Non-zero component of primitive operator
struct prime_op_component {
  /// Constructor
  prime_op_component (
    const MpSite<fermion>::PHYSICAL_INDEX& bra_,
    const MpSite<fermion>::PHYSICAL_INDEX& ket_,
    const double& scalar_)
  : bra(bra_), ket(ket_), scalar(scalar_) { }

  /// Return true if bra index has odd particle number
  bool parity() const
  { return (bra == MpSite<fermion>::alpha || bra == MpSite<fermion>::beta); }

  MpSite<fermion>::PHYSICAL_INDEX bra;   ///< bra index

  MpSite<fermion>::PHYSICAL_INDEX ket;   ///< ket index

  double scalar;  ///< non-zero scalar element
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Static objects for fermionic operators
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//! indentity
struct _identity {
  const static btas::TVector<prime_op_component,4> elements;
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//! create (alpha)
struct _cre_a {
  const static btas::TVector<prime_op_component,2> elements;
};

//! destruct (alpha)
struct _des_a {
  const static btas::TVector<prime_op_component,2> elements;
};

//! create (beta)
struct _cre_b {
  const static btas::TVector<prime_op_component,2> elements;
};

//! destruct (beta)
struct _des_b {
  const static btas::TVector<prime_op_component,2> elements;
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//! create (alpha) x create (beta)
struct _cre_a_cre_b {
  const static btas::TVector<prime_op_component,1> elements;
};

//! create (beta) x create (alpha): this is taken to be normal order
struct _cre_b_cre_a {
  const static btas::TVector<prime_op_component,1> elements;
};

//! destruct (alpha) x destruct (beta): this is taken to be normal order
struct _des_a_des_b {
  const static btas::TVector<prime_op_component,1> elements;
};

//! destruct (beta) x destruct (alpha)
struct _des_b_des_a {
  const static btas::TVector<prime_op_component,1> elements;
};

//! create (alpha) x destruct (alpha): counting operator (alpha)
struct _cre_a_des_a {
  const static btas::TVector<prime_op_component,2> elements;
};

//! create (alpha) x destruct (beta): spin up operator
struct _cre_a_des_b {
  const static btas::TVector<prime_op_component,1> elements;
};

//! create (beta) x destruct (alpha): spin down operator
struct _cre_b_des_a {
  const static btas::TVector<prime_op_component,1> elements;
};

//! create (beta) x destruct (beta): counting operator (beta)
struct _cre_b_des_b {
  const static btas::TVector<prime_op_component,2> elements;
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//! create (alpha) x create (beta) x destruct (beta)
struct _cre_a_cre_b_des_b {
  const static btas::TVector<prime_op_component,1> elements;
};

//! create (beta) x create (alpha) x destruct (alpha)
struct _cre_b_cre_a_des_a {
  const static btas::TVector<prime_op_component,1> elements;
};

//! create (alpha) x destruct (alpha) x destruct (beta)
struct _cre_a_des_a_des_b {
  const static btas::TVector<prime_op_component,1> elements;
};

//! create (beta) x destruct (beta) x destruct (alpha)
struct _cre_b_des_b_des_a {
  const static btas::TVector<prime_op_component,1> elements;
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//! create (alpha) x destruct (alpha) x create (beta) x destruct (beta): referred to chemist's notation
struct _cre_a_des_a_cre_b_des_b {
  const static btas::TVector<prime_op_component,1> elements;
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Prime operator generator to construct a primitive site mpo
/// \tparam PrimeOp site operator type class, e.g. _cre_a, _cre_a_des_a, etc...
/// \param l_index  left  index
/// \param r_index  right index
/// \param factor scaling factor, set 1. by default
template<class PrimeOp>
void prime_op_generator (
        btas::QSTArray<double,4,fermion>& op,
  const int& l_index, const int& r_index, const double& factor = 1.0)
{
  const fermion& l_quanta = op.qshape(0)[l_index];
  const fermion& r_quanta = op.qshape(3)[r_index];

  if(std::fabs(factor) < 1.0e-16) return;

  btas::TArray<double,4> block(1,1,1,1);
  for(const prime_op_component& p : PrimeOp::elements) {
    if(l_quanta.parity() && p.parity())
      block =-factor * p.scalar;
    else
      block = factor * p.scalar;
    op.insert(btas::shape(l_index,p.bra,p.ket,r_index),block);
  }
}

//
// Real-sparse array version?
//
/***
template<class PrimeOp>
void prime_op_generator
(btas::QSTArray<double,4, mpsxx::fermion>& op, const int& l_index, const int& r_index, const double& factor = 1.0)
{
  const fermion& l_quanta = op.qshape(0)[l_index];
  const fermion& r_quanta = op.qshape(3)[r_index];

  if(std::fabs(factor) < 1.0e-16) return;

  double block;
  for(const prime_op_component& p : PrimeOp::elements) {
    if(l_quanta.parity() && p.parity())
      block =-factor * p.scalar;
    else
      block = factor * p.scalar;
    op.insert(btas::shape(l_index,p.bra,p.ket,l_index),block);
  }
}
***/

} // mpogen
} // mpsxx

#endif // __MPSXX_MPOGEN_PRIME_OPERATOR_H
