#ifndef __MPSXX_MPBLAS_HPP
#define __MPSXX_MPBLAS_HPP

namespace mpsxx {

/// MPS(a) = MPS(x) + MPS(y)
template<class Q>
void dsum (
  const Tensor<double,3,Q>& x,
  const Tensor<double,3,Q>& y,
        Tensor<double,3,Q>& a)
{
  // Check total qnum
  assert(x.qnum() == y.qnum());
  // Check qnum_shape for physical index
  assert(x.qnum_shape(1) == y.qnum_shape(1));

  // Calculate qnum_shape of 'a'
  typename Tensor<double,3,Q>::qnum_shape_type qa;
  for(size_t i = 0; i < 3; ++i) {
    qa[i].insert(qa[i].end(),x.qnum_shape(i).begin(),x.qnum_shape(i).end());
    qa[i].insert(qa[i].end(),y.qnum_shape(i).begin(),y.qnum_shape(i).end());
  }
  Tensor<double,3,Q> a(x.qnum,qa);
  // Assign x
  for(auto it = x.begin(); it != x.end(); ++it) {
    a[it->ordinal()] = *it;
  }
  // Tensor slice
  typename Tensor<double,3,Q>::range_array_type y_ranges;
  size_t l_offs = x.extent(0);
  size_t r_offs = x.extent(2);
  y_ranges[0] = make_range(l_offs,a.extent(0));
  y_ranges[1] = make_range(0ul   ,a.extent(1));
  y_ranges[2] = make_range(r_offs,a.extent(2));
  auto a_ref = a.slice(y_ranges);
  // Assign y
  for(auto it = y.begin(); it != y.end(); ++it) {
    a_ref[it->ordinal()] = *it;
  }
}

/// Compute MPS[y] := alpha*MPS[x] + MPS[y]
template<class Q>
void axpy (
  const double& alpha,
  const std::vector<Tensor<double,3,Q>>& x,
        std::vector<Tensor<double,3,Q>>& y)
{
  assert(x.size() == y.size());
  scal(alpha,x);
  // loop over sites
  for(size_t i = 0; i < x.size(); ++i) {
    dsum(x,y,1); // direct sum with trace index 1 of {0,1,2}
  }
}

} // namespace mpsxx

#endif // __MPSXX_MPBLAS_HPP
