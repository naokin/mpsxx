#ifndef __MPSXX_MPSXX_H
#define __MPSXX_MPSXX_H

#include <btas/QSTArray.h>

namespace mpsxx {

template<typename T, size_t N, class Q>
using Tensor = btas::QSTArray<T,N,Q>;

} // namespace mpsxx

#endif // __MPSXX_BTAS_DEFS_H
