#ifndef __BROYDEN_FLETCHER_GOLDFARB_SHANNO_HPP
#define __BROYDEN_FLETCHER_GOLDFARB_SHANNO_HPP

#include <iostream>
#include <iomanip>

#include <cmath>
#include <vector>
#include <algorithm>

#include <mkl_cblas.h>
#include <mkl_lapacke.h>

#include "BTLS.hpp"

struct BFGS {

  typedef std::vector<double> Vector;

  template<class Grad>
  BFGS (Grad G, Vector& x, const double& tole = 1.0e-5)
  {
    double alpha = 0.7;
    size_t N = x.size();

    // initial guess for inverse Hessian
    std::vector<double> Hk(N*N,0.0);
    for(size_t i = 0; i < N; ++i) Hk[i*N+i] = alpha;

    Vector x0 = x;
    Vector g0 = G(x0);

    double gnrm = cblas_dnrm2(N,g0.data(),1)/N;
    if(gnrm < tole) return;

    size_t iter = 0;
    while(iter < 1000) {
      Vector sk(N,0.0);
      cblas_dgemv(CblasRowMajor,CblasNoTrans,N,N,-1.0,Hk.data(),N,g0.data(),1,1.0,sk.data(),1);
      Vector x1(x0);
      double scale = 1.0;
      if(cblas_ddot(N,sk.data(),1,sk.data(),1) > alpha) scale = alpha;
      cblas_daxpy(N,scale,sk.data(),1,x1.data(),1);
      Vector g1 = G(x1);
      gnrm = sqrt(cblas_ddot(N,g1.data(),1,g1.data(),1)/N);
      std::cout.precision(2);
      std::cout << "\tITER :: " << std::setw(3) << iter
                << " / Grad. norm = " << std::scientific << std::setw(10) << gnrm << std::endl;
      //
      if(gnrm < tole) break;
      Vector yk(g1);
      cblas_daxpy(N,-1.0,g0.data(),1,yk.data(),1);
      Vector Hy(N,0.0);
      cblas_dgemv(CblasRowMajor,CblasNoTrans,N,N,1.0,Hk.data(),N,yk.data(),1,1.0,Hy.data(),1);
      double yy = cblas_ddot(N,yk.data(),1,Hy.data(),1);
      double sy = cblas_ddot(N,sk.data(),1,yk.data(),1);
      if(fabs(sy) >= 1.0e-6) { // Hessian to be updated...
        double num = 1.0+yy/sy;
        cblas_dger(CblasRowMajor,N,N, num/sy,sk.data(),1,sk.data(),1,Hk.data(),N);
        cblas_dger(CblasRowMajor,N,N,-1.0/sy,Hy.data(),1,sk.data(),1,Hk.data(),N);
        cblas_dger(CblasRowMajor,N,N,-1.0/sy,sk.data(),1,Hy.data(),1,Hk.data(),N);
      }
      x0 = x1;
      g0 = g1;
      ++iter;
    }

    x = x0;
  }

  /// BFGS with BTLS
  template<class Func, class Grad>
  BFGS (Func F, Grad G, Vector& x, const double& tole = 1.0e-5)
  {
    size_t N = x.size();
    double alpha = 0.1;

    // initial guess for inverse Hessian
    std::vector<double> Hk(N*N,0.0);
    for(size_t i = 0; i < N; ++i) Hk[i*N+i] = 1.0;

//  Vector x0 = x;
    Vector g0 = G(x);

    double gnrm = cblas_dnrm2(N,g0.data(),1)/N;
    if(gnrm < tole) return;

    size_t iter = 0;
    while(iter < 1000) {
      // determine search direction
      Vector pk(N,0.0);
      cblas_dgemv(CblasRowMajor,CblasNoTrans,N,N,-1.0,Hk.data(),N,g0.data(),1,1.0,pk.data(),1);
      double pnrm = cblas_dnrm2(N,pk.data(),1);
      cblas_dscal(N,1.0/pnrm,pk.data(),1);
      // Line-Search
      if(alpha < 1.0e-3) alpha = 1.0e-2;
      double m = -cblas_ddot(N,pk.data(),1,g0.data(),1);
//    Vector x1(x0);
      BTLS(F,x,pk,alpha,m,5); // x and alpha is updated: x = x + alpha*pk
      Vector sk(pk);
      cblas_dscal(N,alpha,sk.data(),1);
      // New Grad.
      Vector g1 = G(x);
      gnrm = sqrt(cblas_ddot(N,g1.data(),1,g1.data(),1)/N);
      std::cout.precision(8);
      std::cout << "\tDEBUG :: alpha = " << std::fixed << alpha << std::endl;
      std::cout.precision(2);
      std::cout << "\tITER :: " << std::setw(3) << iter
                << " / Grad. norm = " << std::scientific << std::setw(10) << gnrm << std::endl;
      //
      if(gnrm < tole) break;
      //
      // Update Hessian matrix
      Vector yk(g1);
      cblas_daxpy(N,-1.0,g0.data(),1,yk.data(),1);
      Vector Hy(N,0.0);
      cblas_dgemv(CblasRowMajor,CblasNoTrans,N,N,1.0,Hk.data(),N,yk.data(),1,1.0,Hy.data(),1);
      double yy = cblas_ddot(N,yk.data(),1,Hy.data(),1);
      double sy = cblas_ddot(N,sk.data(),1,yk.data(),1);
      if(fabs(sy) >= 1.0e-8) { // Hessian to be updated...
        std::cout << "\tDEBUG :: Update Hessian matrix" << std::endl;
        double num = 1.0+yy/sy;
        cblas_dger(CblasRowMajor,N,N, num/sy,sk.data(),1,sk.data(),1,Hk.data(),N);
        cblas_dger(CblasRowMajor,N,N,-1.0/sy,Hy.data(),1,sk.data(),1,Hk.data(),N);
        cblas_dger(CblasRowMajor,N,N,-1.0/sy,sk.data(),1,Hy.data(),1,Hk.data(),N);
      }
//    x0 = x1;
      g0 = g1;
      ++iter;
    }
//  x = x0;
  }
};

#endif // __BROYDEN_FLETCHER_GOLDFARB_SHANNO_HPP
