#ifndef __MPSXX_POINT_GROUP_C2V_H
#define __MPSXX_POINT_GROUP_C2V_H

#include <string>

#include "point_group.h"

namespace mpsxx {
namespace PG {

struct C2v {

  /// irrep #, the same as Molpro order.
  enum IRREP_ELEMENTS {
    A1 = 0,
    B1 = 1,
    B2 = 2,
    A2 = 3,
    ER = 0xffffffff
  };

  static void set (size_t& N_rep, std::vector<IRREP>& table, std::vector<std::string>& label)
  {
    N_rep = 4;

    table = {
      A1, B1, B2, A2,
      B1, A1, A2, B2,
      B2, A2, A1, B1,
      A2, B2, B1, A1
    };

    label.resize(N_rep);
    label[0] = "A1";
    label[1] = "B1";
    label[2] = "B2";
    label[3] = "A2";
  }

};

} // namespace PG
} // namespace mpsxx

#endif // __MPSXX_POINT_GROUP_C2V_H
