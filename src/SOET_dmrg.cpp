#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

#include <btas/DENSE/TArray.h>

#include <time_stamp.h>

#include "mpidefs.h" // Communicator

#include "input.h"
#include "dmrg.hpp"
#include "mpogen_impl.hpp"
#include "mpogen_kmpos_impl.hpp"
#include "fermion.hpp"
#include "point_group.h"
#include "parsing_integral.h"

std::vector<std::vector<double>> compute_occup_1 (const mpsxx::DMRGInput& input, const std::string& pfix)
{
  size_t N = input.N_sites;
  size_t M = input.N_roots;
  //
  btas::Qshapes<fermion<mpsxx::PointGroup>> qn = mpsxx::PhysStates<fermion<mpsxx::PointGroup>>::quanta();
  btas::Dshapes                             dn(qn.size(),1);
  //
  btas::QSTArray<double,2ul,fermion<mpsxx::PointGroup>>
  Oi(fermion<mpsxx::PointGroup>::zero(),btas::make_array(qn,-qn),btas::make_array(dn,dn),false);
  //
  btas::TArray<double,2ul> I(1,1);
  I = 1.0;
  Oi.insert(btas::shape(1ul,1ul),I);
  Oi.insert(btas::shape(2ul,2ul),I);
  I = 2.0;
  Oi.insert(btas::shape(3ul,3ul),I);
  //
  std::vector<std::vector<double>> occn(M);
  for(size_t k = 0; k < M; ++k) {
    std::vector<double> occ1(N,0.0);
    for(size_t i = 0; i < N; ++i) {
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Ai;
      load(Ai,mpsxx::getfile("wave",pfix,i,k));
      //
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Bi;
      btas::Contract(1.0,Ai,btas::shape(0ul,3ul,1ul),Oi,btas::shape(2ul,3ul),1.0,Bi,btas::shape(0ul,2ul,1ul));
      //
      occ1[i] = btas::Dotc(Ai,Bi);
    }
    occn[k] = occ1;
  }
  //
  return occn;
}

std::vector<std::vector<double>> compute_occup_2 (const mpsxx::DMRGInput& input, const std::string& pfix)
{
  size_t N = input.N_sites;
  size_t M = input.N_roots;
  //
  btas::Qshapes<fermion<mpsxx::PointGroup>> qn = mpsxx::PhysStates<fermion<mpsxx::PointGroup>>::quanta();
  btas::Dshapes                             dn(qn.size(),1);
  //
  btas::QSTArray<double,2ul,fermion<mpsxx::PointGroup>>
  Oi(fermion<mpsxx::PointGroup>::zero(),btas::make_array(qn,-qn),btas::make_array(dn,dn),false);
  //
  btas::TArray<double,2ul> I(1,1);
  I = 1.0;
  Oi.insert(btas::shape(3ul,3ul),I);
  //
  std::vector<std::vector<double>> occn(M);
  for(size_t k = 0; k < M; ++k) {
    std::vector<double> occ2(N,0.0);
    for(size_t i = 0; i < N; ++i) {
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Ai;
      load(Ai,mpsxx::getfile("wave",pfix,i,k));
      //
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Bi;
      btas::Contract(1.0,Ai,btas::shape(0ul,3ul,1ul),Oi,btas::shape(2ul,3ul),1.0,Bi,btas::shape(0ul,2ul,1ul));
      //
      occ2[i] = btas::Dotc(Ai,Bi);
    }
    occn[k] = occ2;
  }
  //
  return occn;
}

int main (int argc, char* argv[])
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;
  using std::scientific;

#ifndef _SERIAL
  boost::mpi::environment env(argc,argv);
#endif
  Communicator world;

  std::string finp = "dmrg.conf";
  std::string fout;
  std::string fdmp = "FCIDUMP";

  std::string pfix = ".";

  bool enable_swap_sweep = true;
  bool do_compress = true;
  bool hubbard = true;

  size_t divide = world.size();

  size_t iprint = 0;

  for(int iarg = 0; iarg < argc; ++iarg) {
    if(strcmp(argv[iarg],"-i") == 0) finp = argv[++iarg];
    if(strcmp(argv[iarg],"-o") == 0) fout = argv[++iarg];
    if(strcmp(argv[iarg],"-s") == 0) pfix = argv[++iarg];
    if(strcmp(argv[iarg],"-f") == 0) fdmp = argv[++iarg];
  }

  mpsxx::DMRGInput input(finp);

  //
  // assign cout as alias to ofst
  //
  std::streambuf *backup;
  backup = cout.rdbuf();
  std::ofstream ofst;
  if(fout.size() > 0) {
    std::ostringstream oss;
    oss << fout << "." << world.rank();
    ofst.open(oss.str().c_str());
    cout.rdbuf(ofst.rdbuf());
  }

  int Norbs;
  int Nelec;
  double Ecore;
  std::vector<int> irreps;
  btas::TArray<double,2> oneint;
  btas::TArray<double,4> twoint;

  cout << "\t****************************************************************************************************" << endl;
  cout << "\t\t\t\tMPSXX::PROTOTYPE::MPO GENERATOR FOR QC "                                                        << endl;
  cout << "\t****************************************************************************************************" << endl;
  cout << endl;
  cout << "\t====================================================================================================" << endl;
  cout << "\t\tLoading integral information from FCIDUMP "                                                         << endl;
  cout << "\t====================================================================================================" << endl;
  cout << endl;

  if(world.rank() == 0) {
    std::ifstream ifst_dmp(fdmp.c_str());
    parsing_fcidump(ifst_dmp,Norbs,Nelec,Ecore,irreps,oneint,twoint);
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,Norbs,0);
  boost::mpi::broadcast(world,Nelec,0);
  boost::mpi::broadcast(world,Ecore,0);
  boost::mpi::broadcast(world,irreps,0);
  boost::mpi::broadcast(world,oneint,0);
  boost::mpi::broadcast(world,twoint,0);
#endif

  input.N_sites = Norbs;

  cout << "\t====================================================================================================" << endl;
  cout << "\t\tGenerating QC MPOs from 1- and 2-particle integrals"                                                << endl;
  cout << "\t====================================================================================================" << endl;
  cout << endl;

  // Set symmetry
       if(input.symmetry == "D2h") { mpsxx::PointGroup::set(mpsxx::_D2h); }
  else if(input.symmetry == "C2v") { mpsxx::PointGroup::set(mpsxx::_C2v); }
  else if(input.symmetry == "C2h") { mpsxx::PointGroup::set(mpsxx::_C2h); }
  else if(input.symmetry == "D2" ) { mpsxx::PointGroup::set(mpsxx::_D2 ); }
  else if(input.symmetry == "Cs" ) { mpsxx::PointGroup::set(mpsxx::_Cs ); }
  else if(input.symmetry == "C2" ) { mpsxx::PointGroup::set(mpsxx::_C2 ); }
  else if(input.symmetry == "Ci" ) { mpsxx::PointGroup::set(mpsxx::_Ci ); }
  else if(input.symmetry == "C1" ) { mpsxx::PointGroup::set(mpsxx::_C1 ); }
  else {
    pout << "Requested symmetry [" << input.symmetry << "] is not supported." << endl;
    abort();
  }

  std::vector<mpsxx::PointGroup> orbsym(irreps.size());
  for(size_t i = 0; i < irreps.size(); ++i) orbsym[i] = mpsxx::PointGroup(irreps[i]);

  std::vector<int> rmdocc(Norbs,0);

  time_stamp ts;

  if(divide > 0)
    mpsxx::mpogen_impl(divide,Norbs,Ecore,orbsym,rmdocc,oneint,twoint,"mpo",pfix,enable_swap_sweep,hubbard,do_compress);
  else
    mpsxx::mpogen_kmpos_impl<1>(Norbs,Ecore,orbsym,rmdocc,oneint,twoint,"mpo",pfix,enable_swap_sweep,hubbard,do_compress);

  world.barrier();

  pout << endl;
  pout.precision(2);
  pout << "\t\t\tWall time for MPOs construction: " << setw(8) << fixed << ts.lap() << endl;

  //
  // dmrg optimization
  //
  pout << input << endl;
  input.name = "mpo";
  input.energy = mpsxx::dmrg<fermion<mpsxx::PointGroup>>(
    input.name,
    input.restart,

    fermion<mpsxx::PointGroup>(
      input.N_elecs,
      input.N_spins,
      mpsxx::PointGroup(input.irrep)),

    input.algorithm,
    input.N_sites,
    input.N_max_states,
    pfix,
    input.N_roots,
    input.tolerance,
    input.noise,
    input.shift,
    input.N_max_sweep_iter
  );

  world.barrier();

  pout << endl;
  pout.precision(2);
  pout << "\t\t\tWall time for DMRG optimization: " << setw(8) << fixed << ts.lap() << endl;
  pout << "\t\t\tWall time for Total calculation: " << setw(8) << fixed << ts.elapsed() << endl;

  std::vector<std::vector<double>> occ1 = compute_occup_1(input,pfix);
  std::vector<std::vector<double>> occ2 = compute_occup_2(input,pfix);

  cout.rdbuf(backup);
  ofst.close();

  cout.precision(16);
  cout << setw(24) << fixed << input.energy[0] << endl;
  cout << setw(4) << input.N_sites << endl;
  for(size_t i = 0; i < input.N_sites; ++i) {
    cout << setw(4) << i+1 << setw(24) << scientific << occ1[0][i] << setw(24) << scientific << occ2[0][i] << endl;
  }

  return 0;
}
