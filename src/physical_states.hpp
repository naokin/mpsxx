#ifndef __MPSXX_PHYSICAL_STATES_HPP
#define __MPSXX_PHYSICAL_STATES_HPP

#include <btas/QSPARSE/Qshapes.h>

#include "fermion.hpp"

namespace mpsxx {

/// the struct PhysStates determines the physical indices of every site
template<class Q>
struct PhysStates {
  /// Return collection of quantum numbers corresponding to the physical indices
  const static btas::Qshapes<Q> quanta() { return btas::Qshapes<Q>(1,Q::zero()); }
};

template<class Symmetry>
struct PhysStates<fermion<Symmetry>> {
  /// Return the collection of physical quantumnumbers corresponding to the physical indices on a site.
  const static btas::Qshapes<fermion<Symmetry>> quanta (const Symmetry& sym = Symmetry::zero())
  {
    btas::Qshapes<fermion<Symmetry>> q(4);
    q[0] = fermion<Symmetry>(0, 0, Symmetry::zero());
    q[1] = fermion<Symmetry>(1,+1, sym);
    q[2] = fermion<Symmetry>(1,-1, sym);
    q[3] = fermion<Symmetry>(2, 0, Symmetry::zero());
    return q;
  }
};

} // namespace mpsxx

#endif // __MPSXX_PHYSICAL_STATES_HPP
