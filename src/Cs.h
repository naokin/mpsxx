#ifndef __MPSXX_POINT_GROUP_CS_H
#define __MPSXX_POINT_GROUP_CS_H

#include <string>

#include "point_group.h"

namespace mpsxx {
namespace PG {

struct Cs {

  /// irrep #, the same as Molpro order.
  enum IRREP_ELEMENTS {
    Ap = 0,
    App= 1,
    ER = 0xffffffff
  };

  static void set (size_t& N_rep, std::vector<IRREP>& table, std::vector<std::string>& label)
  {
    N_rep = 2;

    table = {
      Ap, App,
      App, Ap
    };

    label.resize(N_rep);
    label[0] = "A'";
    label[1] = "A\"";
  }

};

} // namespace PG
} // namespace mpsxx

#endif // __MPSXX_POINT_GROUP_CS_H
