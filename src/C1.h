#ifndef __MPSXX_POINT_GROUP_C1_H
#define __MPSXX_POINT_GROUP_C1_H

#include <algorithm>
#include <string>

#include "point_group.h"

namespace mpsxx {
namespace PG {

struct C1 {

  /// irrep #, the same as Molpro order.
  enum IRREP_ELEMENTS {
    A  = 0,
    ER = 0xffffffff
  };

  static void set (size_t& N_rep, std::vector<IRREP>& table, std::vector<std::string>& label)
  {
    N_rep = 1;

    table = { A };

    label = { "A" };
  }

};

} // namespace PG
} // namespace mpsxx

#endif // __MPSXX_POINT_GROUP_C1_H
