#ifndef __MPSXX_CORE_OP_OBJECT_HPP
#define __MPSXX_CORE_OP_OBJECT_HPP

namespace mpsxx {

enum OpType {
  I,
  H,
  CREA,
  CREB,
  DESA,
  DESB,
  CREA_CREA,
  CREA_DESA,
  DESA_CREA,
  DESA_DESA,
  CREA_CREB,
  CREA_DESB,
  DESA_CREB,
  DESA_DESB,
  CREB_CREA,
  CREB_DESA,
  DESB_CREA,
  DESB_DESA,
  CREB_CREB,
  CREB_DESB,
  DESB_CREB,
  DESB_DESB
};

template<class WorkRep, OpType T>
class OpObject : public WorkRep {

public:

private:

  std::array<size_t,GetOpRank<T>::value> index_;

}; // class OpObject

} // namespace mpsxx

#endif // __MPSXX_CORE_OP_OBJECT_HPP
