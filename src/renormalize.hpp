#ifndef __MPSXX_RENORMALIZE_HPP
#define __MPSXX_RENORMALIZE_HPP

#include <btas/QSPARSE/QSTArray.h>

namespace mpsxx {

/// Renormalize operator tensor
template<class Q>
void renormalize (
        bool forward,
  const btas::QSTArray<double,4,Q>& mpo0,
  const btas::QSTArray<double,3,Q>& opr0,
  const btas::QSTArray<double,3,Q>& bra0,
  const btas::QSTArray<double,3,Q>& ket0,
        btas::QSTArray<double,3,Q>& opr1)
{
  if(forward) {
    btas::QSTArray<double,4,Q> scr1;
    btas::Contract(1.0,opr0,btas::shape(0ul),bra0.conjugate(),btas::shape(0ul),1.0,scr1);
    btas::QSTArray<double,4,Q> scr2;
    btas::Contract(1.0,scr1,btas::shape(0ul,2ul),mpo0,btas::shape(0ul,1ul),1.0,scr2);
    btas::Contract(1.0,scr2,btas::shape(0ul,2ul),ket0,btas::shape(0ul,1ul),1.0,opr1);
  }
  else {
    btas::QSTArray<double,4,Q> scr1;
    btas::Contract(1.0,bra0.conjugate(),btas::shape(2ul),opr0,btas::shape(0ul),1.0,scr1);
    btas::QSTArray<double,4,Q> scr2;
    btas::Contract(1.0,scr1,btas::shape(1ul,2ul),mpo0,btas::shape(1ul,3ul),1.0,scr2);
    btas::Contract(1.0,scr2,btas::shape(3ul,1ul),ket0,btas::shape(1ul,2ul),1.0,opr1);
  }
}

/// Renormalize overlap matrix
template<class Q>
void renormalize (
        bool forward,
  const btas::QSTArray<double,2,Q>& sov0,
  const btas::QSTArray<double,3,Q>& bra0,
  const btas::QSTArray<double,3,Q>& ket0,
        btas::QSTArray<double,2,Q>& sov1)
{
  if(forward) {
    btas::QSTArray<double,3,Q> scr1;
    btas::Gemm(CblasTrans,CblasNoTrans,1.0,sov0,bra0.conjugate(),1.0,scr1);
    btas::Gemm(CblasTrans,CblasNoTrans,1.0,scr1,ket0,1.0,sov1);
  }
  else {
    btas::QSTArray<double,3,Q> scr1;
    btas::Gemm(CblasNoTrans,CblasNoTrans,1.0,bra0.conjugate(),sov0,1.0,scr1);
    btas::Gemm(CblasNoTrans,CblasTrans,1.0,scr1,ket0,1.0,sov1);
  }
}

/// Renormalize merged operator matrix
template<class Q>
void renormalize (
        bool forward,
  const btas::QSTArray<double,3,Q>& opr0,
  const btas::QSTArray<double,2,Q>& bra0,
  const btas::QSTArray<double,2,Q>& ket0,
        btas::QSTArray<double,3,Q>& opr1)
{
  if(forward) {
    btas::QSTArray<double,3,Q> scr1;
    btas::Gemm(CblasTrans,CblasNoTrans,1.0,bra0.conjugate(),opr0,1.0,scr1);
    btas::Gemm(CblasNoTrans,CblasNoTrans,1.0,scr1,ket0,1.0,opr1);
  }
  else {
    btas::QSTArray<double,3,Q> scr1;
    btas::Gemm(CblasNoTrans,CblasNoTrans,1.0,bra0.conjugate(),opr0,1.0,scr1);
    btas::Gemm(CblasNoTrans,CblasTrans,1.0,scr1,ket0,1.0,opr1);
  }
}

/// Renormalize merged overlap matrix
template<class Q>
void renormalize (
        bool forward,
  const btas::QSTArray<double,2,Q>& sov0,
  const btas::QSTArray<double,2,Q>& bra0,
  const btas::QSTArray<double,2,Q>& ket0,
        btas::QSTArray<double,2,Q>& sov1)
{
  if(forward) {
    btas::QSTArray<double,2,Q> scr1;
    btas::Gemm(CblasTrans,CblasNoTrans,1.0,sov0,bra0.conjugate(),1.0,scr1);
    btas::Gemm(CblasTrans,CblasNoTrans,1.0,scr1,ket0,1.0,sov1);
  }
  else {
    btas::QSTArray<double,2,Q> scr1;
    btas::Gemm(CblasNoTrans,CblasNoTrans,1.0,bra0.conjugate(),sov0,1.0,scr1);
    btas::Gemm(CblasNoTrans,CblasTrans,1.0,scr1,ket0,1.0,sov1);
  }
}

} // namespace mpsxx

#endif // __MPSXX_RENORMALIZE_HPP
