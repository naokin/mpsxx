#ifndef __MPSXX_MAKE_RESTART_MPSS_HPP
#define __MPSXX_MAKE_RESTART_MPSS_HPP

#include <iostream>
#include <iomanip>
#include <vector>
#include <functional>

#include <btas/QSPARSE/QSTArray.h>

#include "mpidefs.h"
#include "fileio.h"

#include "renormalize.hpp"

namespace mpsxx {

/// Generate restart
/// \param opname file name of MPO object
/// \param prefix file prefix
/// \param N number of sites
/// \param iroot target root
/// \param qt total quantum number
/// \param M number of states to be kept (0 to keep all states).
template<class Q>
void make_restart_mpss (
  const std::string& opname,
  const std::string& prefix,
  const size_t& N,
  const size_t& iroot,
  const Q& qt,
  const int& M = 0)
{
  using std::endl;
  using std::setw;

  Communicator world;

  pout << "\t====================================================================================================" << endl;
  pout << "\t\t\tRestart MPS: number of sites = " << setw(3) << N << endl;
  pout << "\t====================================================================================================" << endl;

  // peak group size
  size_t ngroup = 0;
  {
    std::vector<btas::QSTArray<double,4,Q>> dummy;
    load(dummy,getfile(opname,prefix,0));
    ngroup = dummy.size();
  }

  btas::Qshapes<Q> qz(1, Q::zero());

  std::vector<std::vector<btas::QSTArray<double,3,Q>>> rH0(iroot+1);
  std::vector<btas::QSTArray<double,2,Q>> rS0(iroot+1);
  for(size_t k = 0; k <= iroot; ++k) {
    rH0[k].resize(ngroup);
    for(size_t g = 0; g < ngroup; ++g) {
      rH0[k][g].resize(Q::zero(),btas::make_array(qz,qz,qz));
      rH0[k][g].insert(btas::shape(0ul,0ul,0ul),btas::TArray<double,3>(1,1,1));
      rH0[k][g].fill(1.0);
    }
    save(rH0[k],getfile("right-H",prefix,N-1,iroot,k));
    if(world.rank() == 0) {
      rS0[k].resize(Q::zero(),btas::make_array(qz,qz));
      rS0[k].insert(btas::shape(0ul,0ul),btas::TArray<double,2>(1,1));
      rS0[k].fill(1.0);
      save(rS0[k],getfile("right-S",prefix,N-1,iroot,k));
    }
  }

  for(size_t i = N-1; i > 0; --i) {
    pout << "\t\t\tInitializing MPS for site [ " << setw(3) << i << " ] " << endl;
    pout << "\t----------------------------------------------------------------------------------------------------" << endl;
    btas::QSTArray<double,3,Q> rmps;
    if(world.rank() == 0)
      load(rmps,getfile("rmps",prefix,i,iroot));
#ifndef _SERIAL
    boost::mpi::broadcast(world,rmps,0);
#endif

    std::vector<btas::QSTArray<double,4,Q>> mpo;
    load(mpo,getfile(opname,prefix,i));
    assert(mpo.size() == ngroup);

    for(size_t k = 0; k <= iroot; ++k) {
      btas::QSTArray<double,3,Q> kmps;
      if(world.rank() == 0) {
        load(kmps,getfile("rmps",prefix,i,k));
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,kmps,0);
#endif

      std::vector<btas::QSTArray<double,3,Q>> rH1(ngroup);
      for(size_t g = 0; g < ngroup; ++g) {
        renormalize(0,mpo[g],rH0[k][g],rmps,kmps,rH1[g]);
      }
      save(rH1,getfile("right-H",prefix,i-1,iroot,k));
      rH0[k] = rH1;

      if(world.rank() == 0) {
        btas::QSTArray<double,2,Q> rS1;
        renormalize(0,rS0[k],rmps,kmps,rS1);
        save(rS1,getfile("right-S",prefix,i-1,iroot,k));
        rS0[k] = rS1;
      }
    }
  }
  pout << "\t\t\tInitializing MPS for site [ " << setw(3) << 0 << " ] " << endl;
  pout << "\t====================================================================================================" << endl;

  std::vector<std::vector<btas::QSTArray<double,3,Q>>> lH0(iroot+1);
  std::vector<btas::QSTArray<double,2,Q>> lS0(iroot+1);
  for(size_t k = 0; k <= iroot; ++k) {
    lH0[k].resize(ngroup);
    for(size_t g = 0; g < ngroup; ++g) {
      lH0[k][g].resize(Q::zero(),btas::make_array(qz,qz,qz));
      lH0[k][g].insert(btas::shape(0ul,0ul,0ul),btas::TArray<double,3>(1,1,1));
      lH0[k][g].fill(1.0);
    }
    save(lH0[k],getfile("left-H",prefix,0,iroot,k));
    if(world.rank() == 0) {
      lS0[k].resize(Q::zero(),btas::make_array(qz,qz));
      lS0[k].insert(btas::shape(0ul,0ul),btas::TArray<double,2>(1,1));
      lS0[k].fill(1.0);
      save(lS0[k],getfile("left-S",prefix,0,iroot,k));
    }
  }
}

} // namespace mpsxx

#endif // __MPSXX_MAKE_RESTART_MPSS_HPP
