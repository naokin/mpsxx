#ifndef __MPSXX_MAKE_RANDOM_MPSS_HPP
#define __MPSXX_MAKE_RANDOM_MPSS_HPP

#include <iostream>
#include <iomanip>
#include <vector>

#include <boost/bind.hpp>

#include <btas/QSPARSE/QSTArray.h>

#include "mpidefs.h"
#include "fileio.h"

#include "canonicalize.hpp"
#include "renormalize.hpp"

namespace mpsxx {

/// Generate quantum numbers for each boundary from MPO
///
/// \param sites quantum numbers for each site (physical index)
/// \param qt total quantum number of lattice
/// \param max_quantum_blocks maximum number of quantum blocks on each boundary (0: no limitation by default)
template<class Q>
std::vector<btas::Qshapes<Q>> gen_slater_quanta (const std::vector<Q>& sites)
{
  size_t N = sites.size();

  // zero quantum number
  btas::Qshapes<Q> qz(1, Q::zero());

  // boundary quantum numbers
  std::vector<btas::Qshapes<Q>> qb(N);

  // generate quantum number blocks for MPS
  qb[0] = { sites[0] };
  for(size_t i = 1; i < N; ++i) {
    btas::Qshapes<Q> qb[i] = { qb[i-1][0] * sites[i] }; // get unique elements of { q(l) x q(n) }
  }

  return qb;
}

/// Generate slater MPS from quanta
/// \param opname file name of MPO object
/// \param prefix file prefix
/// \param N number of sites
/// \param iroot root # to be saved
/// \param qt total quantum number
/// \param M number of states to be kept (0 to keep all states).
template<class Q>
void make_slater_mpss (
  const std::string& opname,
  const std::string& prefix,
  const size_t& N,
  const size_t& iroot,
  const std::vector<Q>& sites)
{
  using std::endl;
  using std::setw;

  Communicator world;

  size_t N = sites.size();

  pout << "\t\t\tGenerating quantum states for each boundary " << endl;

  // zero quantum number
  btas::Qshapes<Q> qz(1, Q::zero());

  // boundary quantum numbers
  std::vector<btas::Qshapes<Q>> qb(N);

  // generate quantum number blocks for MPS
  qb[0] = { sites[0] };
  for(size_t i = 1; i < N; ++i) {
    btas::Qshapes<Q> qb[i] = { qb[i-1][0] * sites[i] }; // get unique elements of { q(l) x q(n) }
  }

  pout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

  btas::TVector<btas::Qshapes<Q>,3> q_shape;
  btas::TVector<btas::Dshapes,   3> d_shape;

  for(size_t i = 0; i < N-1; ++i) {
    pout << "\t\t\tGenerating   MPS for site [ " << setw(3) << i << " ] " << endl;
    pout << "\t----------------------------------------------------------------------------------------------------" << endl;
    qr = qb[i];
    dr = btas::Dshapes(qr.size(),1);

    q_shape = btas::make_array(ql,qn[i],-qr);
    d_shape = btas::make_array(dl,dn[i], dr);

    wfnc.resize(Q::zero(),q_shape,d_shape,boost::bind(dist,rgen));
    btas::Normalize(wfnc);
    save(wfnc,getfile("wave",prefix,i,iroot));
    wfnc.clear();

    ql = qr;
    dl = dr;
  }

  pout << "\t\t\tGenerating   MPS for site [ " << setw(3) << N-1 << " ] " << endl;
  pout << "\t====================================================================================================" << endl;

  qr = qz; // qb[N-1] is not used
  dr = btas::Dshapes(qr.size(),1);

  q_shape = btas::make_array(ql,qn[N-1],-qr);
  d_shape = btas::make_array(dl,dn[N-1], dr);

  wfnc.resize(qt,q_shape,d_shape,boost::bind(dist,rgen)); // set qt as a total quantum number of array
  btas::Normalize(wfnc);
  save(wfnc,getfile("wave",prefix,N-1,iroot));

  } // if(world.rank() == 0)

  // peak group size
  size_t ngroup = 0;
  {
    std::vector<btas::QSTArray<double,4,Q>> dummy;
    load(dummy,getfile(opname,prefix,0));
    ngroup = dummy.size();
  }

  // Initial canonicalization
  std::vector<std::vector<btas::QSTArray<double,3,Q>>> rH0(iroot+1);
  std::vector<btas::QSTArray<double,2,Q>> rS0(iroot+1);
  for(size_t k = 0; k <= iroot; ++k) {
    rH0[k].resize(ngroup);
    for(size_t g = 0; g < ngroup; ++g) {
      rH0[k][g].resize(Q::zero(),btas::make_array(qz,qz,qz));
      rH0[k][g].insert(btas::shape(0ul,0ul,0ul),btas::TArray<double,3>(1,1,1));
      rH0[k][g].fill(1.0);
    }
    save(rH0[k],getfile("right-H",prefix,N-1,iroot,k));
    if(world.rank() == 0) {
      rS0[k].resize(Q::zero(),btas::make_array(qz,qz));
      rS0[k].insert(btas::shape(0ul,0ul),btas::TArray<double,2>(1,1));
      rS0[k].fill(1.0);
      save(rS0[k],getfile("right-S",prefix,N-1,iroot,k));
    }
  }

  for(size_t i = N-1; i > 0; --i) {
    pout << "\t\t\tInitializing MPS for site [ " << setw(3) << i << " ] " << endl;
    pout << "\t----------------------------------------------------------------------------------------------------" << endl;
    btas::QSTArray<double,3,Q> rmps;
    btas::QSTArray<double,3,Q> lmps;
    if(world.rank() == 0) {
      load(lmps,getfile("wave",prefix,i-1,iroot));

      canonicalize(0,wfnc,lmps,rmps,M);
      wfnc = lmps;
      btas::Normalize(wfnc);

      save(wfnc,getfile("wave",prefix,i-1,iroot));
      save(rmps,getfile("rmps",prefix,i,iroot));
    }
#ifndef _SERIAL
    boost::mpi::broadcast(world,wfnc,0);
    boost::mpi::broadcast(world,rmps,0);
#endif

    std::vector<btas::QSTArray<double,4,Q>> mpo;
    load(mpo,getfile(opname,prefix,i));
    assert(mpo.size() == ngroup);

    for(size_t k = 0; k <= iroot; ++k) {
      btas::QSTArray<double,3,Q> kmps;
      if(world.rank() == 0) {
        load(kmps,getfile("rmps",prefix,i,k));
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,kmps,0);
#endif

      std::vector<btas::QSTArray<double,3,Q>> rH1(ngroup);
      for(size_t g = 0; g < ngroup; ++g) {
        renormalize(0,mpo[g],rH0[k][g],rmps,kmps,rH1[g]);
      }
      save(rH1,getfile("right-H",prefix,i-1,iroot,k));
      rH0[k] = rH1;

      if(world.rank() == 0) {
        btas::QSTArray<double,2,Q> rS1;
        renormalize(0,rS0[k],rmps,kmps,rS1);
        save(rS1,getfile("right-S",prefix,i-1,iroot,k));
        rS0[k] = rS1;
      }
    }
  }
  pout << "\t\t\tInitializing MPS for site [ " << setw(3) << 0 << " ] " << endl;
  pout << "\t====================================================================================================" << endl;

  std::vector<std::vector<btas::QSTArray<double,3,Q>>> lH0(iroot+1);
  std::vector<btas::QSTArray<double,2,Q>> lS0(iroot+1);
  for(size_t k = 0; k <= iroot; ++k) {
    lH0[k].resize(ngroup);
    for(size_t g = 0; g < ngroup; ++g) {
      lH0[k][g].resize(Q::zero(),btas::make_array(qz,qz,qz));
      lH0[k][g].insert(btas::shape(0ul,0ul,0ul),btas::TArray<double,3>(1,1,1));
      lH0[k][g].fill(1.0);
    }
    save(lH0[k],getfile("left-H",prefix,0,iroot,k));
    if(world.rank() == 0) {
      lS0[k].resize(Q::zero(),btas::make_array(qz,qz));
      lS0[k].insert(btas::shape(0ul,0ul),btas::TArray<double,2>(1,1));
      lS0[k].fill(1.0);
      save(lS0[k],getfile("left-S",prefix,0,iroot,k));
    }
  }
}

} // namespace mpsxx

#endif // __MPSXX_MAKE_RANDOM_MPSS_HPP
