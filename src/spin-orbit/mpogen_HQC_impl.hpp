#ifndef __MPSXX_MPOGEN_HQC_IMPL_HPP
#define __MPSXX_MPOGEN_HQC_IMPL_HPP

#include <iostream>
#include <iomanip>

#include <vector>
#include <numeric>
#include <string>

#include <btas/common/numeric_traits.h>
#include <btas/DENSE/TArray.h>
#include <btas/QSPARSE/QSTArray.h>

#include "fermion.hpp"
#include "boundary_opinfo.h"
#include "fileio.hpp"
#include "physical_states.hpp"
#include "mpogen_HQC_site.hpp"
#include "op_product.h"

namespace mpsxx {

template<typename T, class Symmetry>
void mpogen_HQC_impl (
  const size_t& N,
  const typename btas::remove_complex<T>::type& Ecore,
  const std::vector<Symmetry>& orbsym,
  const btas::TArray<typename btas::remove_complex<T>::type,2>& oneint,
  const btas::TArray<typename btas::remove_complex<T>::type,4>& twoint,
  const std::string& opname,
  const std::string& prefix,
  const bool& doSwitchSweepDir)
{
  using std::cout;
  using std::endl;
  using std::setw;

  typedef fermion<Symmetry> qnum_t;

  // check integral size
  assert(oneint.shape() == btas::shape(N,N));
  assert(twoint.shape() == btas::shape(N,N,N,N));

  // MPO tensor
  btas::QSTArray<T,4,qnum_t> mpo;

  // left  operator indices
  boundary_opinfo l_ops(0,N,doSwitchSweepDir);
  // right operator indices
  boundary_opinfo s_ops;
  // right operator indices
  boundary_opinfo r_ops;

  std::vector<size_t> l_indxs;
  std::vector<size_t> r_indxs;
  for(size_t i = N-1; i > 0; --i) r_indxs.push_back(i);

  size_t nnz = 0;
  for(int i = 0; i < N; ++i) {
    cout << "\t====================================================================================================" << endl;
    cout << "\t\tSITE [ " << setw(3) << i << " ] " << endl;
    cout << "\t----------------------------------------------------------------------------------------------------" << endl;
    // get boundary operators
    s_ops.reset(i); r_ops.reset(i+1,N,doSwitchSweepDir);
    cout << "\t\tL-BLOCK: " << setw(3) << l_indxs.size() << " sites ( " << setw(6) << l_ops.size() << " ops. ) " << endl;
    cout << "\t\tR-BLOCK: " << setw(3) << r_indxs.size() << " sites ( " << setw(6) << r_ops.size() << " ops. ) " << endl;
    cout << "\t----------------------------------------------------------------------------------------------------" << endl;

    // resize mpo array
    cout << "\t\tresizing site operator array..." << std::flush;

    mpo.clear();

    bool swap_sweep_dir = doSwitchSweepDir && (l_ops.direction() != r_ops.direction());

    auto s_quanta = PhysStates<qnum_t>::quanta(orbsym[i]);
    auto l_quanta = get_qshape(l_ops,orbsym);
    auto r_quanta = get_qshape(r_ops,orbsym);

    btas::Dshapes l_dshape(l_quanta.size(),0);
    btas::Dshapes s_dshape(s_quanta.size(),1);
    btas::Dshapes r_dshape(r_quanta.size(),0);

    if(doSwitchSweepDir) {
      if(swap_sweep_dir)
        mpo.resize(qnum_t::zero(),
                   btas::make_array( l_quanta, s_quanta,-s_quanta, r_quanta),
                   btas::make_array( l_dshape, s_dshape, s_dshape, r_dshape),false);
      else if(l_ops.direction() == boundary_opinfo::FORWARD)
        mpo.resize(qnum_t::zero(),
                   btas::make_array( l_quanta, s_quanta,-s_quanta,-r_quanta),
                   btas::make_array( l_dshape, s_dshape, s_dshape, r_dshape),false);
      else
        mpo.resize(qnum_t::zero(),
                   btas::make_array(-l_quanta, s_quanta,-s_quanta, r_quanta),
                   btas::make_array( l_dshape, s_dshape, s_dshape, r_dshape),false);
    }
    else {
        mpo.resize(qnum_t::zero(),
                   btas::make_array( l_quanta, s_quanta,-s_quanta,-r_quanta),
                   btas::make_array( l_dshape, s_dshape, s_dshape, r_dshape),false);
    }

    cout << "done" << endl;
    // 'dot with sys' in Block code
    size_t nnz_local = 0;
    if(l_ops.direction() == boundary_opinfo::FORWARD) {
      cout << "\t\tgenerating site operators (fwd)..." << std::flush;
      for(auto l = l_ops.begin(); l != l_ops.end(); ++l) {
        for(auto s = s_ops.begin(); s != s_ops.end(); ++s) {
          // prod. operator O_l*O_s
          std::vector<mpogen::BIT_OPERATOR_TYPE> ls_ops = mpogen::op_product(l->first,s->first,r_indxs,swap_sweep_dir);
          // create operator
          for(size_t j = 0; j < ls_ops.size(); ++j) {
            auto r = r_ops.find(ls_ops[j]);
            if(r != r_ops.end()) {
              ++nnz_local;
              mpogen_HQC_site(mpo,l->first,l->second,s->first,r->first,r->second,Ecore,oneint,twoint);
            }
          }
        }
      }
    }
    // 'dot with env'
    else {
      cout << "\t\tgenerating site operators (bwd)..." << std::flush;
      for(auto r = r_ops.begin(); r != r_ops.end(); ++r) {
        for(auto s = s_ops.begin(); s != s_ops.end(); ++s) {
          // prod. operator O_s*O_r
          std::vector<mpogen::BIT_OPERATOR_TYPE> rs_ops = mpogen::op_product(r->first,s->first,l_indxs,swap_sweep_dir);
          // create operator
          for(size_t j = 0; j < rs_ops.size(); ++j) {
            auto l = l_ops.find(rs_ops[j]);
            if(l != l_ops.end()) {
              ++nnz_local;
              mpogen_HQC_site(mpo,l->first,l->second,s->first,r->first,r->second,Ecore,oneint,twoint);
            }
          }
        }
      }
    }
    nnz += nnz_local;
    cout << "done ( " << nnz_local << " ops. are generated ) " << endl;

    cout << "\t\tremoving zero contributed terms (fwd)..." << std::flush;

    btas::TVector<btas::Dshapes,4> mpo_dshape = mpo.check_net_dshape();
    btas::TVector<btas::Dshapes,4> nz_indices;
    for(size_t x = 0; x < mpo_dshape[0].size(); ++x) nz_indices[0].push_back(x);
    for(size_t x = 0; x < mpo_dshape[1].size()-rmdocc[i]; ++x) nz_indices[1].push_back(x);
    for(size_t x = 0; x < mpo_dshape[2].size()-rmdocc[i]; ++x) nz_indices[2].push_back(x);
    for(size_t x = 0; x < mpo_dshape[3].size(); ++x) if(mpo_dshape[3][x] > 0) nz_indices[3].push_back(x);

    mpo = mpo.subarray(nz_indices);
    r_ops.clean(mpo_dshape[3]);

    cout << "done ( non-zero elements: " << mpo.nnz() << " ) " << endl;

    l_ops = r_ops;
    l_indxs.push_back(i); r_indxs.pop_back();

    cout << "\t\tsaving site operator array..." << std::flush;
    save(mpo,getfile(opname,prefix,i));
    mpo.clear();
    cout << "done" << endl;
  }

  btas::Dshapes r_nz_index(1,0);
  for(int i = N-1; i >= 0; --i) {
    cout << "\t====================================================================================================" << endl;
    cout << "\t\tSITE [ " << setw(3) << i << " ] " << endl;
    cout << "\t----------------------------------------------------------------------------------------------------" << endl;
    cout << "\t\tloading site operator array..." << std::flush;
    load(mpo,getfile(opname,prefix,i));
    cout << "done" << endl;

    cout << "\t\tremoving zero contributed terms (bwd)..." << std::flush;

    btas::TVector<btas::Dshapes,4> mpo_dshape = mpo.check_net_dshape();
    btas::TVector<btas::Dshapes,4> nz_indices;

    for(size_t x = 0; x < mpo_dshape[0].size(); ++x) if(mpo_dshape[0][x] > 0) nz_indices[0].push_back(x);
    for(size_t x = 0; x < mpo_dshape[1].size(); ++x) nz_indices[1].push_back(x);
    for(size_t x = 0; x < mpo_dshape[2].size(); ++x) nz_indices[2].push_back(x);
                                                     nz_indices[3] = r_nz_index;

    mpo = mpo.subarray(nz_indices);
    btas::Dshapes nz_index_save = nz_indices[0];

    mpo_dshape = mpo.check_net_dshape();
    nz_indices[0].clear(); r_nz_index.clear();
    for(size_t x = 0; x < mpo_dshape[0].size(); ++x)
      if(mpo_dshape[0][x] > 0) {
        nz_indices[0].push_back(x);
        r_nz_index.push_back(nz_index_save[x]);
      }

    nz_indices[3].clear();
    for(size_t x = 0; x < mpo_dshape[3].size(); ++x) nz_indices[3].push_back(x);

    mpo = mpo.subarray(nz_indices);

    size_t ldim = std::accumulate(mpo_dshape[0].begin(),mpo_dshape[0].end(),0);
    size_t rdim = std::accumulate(mpo_dshape[3].begin(),mpo_dshape[3].end(),0);
    cout << "done" << endl;
    cout << "\t\tL::" << setw(6) << ldim << ", R::" << setw(6) << rdim << " ( elements: " << mpo.nnz() << " ) " << endl;

    cout << "\t\tsaving site operator array..." << std::flush;
    save(mpo,getfile(opname,prefix,i));
    mpo.clear();
    cout << "done" << endl;
  }

    cout << "\t====================================================================================================" << endl;
    cout << "\t\tTotal number of operator elements: " << nnz << endl;
}

} // namespace mpsxx

#endif // __MPSXX_MPOGEN_HQC_IMPL_HPP
