#ifndef __MPSXX_OP_COMPONENT_HPP
#define __MPSXX_OP_COMPONENT_HPP

#include <btas/QSPARSE/QSTArray.h>

namespace mpsxx {
namespace mpogen {
namespace Fermionic {

/// Physical states of quantum chemistry Hamiltonian
enum SITE_OCCUPATION { vacuum = 0, alpha = 1, beta = 2, pair = 3 };

/// Non-zero component of primitive operator
struct op_component {

  op_component ()
  : bra(vacuum), ket(vacuum), scalar(0.0)
  { }

  /// Constructor
  op_component (const SITE_OCCUPATION& bra_, const SITE_OCCUPATION& ket_, const double& scalar_)
  : bra(bra_), ket(ket_), scalar(scalar_)
  { }

  /// Return true if bra index has odd particle number
  bool parity() const { return (bra == alpha || bra == beta); }

  SITE_OCCUPATION bra;   ///< bra index

  SITE_OCCUPATION ket;   ///< ket index

  double scalar;  ///< non-zero scalar element

};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Static objects for fermionic operators
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// indentity
struct I {
  const static btas::TVector<op_component,4> get ()
  {
    btas::TVector<op_component,4> elements;
    elements[0] = op_component(vacuum, vacuum, 1.0);
    elements[1] = op_component(alpha,  alpha,  1.0);
    elements[2] = op_component(beta,   beta,   1.0);
    elements[3] = op_component(pair,   pair,   1.0);
    return elements;
  }
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// create (alpha)
struct CreA {
  const static btas::TVector<op_component,2> get ()
  {
    btas::TVector<op_component,2> elements;
    elements[0] = op_component(alpha,  vacuum, 1.0);
    elements[1] = op_component(pair,   beta,  -1.0);
    return elements;
  }
};

/// destruct (alpha)
struct DesA {
  const static btas::TVector<op_component,2> get ()
  {
    btas::TVector<op_component,2> elements;
    elements[0] = op_component(vacuum, alpha,  1.0);
    elements[1] = op_component(beta,   pair,  -1.0);
    return elements;
  }
};

/// create (beta)
struct CreB {
  const static btas::TVector<op_component,2> get ()
  {
    btas::TVector<op_component,2> elements;
    elements[0] = op_component(beta,   vacuum, 1.0);
    elements[1] = op_component(pair,   alpha,  1.0);
    return elements;
  }
};

/// destruct (beta)
struct DesB {
  const static btas::TVector<op_component,2> get ()
  {
    btas::TVector<op_component,2> elements;
    elements[0] = op_component(vacuum, beta,   1.0);
    elements[1] = op_component(alpha,  pair,   1.0);
    return elements;
  }
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// create (alpha) x create (beta)
struct CreA_CreB {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(pair,   vacuum,-1.0);
    return elements;
  }
};

/// create (beta) x create (alpha): this is taken to be normal order
struct CreB_CreA {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(pair,   vacuum, 1.0);
    return elements;
  }
};

/// destruct (alpha) x destruct (beta): this is taken to be normal order
struct DesA_DesB {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(vacuum, pair,   1.0);
    return elements;
  }
};

/// destruct (beta) x destruct (alpha)
struct DesB_DesA {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(vacuum, pair,  -1.0);
    return elements;
  }
};

/// create (alpha) x destruct (alpha): counting operator (alpha)
struct CreA_DesA {
  const static btas::TVector<op_component,2> get ()
  {
    btas::TVector<op_component,2> elements;
    elements[0] = op_component(alpha,  alpha,  1.0);
    elements[1] = op_component(pair,   pair,   1.0);
    return elements;
  }
};

/// create (alpha) x destruct (beta): spin up operator
struct CreA_DesB {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(alpha,  beta,   1.0);
    return elements;
  }
};

/// create (beta) x destruct (alpha): spin down operator
struct CreB_DesA {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(beta,   alpha,  1.0);
    return elements;
  }
};

/// create (beta) x destruct (beta): counting operator (beta)
struct CreB_DesB {
  const static btas::TVector<op_component,2> get ()
  {
    btas::TVector<op_component,2> elements;
    elements[0] = op_component(beta,   beta,   1.0);
    elements[1] = op_component(pair,   pair,   1.0);
    return elements;
  }
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// create (alpha) x create (beta) x destruct (beta)
struct CreA_CreB_DesB {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(pair,   beta,  -1.0);
    return elements;
  }
};

/// create (beta) x create (alpha) x destruct (alpha)
struct CreB_CreA_DesA {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(pair,   alpha,  1.0);
    return elements;
  }
};

/// create (alpha) x destruct (alpha) x destruct (beta)
struct CreA_DesA_DesB {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(alpha,  pair,   1.0);
    return elements;
  }
};

/// create (beta) x destruct (beta) x destruct (alpha)
struct CreB_DesB_DesA {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(beta,   pair,  -1.0);
    return elements;
  }
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// create (alpha) x destruct (alpha) x create (beta) x destruct (beta): referred to chemist's notation
struct CreA_DesA_CreB_DesB {
  const static btas::TVector<op_component,1> get ()
  {
    btas::TVector<op_component,1> elements;
    elements[0] = op_component(pair,   pair,   1.0);
    return elements;
  }
};

} // namespace Fermionic

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Prime operator generator to construct a primitive site mpo
/// \tparam PrimeOp site operator type class, e.g. CreA, CreA_DesA, etc...
/// \param l  left  index
/// \param r  right index
/// \param factor scaling factor, set 1. by default
template<typename T, class PrimeOp, class Q>
void set_op_component (btas::QSTArray<T,4,Q>& op, const size_t& l, const size_t& r, const T& factor = 1.0)
{
  const Q& lq = op.qshape(0)[l];
//const Q& rq = op.qshape(3)[r];

  if(std::abs(factor) < 1.0e-16) return;

  btas::TArray<T,4> block(1,1,1,1);

  auto elements = PrimeOp::get();
  for(auto p : elements) {
    if(lq.parity() && p.parity())
      block =-factor * p.scalar;
    else
      block = factor * p.scalar;
    op.insert(btas::shape(l,p.bra,p.ket,r),block);
  }
}

} // namespace mpogen
} // namespace mpsxx

#endif // __MPSXX_OP_COMPONENT_HPP
