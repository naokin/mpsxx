#ifndef __MPSXX_PARSE_INTEGRAL_H
#define __MPSXX_PARSE_INTEGRAL_H

//
// parse molpro FCIDUMP file
//

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <btas/DENSE/TArray.h>

/// parse line to tokens
std::vector<std::string> gettoken (std::ifstream& fin);

/// parse orbital reorder file
void parse_reorder (std::ifstream& frord, std::vector<int>& reorder);

/// parse general 1-body integrals
void parse_oneint (std::ifstream& fint1, int& norbs, btas::TArray<double,2>& oneint);
/// parse general 1-body integrals w/ reorder
void parse_oneint (std::ifstream& fint1, int& norbs, btas::TArray<double,2>& oneint, const std::vector<int>& reorder);
/// parse general 2-body integrals
void parse_twoint (std::ifstream& fint2, int& norbs, btas::TArray<double,4>& twoint);
/// parse general 2-body integrals w/ reorder
void parse_twoint (std::ifstream& fint2, int& norbs, btas::TArray<double,4>& twoint, const std::vector<int>& reorder);

/// parse 1-el and 2-el integrals from MOLPRO's FCIDUMP file
void parse_fcidump (
        std::ifstream& fdump,
        int& norbs,
        int& nelec,
        double& ecore,
        std::vector<int>& irreps,
        btas::TArray<double,2>& oneint,
        btas::TArray<double,4>& twoint);
/// parse 1-el and 2-el integrals from MOLPRO's FCIDUMP file w/ reorder
void parse_fcidump (
        std::ifstream& fdump,
        int& norbs,
        int& nelec,
        double& ecore,
        std::vector<int>& irreps,
        btas::TArray<double,2>& oneint,
        btas::TArray<double,4>& twoint,
  const std::vector<int>& reorder);

/// write 1-el and 2-el integrals into MOLPRO's FCIDUMP format
void write_fcidump (
  std::ofstream& fdump,
  const int& norbs,
  const int& nelec,
  const double& ecore,
  const std::vector<int>& irreps,
  const btas::TArray<double,2>& oneint,
  const btas::TArray<double,4>& twoint);

#endif // __MPSXX_PARSE_INTEGRAL_H
