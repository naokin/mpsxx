#ifndef __MPSXX_MPOGEN_COMPRESS_HPP
#define __MPSXX_MPOGEN_COMPRESS_HPP

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

#include <boost/bind.hpp>
#include <boost/function.hpp>

#include <btas/QSPARSE/QSTArray.h>

#include "fileio.hpp"

namespace mpsxx {

/// Doing SVD compression on 2-site MPOs
/// \param isFwd true for forward compression, and vice versa.
/// \param isNrm if true, singular values are normalized.
/// \param x left MPOs.
/// \param y right MPOs.
/// \param D max. dim. to be kept in SVD.
/// \param tole tolerance of SVD, used only when D = 0 is specified.
template<typename T, class Q>
size_t mpogen_compress_twodot (
  const bool& isFwd,
  const bool& isNrm,
        btas::QSTArray<T,4,Q>& x,
        btas::QSTArray<T,4,Q>& y,
  const size_t& D,
  const typename btas::remove_complex<T>::type& tole = 1.0e-8)
{
  typedef typename btas::remove_complex<T>::type real_t;

  btas::QSTArray<T,6,Q> xy;
  btas::Gemm(CblasNoTrans,CblasNoTrans,1.0,x,y,1.0,xy);

  btas::STArray<real_t,1> s;
  if(isFwd)
    btas::Gesvd<T,6,4,Q,btas::QST::USE_L_QUANTA>(xy,s,x,y,D,tole);
  else
    btas::Gesvd<T,6,4,Q,btas::QST::USE_R_QUANTA>(xy,s,x,y,D,tole);

  // Normalization
  if(isNrm) {
    real_t f = 0.0;
    // loop over sparse block
    for(auto& sb : s) {
      // loop over elements in block
      for(auto& si : *sb.second) f += si*si;
    }
    f = 1.0/sqrt(f);
    // loop over sparse block
    for(auto& sb : s) {
      // loop over elements in block
      for(auto& si : *sb.second) si *= f;
    }
  }

  // Check net D size
  size_t DNet = 0;
  std::cout << "block = ";
  for(auto& sb : s) {
    size_t Di = sb.second->size();
    std::cout << Di << ":";
    DNet += Di;
  }
  std::cout << "NET(" << DNet << ")" << std::endl;

  if(isFwd)
    btas::Dimm(s,y);
  else
    btas::Dimm(x,s);

  return DNet;
}

/// Compression sweep for single MPOs
template<typename T, class Q>
std::vector<size_t> mpogen_compress (
  const bool& isNrm,
        std::vector<btas::QSTArray<T,4,Q>>& mpos,
  const size_t& D,
  const typename btas::remove_complex<T>::type& tole = 1.0e-8)
{
  size_t N = mpos.size();

  std::vector<size_t> DSav(N-1,0);

  bool   conv = false;
  size_t iter = 0;

  // Iteration until converge sizes of MPOs
  while(!conv) {
    std::cout << "\t====================================================================================================" << std::endl;
    std::cout << "\t\tSWEEP :: " << std::setw(3) << iter << std::endl;
    std::cout << "\t----------------------------------------------------------------------------------------------------" << std::endl;
    // forward sweep
    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
    std::cout << "\t\t\tFORWARD SWEEP" << std::endl;
    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
    std::vector<size_t> DFwd(N-1,0);
    for(size_t i = 0; i < N-1; ++i) {
      std::cout << "\t\t\tSITE [ " << std::setw(3) << i << " ] :: ";
      DFwd[i] = mpogen_compress_twodot(1,isNrm,mpos[i],mpos[i+1],D,tole);
    }
    // backward sweep
    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
    std::cout << "\t\t\tBACKWARD SWEEP" << std::endl;
    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
    std::vector<size_t> DBwd(N-1,0);
    for(size_t i = N-1; i > 0; --i) {
      std::cout << "\t\t\tSITE [ " << std::setw(3) << i << " ] :: ";
      DBwd[i-1] = mpogen_compress_twodot(0,isNrm,mpos[i-1],mpos[i],D,tole);
    }
    // check boundary sizes
    conv  = std::equal(DFwd.begin(),DFwd.end(),DSav.begin());
    conv &= std::equal(DBwd.begin(),DBwd.end(),DSav.begin());
    ++iter;
    DSav = DBwd;
  }
  return DSav;
}

/// Accumulate multi-MPOs into single MPOs with compression
/// \param mpos array of MPOs, on exit, shrink to single MPOs stored in 'mpos[0]'
template<typename T, class Q>
std::vector<size_t> mpogen_compress (
  const bool& isNrm,
        std::vector<std::vector<btas::QSTArray<T,4,Q>>>& mpos,
  const size_t& D,
  const typename btas::remove_complex<T>::type& tole = 1.0e-8)
{
  assert(mpos.size() > 0);

  size_t MAXBUF = 200;
  size_t N = mpos[0].size();

  std::vector<btas::QSTArray<T,4,Q>> comp(mpos[N-1]);

  std::vector<size_t> Ds(N-1,0);

  size_t nbuf = 1;

  for(size_t g = N-1; g > 0; --g) {

    // Delete the last MPOs
    mpos.pop_back();

    // Accumulate MPOs

    // For the first site
    {
      btas::QSTArray<T,4,Q> temp(comp[0]);
      comp[0].clear();
      btas::IVector<3> traceIdx = {0,1,2};
      btas::QSTdsum(mpos.back()[0],temp,traceIdx,comp[0]);
    }

    // Sweep 1 to N-1
    for(size_t i = 1; i < N-1; ++i) {
      btas::QSTArray<T,4,Q> temp(comp[i]);
      comp[i].clear();
      btas::IVector<2> traceIdx = {1,2};
      btas::QSTdsum(mpos.back()[i],temp,traceIdx,comp[i]);
    }

    // For the last site
    {
      btas::QSTArray<T,4,Q> temp(comp[N-1]);
      comp[N-1].clear();
      btas::IVector<3> traceIdx = {1,2,3};
      btas::QSTdsum(mpos.back()[N-1],temp,traceIdx,comp[N-1]);
    }

    if(++nbuf >= MAXBUF) {
      std::cout << "\t\tCompress and clean buffers..." << std::endl;
      Ds = mpogen_compress(isNrm,comp,D,tole);
      std::cout << "\t\t\t";
      for(size_t s = 0; s < Ds.size(); ++s) std::cout << Ds[s] << ":";
      std::cout << std::endl;
      nbuf = 1;
      mpos.shrink_to_fit(); // Deallocated the data finished to accumulate
    }
  }
  // Final compression
  std::cout << "\t\tCompress buffers to finalize..." << std::endl;
  Ds = mpogen_compress(isNrm,comp,D,tole);
  std::cout << "\t\t\t";
  for(size_t s = 0; s < Ds.size(); ++s) std::cout << Ds[s] << ":";
  std::cout << std::endl;

  mpos[0].swap(comp);

  return Ds;
}

template<typename T, class Q>
void mpogen_compress (const size_t& N, const std::string& opname, const std::string& prefix)
{
  std::vector<size_t> Msav(N-1,0);

  size_t iter = 0;
  bool conv = false;
  while(!conv) {
    std::cout << "\t====================================================================================================" << std::endl;
    std::cout << "\t\tMPO compression sweep :: " << iter << std::endl;
    btas::QSTArray<T,4,Q> lmpo;
    btas::QSTArray<T,4,Q> rmpo;

    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
    std::cout << "\t\t\tFORWARD SWEEP" << std::endl;
    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

    // forward sweep
    std::vector<size_t> Mfwd(N-1,0);
    load(lmpo,getfile(opname,prefix,0));
    for(size_t i = 0; i < N-1; ++i) {
      std::cout << "\t====================================================================================================" << std::endl;
      std::cout << "\t\tSITE [ " << std::setw(3) << i << " ] " << std::endl;
      std::cout << "\t----------------------------------------------------------------------------------------------------" << std::endl;
      std::cout << "\t\t\t";

      load(rmpo,getfile(opname,prefix,i+1));
      Mfwd[i] = mpogen_compress_twodot(1,0,lmpo,rmpo,0);
      save(lmpo,getfile(opname,prefix,i));
      lmpo = rmpo;
    }
    save(lmpo,getfile(opname,prefix,N-1));

    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
    std::cout << "\t\t\tBACKWARD SWEEP" << std::endl;
    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

    // backward sweep
    std::vector<size_t> Mbwd(N-1,0);
    for(size_t i = N-1; i > 0; --i) {
      std::cout << "\t====================================================================================================" << std::endl;
      std::cout << "\t\tSITE [ " << std::setw(3) << i << " ] " << std::endl;
      std::cout << "\t----------------------------------------------------------------------------------------------------" << std::endl;
      std::cout << "\t\t\t";

      load(lmpo,getfile(opname,prefix,i-1));
      Mbwd[i-1] = mpogen_compress_twodot(0,0,lmpo,rmpo,0);
      save(rmpo,getfile(opname,prefix,i));
      rmpo = lmpo;
    }
    save(rmpo,getfile(opname,prefix,0));

    conv  = std::equal(Mfwd.begin(),Mfwd.end(),Msav.begin());
    conv &= std::equal(Mbwd.begin(),Mbwd.end(),Msav.begin());

    ++iter;

    Msav = Mbwd;
  }

  size_t Msum = 0;
  std::cout << "\t\tCheck boundary size..." << std::endl;
  std::cout << "\t\t\tM = 1:";
  for(size_t s = 0; s < Msav.size(); ++s) {
    Msum += Msav[s];
    std::cout << Msav[s] << ":";
  }
  std::cout << "1" << std::endl;
}

/// Simple SVD MPOs compression (scratch base)
/// \param isNrm whether or not MPOs are normalized.
/// \param N size of lattice.
/// \param opname file name for MPOs to be compressed.
/// \param D max. dim. for each MPO boundary.
/// \param tole tolerance for SVD, used only when D = 0 is specified.
template<typename T, class Q>
void mpogen_compress (
  const bool& isNrm,
  const size_t& N,
  const std::string& opname,
  const std::string& prefix,
  const size_t& D,
  const typename btas::remove_complex<T>::type& tole = 1.0e-8)
{
  std::vector<size_t> DSav(N-1,0);

  bool   conv = false;
  size_t iter = 0;

  while(!conv) {
    std::cout << "\t====================================================================================================" << std::endl;
    std::cout << "\t\tMPO compression sweep :: " << iter << std::endl;
    std::vector<btas::QSTArray<T,4,Q>> lmpo;
    std::vector<btas::QSTArray<T,4,Q>> rmpo;

    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
    std::cout << "\t\t\tFORWARD SWEEP" << std::endl;
    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

    // forward sweep
    std::vector<size_t> DFwd(N-1,0);
    load(lmpo,getfile(opname,prefix,0));
    for(size_t i = 0; i < N-1; ++i) {
      std::cout << "\t====================================================================================================" << std::endl;
      std::cout << "\t\tSITE [ " << std::setw(3) << i << " ] " << std::endl;
      std::cout << "\t----------------------------------------------------------------------------------------------------" << std::endl;
      std::cout << "\t\t\t";

      load(rmpo,getfile(opname,prefix,i+1));
      DFwd[i] = mpogen_compress_twodot(1,isNrm,lmpo[0],rmpo[0],D,tole);
      save(lmpo,getfile(opname,prefix,i));
      lmpo = rmpo;
    }
    save(lmpo,getfile(opname,prefix,N-1));

    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
    std::cout << "\t\t\tBACKWARD SWEEP" << std::endl;
    std::cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

    // backward sweep
    std::vector<size_t> DBwd(N-1,0);
    for(size_t i = N-1; i > 0; --i) {
      std::cout << "\t====================================================================================================" << std::endl;
      std::cout << "\t\tSITE [ " << std::setw(3) << i << " ] " << std::endl;
      std::cout << "\t----------------------------------------------------------------------------------------------------" << std::endl;
      std::cout << "\t\t\t";

      load(lmpo,getfile(opname,prefix,i-1));
      DBwd[i-1] = mpogen_compress_twodot(0,isNrm,lmpo[0],rmpo[0],D,tole);
      save(rmpo,getfile(opname,prefix,i));
      rmpo = lmpo;
    }
    save(rmpo,getfile(opname,prefix,0));

    conv  = std::equal(DFwd.begin(),DFwd.end(),DSav.begin());
    conv &= std::equal(DBwd.begin(),DBwd.end(),DSav.begin());

    ++iter;

    DSav = DBwd;
  }

  size_t DNet = 0;
  std::cout << "\t\tCheck boundary size..." << std::endl;
  std::cout << "\t\t\tD = L:";
  for(size_t s = 0; s < DSav.size(); ++s) {
    DNet += DSav[s];
    std::cout << DSav[s] << ":";
  }
  std::cout << "R" << std::endl;
}

/// Variational MPOs compression (scratch base)
/// \param N size of lattice.
/// \param rfname file name for reference MPOs.
/// \param opname file name for MPOs to be compressed.
/// \param D max. dim. for each MPO boundary.
/// \param tole tolerance for SVD, used only when D = 0 is specified.
template<typename T, class Q>
void mpogen_compress (
  const size_t& N,
  const std::string& rfname,
  const std::string& opname,
  const std::string& prefix,
  const size_t& D,
  const typename btas::remove_complex<T>::type& tole = 1.0e-8)
{
  assert(false); // have not yet been implemented!
}

} // namespace mpsxx

#endif // __MPSXX_MPOGEN_COMPRESS_HPP
