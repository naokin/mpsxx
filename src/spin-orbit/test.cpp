#include <iostream>
#include <cstdlib>
#include <complex>

int main () {
  std::cout << "std::abs(1.5) = " << std::abs(1.5) << std::endl;
  std::cout << "std::abs(1.0+0.5i) = " << std::abs(std::complex<double>(1.0,0.5)) << std::endl;
  return 0;
}
