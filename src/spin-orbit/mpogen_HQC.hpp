#ifndef __MPSXX_MPOGEN_HQC_HPP
#define __MPSXX_MPOGEN_HQC_HPP

#include <iostream>
#include <sstream>
#include <vector>
#include <set>
#include <string>
#include <complex>

#include <boost/filesystem.hpp>

#include <btas/common/numeric_traits.h>
#include <btas/DENSE/TArray.h>
#include <btas/QSPARSE/QSTArray.h>

#include "mpidefs.h"
#include "fermion.hpp"
#include "mpogen_HQC_impl.hpp"
#include "mpogen_compress.hpp"

namespace mpsxx {

inline int getGroup (const size_t& Ndiv, const int& Norbs, const int& i, const int& j)
{
  int ix = i;
  int jx = j;
  if(ix > jx) std::swap(ix,jx);

  return (jx*(jx+1)/2+ix)%Ndiv;
}

inline int getGroup (const size_t& Ndiv, const int& Norbs, const int& i, const int& j, const int& k, const int& l)
{
  std::vector<int> index = { i, j, k, l };
  std::sort(index.begin(),index.end());
  const int& ix = index[0];
  const int& jx = index[1];
  const int& kx = Norbs-index[2];
  const int& lx = Norbs-index[3];

  int ij = jx*(jx+1)/2+ix;
  int kl = kx*(kx+1)/2+lx;

  if(ij < kl)
    return ij%Ndiv;
  else
    return kl%Ndiv;
}

/// Generate MPO's of quantum chemistry Hamiltonian
/// integrals are divided and distributed over cores
template<class Symmetry>
void mpogen_HQC (
  const size_t& Ndivs,
  const size_t& Norbs,
  const double& Ecore,
  const std::vector<Symmetry>& orbsym,
  const btas::TArray<double,2>& oneint,
  const btas::TArray<double,4>& twoint,
  const std::string& opname,
  const std::string& prefix,
  const bool& complexNumber,
  const bool& doSwitchSweepDir,
  const bool& doCompress)
{
  typedef fermion<Symmetry> qnum_t;

  std::vector<int> group;

  Communicator world;
  size_t nproc = world.size();
  size_t iproc = world.rank();

  btas::TArray<int,2> grp1e(Norbs,Norbs);
  grp1e.fill(-1);
  btas::TArray<int,4> grp2e(Norbs,Norbs,Norbs,Norbs);
  grp2e.fill(-1);

  if(world.rank() == 0) {
    // this is for sorting group index
    std::set<int> gset;
    // 1e integral group
    for(int i = 0; i < Norbs; ++i)
      for(int j = 0; j < Norbs; ++j) {
        if(fabs(oneint(i,j)) >= 1.0e-16) {
          int g = getGroup(Ndivs,Norbs,i,j);
          if(g >= 0) gset.insert(g);
          grp1e(i,j) = g;
        }
      }
    // 2e integral group
    for(int i = 0; i < Norbs; ++i)
      for(int k = 0; k < Norbs; ++k)
        for(int j = 0; j < Norbs; ++j)
          for(int l = 0; l < Norbs; ++l) {
            if(fabs(twoint(i,k,j,l)) >= 1.0e-16) {
              int g = getGroup(Ndivs,Norbs,i,k,j,l);
              if(g >= 0) gset.insert(g);
              grp2e(i,k,j,l) = g;
            }
          }
    // keep unique group index
    group.assign(gset.begin(),gset.end());
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,group,0);
  boost::mpi::broadcast(world,grp1e,0);
  boost::mpi::broadcast(world,grp2e,0);
#endif

  std::string tmpname = "SCRATCH";

  size_t gLocal = 0;
  for(size_t g = 0; g < group.size(); ++g) {

    // core energy is included in the group 0
    double e = 0.0; if(g == 0) e = Ecore;

    if(g%nproc == iproc) {
      btas::TArray<double,2> tmp1e(Norbs,Norbs);
      tmp1e.fill(0.0);
      btas::TArray<double,4> tmp2e(Norbs,Norbs,Norbs,Norbs);
      tmp2e.fill(0.0);
      // distribute 1e integrals over processors
      for(int i = 0; i < Norbs; ++i)
        for(int j = 0; j < Norbs; ++j)
          if(grp1e(i,j) == group[g])
            tmp1e(i,j) = oneint(i,j);
      // distribute 2e integrals over processors
      for(int i = 0; i < Norbs; ++i)
        for(int k = 0; k < Norbs; ++k)
          for(int j = 0; j < Norbs; ++j)
            for(int l = 0; l < Norbs; ++l)
              if(grp2e(i,k,j,l) == group[g])
                tmp2e(i,k,j,l) = twoint(i,k,j,l);

      std::ostringstream oss;
      oss << tmpname << "-" << gLocal;
      // generate MPO for the group 'group[g]'
      if(complexNumber) {
        mpogen_HQC_impl<std::complex<double>,Symmetry>(Norbs,e,orbsym,tmp1e,tmp2e,oss.str(),prefix,doSwitchSweepDir);
        // merge quantum numbers
        if(doCompress) mpogen_compress<std::complex<double>,qnum_t>(Norbs,oss.str(),prefix);
      }
      else {
        mpogen_HQC_impl<double,Symmetry>(Norbs,e,orbsym,tmp1e,tmp2e,oss.str(),prefix,doSwitchSweepDir);
        // merge quantum numbers
        if(doCompress) mpogen_compress<double,qnum_t>(Norbs,oss.str(),prefix);
      }
      // count up local MPO group
      ++gLocal;
    }
  }

  // Sorting out MPOs for each processor
  if(complexNumber) {
    mpogen_sort<std::complex<double>,qnum_t>(Norbs,gLocal,prefix,tmpname,opname);
  }
  else {
    mpogen_sort<double,qnum_t>(Norbs,gLocal,prefix,tmpname,opname);
  }
}

} // namespace mpsxx

#endif // __MPSXX_MPOGEN_HQC_HPP
