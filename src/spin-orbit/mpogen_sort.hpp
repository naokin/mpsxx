#ifndef __MPSXX_MPOGEN_SORT_HPP
#define __MPSXX_MPOGEN_SORT_HPP

#include <vector>
#include <cassert>
#include <sstream>

#include <boost/filesystem.hpp>
#include <btas/QSPARSE/QSTArray.h>
#include "fileio.hpp"

namespace mpsxx {

// Sorting out MPOs
// MPOs each of which is stored as a single MPO are sorted to be a single array of MPOs
template<typename T, class Q>
void mpogen_sort (
  const size_t& Norbs,
  const size_t& Ngroup,
  const std::string& prefix,
  const std::string& baseop,
  const std::string& sortop)
{
  // loop for site
  for(size_t i = 0; i < Norbs; ++i) {
    std::vector<btas::QSTArray<T,4,Q>> mpo(Ngroup);
    // loop for group
    for(size_t g = 0; g < Ngroup; ++g) {
      std::ostringstream oss;
      oss << baseop << "-" << g; // base MPOs are stored as name 'baseop'-g.*
      load(mpo[g],getfile(oss.str(),prefix,i));

      // Remove temporary MPO files...
      boost::filesystem::path path_to_scr(getfile(oss.str(),prefix,i));
      assert(boost::filesystem::exists(path_to_scr));
      boost::filesystem::remove(path_to_scr);
    }
    save(mpo,getfile(opname,prefix,i));
  }
}

} // namespace mpsxx

#endif // __MPSXX_MPOGEN_SORT_HPP
