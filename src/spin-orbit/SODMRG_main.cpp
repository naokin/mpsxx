#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

#include <btas/DENSE/TArray.h>

#include <time_stamp.h>

#include "mpidefs.h" // Communicator

#include "input.h"
#include "dmrg.hpp"
#include "mpogen_HQC.hpp"
#include "mpogen_HSO.hpp"
#include "fermion.hpp"
#include "point_group.h"
#include "parse_integral.h"

int main (int argc, char* argv[])
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

#ifndef _SERIAL
  boost::mpi::environment env(argc,argv);
#endif
  Communicator world;

  // file paths/names
  std::string fin = "dmrg.conf";
  std::string fout;
  std::string fcidump = "FCIDUMP";
  std::string hsodump = "HSODUMP";
  std::string reorder;
  std::string prefix = ".";

  // NOTE: HSODUMP format
  // -------------------------------------------------- 
  // NX
  //  1.00000000E+00
  // ...
  // NY
  //  1.00000000E+00
  // ...
  // NZ
  //  1.00000000E+00
  // ...

  // env. var. for MPOGEN
  bool doSwitchSweepDir = false;
  bool doCompress = false;
  bool doReorder = false;
  bool doSpinOrbit = false;

  // # divisions for k-MPO scheme
  size_t Ndivide = world.size();

  // print level
  size_t printLevel = 0;

  // parse command line input
  for(int iarg = 0; iarg < argc; ++iarg) {
    if(strcmp(argv[iarg],"-i") == 0) fin     = argv[++iarg];
    if(strcmp(argv[iarg],"-o") == 0) fout    = argv[++iarg];
    if(strcmp(argv[iarg],"-s") == 0) prefix  = argv[++iarg];
    if(strcmp(argv[iarg],"-f") == 0) fcidump = argv[++iarg];
    if(strcmp(argv[iarg],"-h") == 0) hsodump = argv[++iarg];
    if(strcmp(argv[iarg],"-r") == 0) reorder = argv[++iarg];
    if(strcmp(argv[iarg],"-x") == 0) doSwitchSweepDir = true;
    if(strcmp(argv[iarg],"-c") == 0) doCompress = true;
    if(strcmp(argv[iarg],"-v") == 0) printLevel = 1;
    if(strcmp(argv[iarg],"-n") == 0) Ndivide = atoi(argv[++iarg]);
  }
  if(!reorder.empty()) doReorder = true;
  if(!hsodump.empty()) doSpinOrbit = true;

  // parse DMRG input file
  mpsxx::DMRGInput input(fin);

  // assign cout as alias to ofst
  std::streambuf *backup;
  backup = cout.rdbuf();
  std::ofstream ofst;
  if(fout.size() > 0) {
    std::ostringstream oss;
    oss << fout << "." << world.rank();
    ofst.open(oss.str().c_str());
    cout.rdbuf(ofst.rdbuf());
  }

  // ================================================== 
  // ================================================== 

  // # orbitals
  int Norbs;
  // # electrons
  int Nelec;
  // core energy
  double Ecore;
  // irreducible representations of orbitals (sites)
  std::vector<int> irreps;
  // 1-el integrals
  btas::TArray<double,2> oneint;
  // 2-el integrals
  btas::TArray<double,4> twoint;

  // SO(1-el) integrals
  btas::TArray<double,2> sointX;
  btas::TArray<double,2> sointY;
  btas::TArray<double,2> sointZ;

  cout << "\t====================================================================================================" << endl;
  cout << "\t\tLoading integral information from " << fcidump << " and " << hsodump                                << endl;
  cout << "\t====================================================================================================" << endl;
  cout << endl;
  // parse integrals
  std::vector<int> orborder;
  if(world.rank() == 0) {
    std::ifstream ifsfcidump(fcidump.c_str());
    if(doReorder) {
      std::ifstream ifsreorder(reorder.c_str());
      parse_reorder(ifsreorder,orborder);
      //
      parse_fcidump(ifsfcidump,Norbs,Nelec,Ecore,irreps,oneint,twoint,orborder);
    }
    else {
      //
      parse_fcidump(ifsfcidump,Norbs,Nelec,Ecore,irreps,oneint,twoint);
    }
    if(doSpinOrbit) {
      // spin-orbit Hamiltonian (1-el)
      int dumNorbs;
      std::ifstream ifshsodump(hsodump.c_str());
      if(doReorder) {
        parse_oneint(ifshsodump,dumNorbs,sointX,orborder); assert(dumNorbs == Norbs);
        parse_oneint(ifshsodump,dumNorbs,sointY,orborder); assert(dumNorbs == Norbs);
        parse_oneint(ifshsodump,dumNorbs,sointZ,orborder); assert(dumNorbs == Norbs);
      }
      else {
        parse_oneint(ifshsodump,dumNorbs,sointX); assert(dumNorbs == Norbs);
        parse_oneint(ifshsodump,dumNorbs,sointY); assert(dumNorbs == Norbs);
        parse_oneint(ifshsodump,dumNorbs,sointZ); assert(dumNorbs == Norbs);
      }
    }
  }

  // broadcasting input info's
#ifndef _SERIAL
  boost::mpi::broadcast(world,Norbs,0);
  boost::mpi::broadcast(world,Nelec,0);
  boost::mpi::broadcast(world,Ecore,0);
  boost::mpi::broadcast(world,irreps,0);
  boost::mpi::broadcast(world,oneint,0);
  boost::mpi::broadcast(world,twoint,0);
  boost::mpi::broadcast(world,sointX,0);
  boost::mpi::broadcast(world,sointY,0);
  boost::mpi::broadcast(world,sointZ,0);
  boost::mpi::broadcast(world,orborder,0);
#endif

  // TODO: This is for core-ionized/excited state: temporary disabled...
//if(orborder.size() > 0) {
//  if(input.IonCore >= 0 && input.IonCore < Norbs)
//    input.IonCore = orborder[input.IonCore];
//}

  input.N_sites = Norbs;

  cout << "\t====================================================================================================" << endl;
  cout << "\t\tGenerating MPO: (div./cores) = (" << Ndivide << "/" << world.size() << ") "                         << endl;
  cout << "\t====================================================================================================" << endl;
  cout << endl;

  // Set point-group symmetry
       if(input.symmetry == "D2h") { mpsxx::PointGroup::set(mpsxx::_D2h); }
  else if(input.symmetry == "C2v") { mpsxx::PointGroup::set(mpsxx::_C2v); }
  else if(input.symmetry == "C2h") { mpsxx::PointGroup::set(mpsxx::_C2h); }
  else if(input.symmetry == "D2" ) { mpsxx::PointGroup::set(mpsxx::_D2 ); }
  else if(input.symmetry == "Cs" ) { mpsxx::PointGroup::set(mpsxx::_Cs ); }
  else if(input.symmetry == "C2" ) { mpsxx::PointGroup::set(mpsxx::_C2 ); }
  else if(input.symmetry == "Ci" ) { mpsxx::PointGroup::set(mpsxx::_Ci ); }
  else if(input.symmetry == "C1" ) { mpsxx::PointGroup::set(mpsxx::_C1 ); }
  else {
    cout << "Requested symmetry [" << input.symmetry << "] is not supported." << endl;
    abort();
  }
  // Set orbital symmetry
  std::vector<mpsxx::PointGroup> orbsym(irreps.size());
  for(size_t i = 0; i < irreps.size(); ++i) orbsym[i] = mpsxx::PointGroup(irreps[i]);

  // time-stamp object / clock starts here
  time_stamp ts;

  std::string hqc  = "hqc";
  std::string hsop = "hsop";
  std::string hsom = "hsom";
  std::string hsoz = "hsoz";

  if(!input.restart && !input.load_mpos) {
    // Non-rel. molecular Hamiltonian >> real
    mpsxx::mpogen_HQC(Ndivide,Norbs,Ecore,orbsym,oneint,twoint,hqc.c_str(),prefix,doSpinOrbit,doSwitchSweepDir,doCompress);
    world.barrier();
    if(doSpinOrbit) {
      // Spin-orbit, V(+)*T(+) >> complex
      mpsxx::mpogen_HSOp(Ndivide,Norbs,orbsym,sointX,sointY,hsop.c_str(),prefix,doSwitchSweepDir,doCompress);
      world.barrier();
      // Spin-orbit, V(-)*T(-) >> complex
      mpsxx::mpogen_HSOm(Ndivide,Norbs,orbsym,sointX,sointY,hsom.c_str(),prefix,doSwitchSweepDir,doCompress);
      world.barrier();
      // Spin-orbit, V(z)*T(z) >> imaginary
      mpsxx::mpogen_HSOz(Ndivide,Norbs,orbsym,sointZ,       hsoz.c_str(),prefix,doSwitchSweepDir,doCompress);
      world.barrier();
    }
    cout << endl;
    cout.precision(2);
    cout << "\t\t\tWall time for MPO construction: " << setw(8) << fixed << ts.lap() << endl;
  }

  // Calling DMRG main-routine

  input.energy = mpsxx::dmrg<fermion<mpsxx::PointGroup>>
  (
    input.restart,

    doSpinOrbit,
    hqc.c_str(),
    hsop.c_str(),
    hsom.c_str(),
    hsoz.c_str(),

    fermion<mpsxx::PointGroup>(
      input.N_elecs,
      input.N_spins,
      mpsxx::PointGroup(input.irrep)),

    input.algorithm,
    input.N_sites,
    input.N_max_states,
    prefix,
    input.N_roots,
    input.tolerance,
    input.noise,
    input.shift,
    input.N_max_sweep_iter
  );

  world.barrier();

  cout << endl;
  cout.precision(2);
  cout << "\t\t\tWall time for DMRG optimization: " << setw(8) << fixed << ts.lap() << endl;
  cout << "\t\t\tWall time for Total calculation: " << setw(8) << fixed << ts.elapsed() << endl;

  // ================================================== 
  // ================================================== 

  // assign cout back to standard output
  cout.rdbuf(backup);
  ofst.close();

  return 0;
}
