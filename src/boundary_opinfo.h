#ifndef __MPSXX_MPOGEN_BOUNDARY_OPINFO_H
#define __MPSXX_MPOGEN_BOUNDARY_OPINFO_H 1

#include <iostream>
#include <map>
#include <cstdlib>

#include <btas/QSPARSE/Qshapes.h>

#include "bit_operator_type.h"
#include "fermion.hpp"

namespace mpsxx {

/// DMRG operators at certain boundary
class boundary_opinfo {

public:

  typedef std::map<mpogen::BIT_OPERATOR_TYPE,size_t> bop_map_t;

  typedef bop_map_t::iterator iterator;

  typedef bop_map_t::const_iterator const_iterator;

  /// Direction
  enum DIRECTION {
    BACKWARD = 0, ///< left block >  right block
    FORWARD  = 1, ///< left block <= right block
    NOBOUND  = 2  ///< for single site
  };

  /// Default constructor
  boundary_opinfo() : m_bn_dir(NOBOUND) { }
  /// Constructor with site index
  explicit boundary_opinfo(size_t index, bool hubbard = false) { reset(index,hubbard); }
  /// Constructor with size
  boundary_opinfo(size_t L, size_t N, bool ENABLE_SWAP_SWEEP = true, bool hubbard = false) { reset(L,N,ENABLE_SWAP_SWEEP,hubbard); }
  /// Copy constructor
  boundary_opinfo(const boundary_opinfo& x) : m_bn_dir(x.m_bn_dir), m_bn_ops(x.m_bn_ops) { }

  /// Reset operators for single site
  void reset(size_t index, bool hubbard = false);
  /// Reset operators
  void reset(size_t L, size_t N, bool ENABLE_SWAP_SWEEP = true, bool hubbard = false);

  /// Get/Set direction
  inline const DIRECTION& direction() const { return m_bn_dir; }
  inline       DIRECTION& direction()       { return m_bn_dir; }

  /// Number of operators
  inline size_t size() const { return m_bn_ops.size(); }

  //  Iterator functions

  inline const_iterator begin() const { return m_bn_ops.begin(); }
  inline       iterator begin()       { return m_bn_ops.begin(); }
  inline const_iterator end  () const { return m_bn_ops.end  (); }
  inline       iterator end  ()       { return m_bn_ops.end  (); }

  inline const_iterator find(mpogen::BIT_OPERATOR_TYPE _type) const { return m_bn_ops.find(_type); }
  inline       iterator find(mpogen::BIT_OPERATOR_TYPE _type)       { return m_bn_ops.find(_type); }

  void clean(const btas::Dshapes& _dn_shape);

private:

  DIRECTION m_bn_dir;

  bop_map_t m_bn_ops;

};

template<class Symmetry>
btas::Qshapes<fermion<Symmetry>> get_qshape (const boundary_opinfo& info, const std::vector<Symmetry>& orbsym)
{
  using namespace mpogen::bit_operator;

  btas::Qshapes<fermion<Symmetry>> q(info.size());
  for(auto it = info.begin(); it != info.end(); ++it) {

    Symmetry sym = Symmetry::zero();

    mpogen::BIT_OPERATOR_TYPE op = it->first;
    int p = 0;
    int s = 0;
    if((op & IDEN) == 0) { // except for I and H
      if(op & SINGLE) {
        mpogen::BIT_OPERATOR_TYPE op1st = (op & FIRST) >> INDEX_SHIFT;
        if(op1st & 0x2000)
        { ++p; if(op1st & 0x1000) ++s; else --s; }
        else
        { --p; if(op1st & 0x1000) --s; else ++s; }
        sym = sym * orbsym[op1st & INDEX];
      }
      if(op & SINGLE_2) {
        mpogen::BIT_OPERATOR_TYPE op2nd = (op & SECOND);
        if(op2nd & 0x2000)
        { ++p; if(op2nd & 0x1000) ++s; else --s; }
        else
        { --p; if(op2nd & 0x1000) --s; else ++s; }
        sym = sym * orbsym[op2nd & INDEX];
      }
    }
    q.at(it->second) = fermion<Symmetry>(p,s,sym);
  }

  return std::move(q);
}

} // namespace mpsxx

std::ostream& operator<< (std::ostream& ost, const mpsxx::boundary_opinfo& info);

#endif // __MPSXX_MPOGEN_BOUNDARY_OPINFO_H
