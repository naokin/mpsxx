#ifndef __BACK_TRACKING_LINE_SEARCH_HPP
#define __BACK_TRACKING_LINE_SEARCH_HPP

#include <iostream>
#include <iomanip>

#include <cmath>
#include <vector>
#include <map>
#include <algorithm>

#include <mkl_cblas.h>
#include <mkl_lapacke.h>

/// Back-Tracking Line Search for Maximization
struct BTLS {

  typedef std::vector<double> Vector;

  /// \param x a starting position on input, the solution on exit
  /// \return alpha
  template<class Func>
  BTLS (Func F, Vector& x, const Vector& p, double& alpha, const double& m, size_t MAXIT = 10, double c = 0.5, double tau = 0.618)
  {
    std::cout << "\tBacktracking Line-Search " << std::endl;

    size_t N = x.size();
    size_t iter = 0;
    size_t imax = MAXIT;
    double atmp = alpha;
    std::map<double,double> aopt;
    double f0 = F(x);
    double fm = f0;
    while(iter < MAXIT) {
      Vector xi = x;
      cblas_daxpy(N,atmp,p.data(),1,xi.data(),1);
      double fn = F(xi); // the second point
      if(fn > fm) { imax = iter; fm = fn; }
      aopt.insert(std::make_pair(fn,atmp));
      double AG = fn-f0-c*m*atmp; // Armijo-Goldstein condition
      if(AG >= 0.0) break;
      // repeat
      ++iter;
      atmp *= tau;
    }
    alpha = aopt.rbegin()->second;
    cblas_daxpy(N,alpha,p.data(),1,x.data(),1);
    // increase alpha
    if(imax == 0) alpha /= tau;
  }
};

#endif // __BACK_TRACKING_LINE_SEARCH_HPP
