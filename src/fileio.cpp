#include <sstream>
#include <boost/filesystem.hpp>

#include "mpidefs.h"
#include "fileio.h"

void removefile (const std::string& f_name)
{
  boost::filesystem::path path_to_scr(f_name);
  if(boost::filesystem::exists(path_to_scr))
    boost::filesystem::remove(path_to_scr);
}

std::string mpsxx::getfile (const std::string& name, const std::string& prefix, int site, int iState, int jState)
{
  Communicator world;
  std::stringstream filename;
  filename << prefix << "/" << name << "." << site << "." << iState << "." << jState << "." << world.rank() << ".tmp";
  return filename.str();
}
