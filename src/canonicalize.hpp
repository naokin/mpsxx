#ifndef __MPSXX_CANONICALIZE_HPP
#define __MPSXX_CANONICALIZE_HPP

#include <random>
#include <functional>
#include <boost/random.hpp>

#include <btas/QSPARSE/QSTArray.h>

namespace mpsxx {

/// Add random noise to MPS
template<size_t N, class Q>
void randomize (btas::QSTArray<double,N,Q>& wfnc, const double& noise)
{
  if(noise < 1.0e-12) return;

  boost::random::mt19937 rgen;
  boost::random::uniform_real_distribution<double> dist(-1.0,1.0);

  auto ds = wfnc.check_net_dshape();
  auto qs = wfnc.qshape();
  for(size_t i = 1; i < N-1; ++i) {
    ds[i] = btas::Dshapes(qs[i].size(),1);
  }

  btas::QSTArray<double,N,Q> wfpt(wfnc.q(),qs,ds,std::bind(dist,rgen));
  btas::Normalize(wfpt);

  btas::Axpy(noise,wfpt,wfnc);
}

/// Canonicalize two-site MPS
template<class Q>
void canonicalize (
  const bool& forward,
        btas::QSTArray<double,4,Q>& wfnc,
        btas::QSTArray<double,3,Q>& lmps,
        btas::QSTArray<double,3,Q>& rmps,
  const size_t& M,
  const double& T = 1.0e-12,
  const double& noise = 0.0)
{
//randomize(wfnc,noise);

  btas::STArray<double,1> s;
  if(forward) {
    btas::Gesvd<double,4,3,Q,btas::QST::USE_L_QUANTA>(wfnc,s,lmps,rmps,M,T);
    btas::Dimm(s,rmps);
  }
  else {
    btas::Gesvd<double,4,3,Q,btas::QST::USE_R_QUANTA>(wfnc,s,lmps,rmps,M,T);
    btas::Dimm(lmps,s);
  }
}

/// Canonicalize one-site MPS
template<class Q>
void canonicalize (
  const bool& forward,
        btas::QSTArray<double,3,Q>& wfnc,
        btas::QSTArray<double,3,Q>& lmps,
        btas::QSTArray<double,3,Q>& rmps,
  const size_t& M,
  const double& T = 1.0e-12,
  const double& noise = 0.0)
{
  if(noise > 0.0) {
    btas::QSTArray<double,4,Q> temp;
    if(forward) {
      btas::Gemm(CblasNoTrans,CblasNoTrans,1.0,wfnc,rmps,1.0,temp);
    }
    else {
      btas::Gemm(CblasNoTrans,CblasNoTrans,1.0,lmps,wfnc,1.0,temp);
    }
    return canonicalize(forward,temp,lmps,rmps,M,noise);
  }

  btas::STArray<double,1> s;
  btas::QSTArray<double,2,Q> g;
  if(forward) {
    btas::Gesvd<double,3,3,Q,btas::QST::USE_L_QUANTA>(wfnc,s,lmps,g,M,T);
    btas::Dimm(s,g);
    btas::QSTArray<double,3,Q> temp;
    btas::Gemm(CblasNoTrans,CblasNoTrans,1.0,g,rmps,1.0,temp);
    rmps = temp;
  }
  else {
    btas::Gesvd<double,3,2,Q,btas::QST::USE_R_QUANTA>(wfnc,s,g,rmps,M,T);
    btas::Dimm(g,s);
    btas::QSTArray<double,3,Q> temp;
    btas::Gemm(CblasNoTrans,CblasNoTrans,1.0,lmps,g,1.0,temp);
    lmps = temp;
  }
}

//! Canonicalize merged MPS
template<class Q>
void canonicalize (
  const bool& forward,
        btas::QSTArray<double,2,Q>& wfnc,
        btas::QSTArray<double,2,Q>& lmat,
        btas::QSTArray<double,2,Q>& rmat,
  const size_t& M,
  const double& T = 1.0e-12,
  const double& noise = 0.0)
{
//randomize(wfnc,noise);

  btas::STArray<double,1> s;
  if(forward) {
    btas::Gesvd<double,2,2,Q,btas::QST::USE_L_QUANTA>(wfnc,s,lmat,rmat,M,T);
    btas::Dimm(s,rmat);
  }
  else {
    btas::Gesvd<double,2,2,Q,btas::QST::USE_R_QUANTA>(wfnc,s,lmat,rmat,M,T);
    btas::Dimm(lmat,s);
  }
}

} // namespace mpsxx

#endif // __MPSXX_CANONICALIZE_HPP
