#ifndef __MPSXX_POINT_GROUP_C2H_H
#define __MPSXX_POINT_GROUP_C2H_H

#include <string>

#include "point_group.h"

namespace mpsxx {
namespace PG {

struct C2h {

  /// irrep #, the same as Molpro order.
  enum IRREP_ELEMENTS {
    Ag = 0,
    Au = 1,
    Bu = 2,
    Bg = 3,
    ER = 0xffffffff
  };

  static void set (size_t& N_rep, std::vector<IRREP>& table, std::vector<std::string>& label)
  {
    N_rep = 4;

    table = {
      Ag, Au, Bu, Bg,
      Au, Ag, Bg, Bu,
      Bu, Bg, Ag, Au,
      Bg, Bu, Au, Ag
    };

    label.resize(N_rep);
    label[0] = "Ag";
    label[1] = "Au";
    label[2] = "Bu";
    label[3] = "Bg";
  }

};

} // namespace PG
} // namespace mpsxx

#endif // __MPSXX_POINT_GROUP_C2H_H
