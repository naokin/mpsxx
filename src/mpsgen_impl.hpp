#ifndef __MPSXX_MPSGEN_IMPL_HPP
#define __MPSXX_MPSGEN_IMPL_HPP

#include <iostream>
#include <iomanip>
#include <vector>

#include <boost/random.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>

#include <btas/QSPARSE/QSTArray.h>

#include "mpidefs.h"
#include "fileio.h"

#include "canonicalize.hpp"
#include "renormalize.hpp"

namespace mpsxx {

/// Generate quantum numbers for each boundary from MPO
///
/// \param sites quantum numbers for each site (physical index)
/// \param qt total quantum number of lattice
/// \param max_quantum_blocks maximum number of quantum blocks on each boundary (0: no limitation by default)
template<class Q>
std::vector<btas::Qshapes<Q>> gen_quantum_states (const std::vector<btas::Qshapes<Q>>& sites, const Q& qt, size_t max_quantum_blocks = 0)
{
  size_t N = sites.size();

  // zero quantum number
  btas::Qshapes<Q> qz(1, Q::zero());

  // boundary quantum numbers
  std::vector<btas::Qshapes<Q>> qb(N);

  // generate quantum number blocks for MPS
  qb[0] = sites[0];
  for(size_t i = 1; i < N-1; ++i) {
    btas::Qshapes<Q> qb_bare = qb[i-1] & sites[i]; // get unique elements of { q(l) x q(n) }
    qb[i].clear();
    qb[i].reserve(qb_bare.size());
    for(size_t j = 0; j < qb_bare.size(); ++j)
      if(qb_bare[j].p() <= qt.p()) qb[i].push_back(qb_bare[j]);
  }
  qb[N-1] = btas::Qshapes<Q>(1, qt); // used for checking quantum states

  // reduce zero quantum blocks
  for(size_t i = N-1; i > 0; --i) {
    const btas::Qshapes<Q>& qn = sites[i];
          btas::Qshapes<Q>& ql = qb[i-1];
          btas::Qshapes<Q>& qr = qb[i];

    // check non-zero for each ql index
    auto lt = ql.begin();
    while(lt != ql.end()) {
      bool non_zero = false;
      for(size_t p = 0; p < qn.size(); ++p) {
        for(size_t r = 0; r < qr.size(); ++r) {
          non_zero |= (qr[r] == (qn[p] * (*lt)));
        }
      }
      if(non_zero)
        ++lt;
      else
        ql.erase(lt);
    }
    assert(ql.size() > 0);
  }
  for(size_t i = 0; i < N-1; ++i) {
    const btas::Qshapes<Q>& qn = sites[i];
          btas::Qshapes<Q>& ql = qb[i];
          btas::Qshapes<Q>& qr = qb[i+1];

    // further reduction
    if(max_quantum_blocks > 0 && ql.size() > max_quantum_blocks) {
      size_t offs = (ql.size()-max_quantum_blocks)/2;
      ql = btas::Qshapes<Q>(ql.begin()+offs, ql.begin()+offs+max_quantum_blocks);

      // check non-zero for each qr index
      auto rt = qr.begin();
      while(rt != qr.end()) {
        bool non_zero = false;
        for(size_t l = 0; l < ql.size(); ++l) {
          for(size_t p = 0; p < qn.size(); ++p) {
            non_zero |= (*rt == (ql[l] * qn[p]));
          }
        }
        if(non_zero)
          ++rt;
        else
          qr.erase(rt);
      }
      assert(qr.size() > 0);
    }
  }
  return qb;
}

/// Generate random MPS for a specified root
/// \param opname file name of MPO object
/// \param prefix file prefix
/// \param N number of sites
/// \param iroot target root
/// \param qt total quantum number
/// \param M number of states to be kept (0 to keep all states).
template<class Q>
void make_random_mpss (
  const std::string& opname,
  const std::string& prefix,
  const size_t& N,
  const size_t& iroot,
  const Q& qt,
  const int& M = 0)
{
  using std::cout;
  using std::endl;
  using std::setw;

  boost::random::mt19937 rgen;
  boost::random::uniform_real_distribution<double> dist(-1.0,1.0);

  cout << "\t====================================================================================================" << endl;
  cout << "\t\t\tGenerate & initialize MPS (Random MPSs): number of sites = " << setw(3) << N << endl;
  cout << "\t====================================================================================================" << endl;

  btas::QSTArray<double,3,Q> wfnc;

  btas::Qshapes<Q> qz(1,Q::zero());

  btas::Qshapes<Q> ql(1,Q::zero());
  btas::Dshapes    dl(ql.size(),1);

  btas::Qshapes<Q> qr;
  btas::Dshapes    dr;

  std::vector<btas::Qshapes<Q>> qn(N);
  std::vector<btas::Dshapes>    dn(N);

  for(size_t i = 0; i < N; ++i) {
    // load MPO to get physical states
    std::vector<btas::QSTArray<double,4,Q>> dummy;
    load(dummy,getfile(opname,prefix,i));

    qn[i] = dummy[0].qshape(1);
    dn[i] = btas::Dshapes(qn[i].size(),1);
  }

  cout << "\t\t\tGenerating quantum states for each boundary " << endl;
  std::vector<btas::Qshapes<Q>> qb = gen_quantum_states(qn,qt,M);
  cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

  btas::TVector<btas::Qshapes<Q>,3> q_shape;
  btas::TVector<btas::Dshapes,   3> d_shape;

  for(size_t i = 0; i < N-1; ++i) {
    cout << "\t\t\tGenerating   MPS for site [ " << setw(3) << i << " ] " << endl;
    cout << "\t----------------------------------------------------------------------------------------------------" << endl;
    qr = qb[i];
    dr = btas::Dshapes(qr.size(),1);

    q_shape = btas::make_array(ql,qn[i],-qr);
    d_shape = btas::make_array(dl,dn[i], dr);

    wfnc.resize(Q::zero(),q_shape,d_shape,boost::bind(dist,rgen));
    btas::Normalize(wfnc);
    save(wfnc,getfile("wave",prefix,i,iroot));
    wfnc.clear();

    ql = qr;
    dl = dr;
  }

  cout << "\t\t\tGenerating   MPS for site [ " << setw(3) << N-1 << " ] " << endl;
  cout << "\t====================================================================================================" << endl;

  qr = qz; // qb[N-1] is not used
  dr = btas::Dshapes(qr.size(),1);

  q_shape = btas::make_array(ql,qn[N-1],-qr);
  d_shape = btas::make_array(dl,dn[N-1], dr);

  wfnc.resize(qt,q_shape,d_shape,boost::bind(dist,rgen)); // set qt as a total quantum number of array
  btas::Normalize(wfnc);
  save(wfnc,getfile("wave",prefix,N-1,iroot));
}

template<class Q>
void make_slater_mpss (
  const std::string& opname,
  const std::string& prefix,
  const size_t& N,
  const size_t& iroot,
  const Q& qt,
  const std::vector<Q>& guess)
{
  using std::cout;
  using std::endl;
  using std::setw;

  cout << "\t====================================================================================================" << endl;
  cout << "\t\t\tGenerate & initialize MPS (Slater MPSs): number of sites = " << setw(3) << N << endl;
  cout << "\t====================================================================================================" << endl;

  btas::QSTArray<double,3,Q> wfnc;
  btas::Qshapes<Q> qz(1,Q::zero());

  btas::Qshapes<Q> ql(1,Q::zero());
  btas::Dshapes    dl(ql.size(),1);

  btas::Qshapes<Q> qr;
  btas::Dshapes    dr;

  std::vector<btas::Qshapes<Q>> qn(N);
  std::vector<btas::Dshapes>    dn(N);

  for(size_t i = 0; i < N; ++i) {
    // load MPO to get physical states
    std::vector<btas::QSTArray<double,4,Q>> dummy;
    load(dummy,getfile(opname,prefix,i));

    qn[i] = dummy[0].qshape(1);
    dn[i] = btas::Dshapes(qn[i].size(),1);
  }

  assert(guess.size() == N);
  //
  cout << "\t\t\tGenerating quantum states for each boundary " << endl;
  std::vector<btas::Qshapes<Q>> qb(N);
  qb[0].push_back(guess[0]);
  for(size_t i = 1; i < N; ++i)
    qb[i].push_back(qb[i-1][0]*guess[i]);
  cout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
  //
  assert(qb[N-1][0] == qt);

  btas::TVector<btas::Qshapes<Q>,3> q_shape;
  btas::TVector<btas::Dshapes,   3> d_shape;

  for(size_t i = 0; i < N-1; ++i) {
    cout << "\t\t\tGenerating   MPS for site [ " << setw(3) << i << " ] " << endl;
    cout << "\t----------------------------------------------------------------------------------------------------" << endl;
    qr = qb[i];
    dr = btas::Dshapes(qr.size(),1);

    q_shape = btas::make_array(ql,qn[i],-qr);
    d_shape = btas::make_array(dl,dn[i], dr);

    wfnc.resize(Q::zero(),q_shape,d_shape,1.0);
    btas::Normalize(wfnc);
    save(wfnc,getfile("wave",prefix,i,iroot));
    wfnc.clear();

    ql = qr;
    dl = dr;
  }

  cout << "\t\t\tGenerating   MPS for site [ " << setw(3) << N-1 << " ] " << endl;
  cout << "\t====================================================================================================" << endl;

  qr = qz; // qb[N-1] is not used
  dr = btas::Dshapes(qr.size(),1);

  q_shape = btas::make_array(ql,qn[N-1],-qr);
  d_shape = btas::make_array(dl,dn[N-1], dr);

  wfnc.resize(qt,q_shape,d_shape,1.0); // set qt as a total quantum number of array
  btas::Normalize(wfnc);
  save(wfnc,getfile("wave",prefix,N-1,iroot));
}

/// Generate MPSs and initialize operators
template<class Q>
void mpsgen_impl (
  const std::string& opname,
  const std::string& prefix,
  const size_t& N,
  const size_t& iroot,
  const Q& qt,
  const std::vector<Q>& guess,
  const int& M = 0)
{
  using std::endl;
  using std::setw;

  Communicator world;

  btas::Qshapes<Q> qz(1,Q::zero());

  btas::QSTArray<double,3,Q> wfnc;
  if(world.rank() == 0) {
    if(guess.empty() || iroot > 0) // For excited state, slater mpss hasn't yet implemented
      make_random_mpss(opname,prefix,N,iroot,qt,M);
    else
      make_slater_mpss(opname,prefix,N,iroot,qt,guess);
    //
    load(wfnc,getfile("wave",prefix,N-1,iroot));
  }

  // peak group size
  size_t ngroup = 0;
  {
    std::vector<btas::QSTArray<double,4,Q>> dummy;
    load(dummy,getfile(opname,prefix,0));
    ngroup = dummy.size();
  }

  // Initial canonicalization
  std::vector<std::vector<btas::QSTArray<double,3,Q>>> rH0(iroot+1);
  std::vector<btas::QSTArray<double,2,Q>> rS0(iroot+1);
  for(size_t k = 0; k <= iroot; ++k) {
    rH0[k].resize(ngroup);
    for(size_t g = 0; g < ngroup; ++g) {
      rH0[k][g].resize(Q::zero(),btas::make_array(qz,qz,qz));
      rH0[k][g].insert(btas::shape(0ul,0ul,0ul),btas::TArray<double,3>(1,1,1));
      rH0[k][g].fill(1.0);
    }
    save(rH0[k],getfile("right-H",prefix,N-1,iroot,k));
    if(world.rank() == 0) {
      rS0[k].resize(Q::zero(),btas::make_array(qz,qz));
      rS0[k].insert(btas::shape(0ul,0ul),btas::TArray<double,2>(1,1));
      rS0[k].fill(1.0);
      save(rS0[k],getfile("right-S",prefix,N-1,iroot,k));
    }
  }

  for(size_t i = N-1; i > 0; --i) {
    pout << "\t\t\tInitializing MPS for site [ " << setw(3) << i << " ] " << endl;
    pout << "\t----------------------------------------------------------------------------------------------------" << endl;
    btas::QSTArray<double,3,Q> rmps;
    btas::QSTArray<double,3,Q> lmps;
    if(world.rank() == 0) {
      load(lmps,getfile("wave",prefix,i-1,iroot));

      canonicalize(0,wfnc,lmps,rmps,M);
      wfnc = lmps;
      btas::Normalize(wfnc);

      save(wfnc,getfile("wave",prefix,i-1,iroot));
      save(rmps,getfile("rmps",prefix,i,iroot));
    }
#ifndef _SERIAL
    boost::mpi::broadcast(world,wfnc,0);
    boost::mpi::broadcast(world,rmps,0);
#endif

    std::vector<btas::QSTArray<double,4,Q>> mpo;
    load(mpo,getfile(opname,prefix,i));
    assert(mpo.size() == ngroup);

    for(size_t k = 0; k <= iroot; ++k) {
      btas::QSTArray<double,3,Q> kmps;
      if(world.rank() == 0) {
        load(kmps,getfile("rmps",prefix,i,k));
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,kmps,0);
#endif

      std::vector<btas::QSTArray<double,3,Q>> rH1(ngroup);
      for(size_t g = 0; g < ngroup; ++g) {
        renormalize(0,mpo[g],rH0[k][g],rmps,kmps,rH1[g]);
      }
      save(rH1,getfile("right-H",prefix,i-1,iroot,k));
      rH0[k] = rH1;

      if(world.rank() == 0) {
        btas::QSTArray<double,2,Q> rS1;
        renormalize(0,rS0[k],rmps,kmps,rS1);
        save(rS1,getfile("right-S",prefix,i-1,iroot,k));
        rS0[k] = rS1;
      }
    }
    world.barrier();
  }
  pout << "\t\t\tInitializing MPS for site [ " << setw(3) << 0 << " ] " << endl;
  pout << "\t====================================================================================================" << endl;

  std::vector<std::vector<btas::QSTArray<double,3,Q>>> lH0(iroot+1);
  std::vector<btas::QSTArray<double,2,Q>> lS0(iroot+1);
  for(size_t k = 0; k <= iroot; ++k) {
    lH0[k].resize(ngroup);
    for(size_t g = 0; g < ngroup; ++g) {
      lH0[k][g].resize(Q::zero(),btas::make_array(qz,qz,qz));
      lH0[k][g].insert(btas::shape(0ul,0ul,0ul),btas::TArray<double,3>(1,1,1));
      lH0[k][g].fill(1.0);
    }
    save(lH0[k],getfile("left-H",prefix,0,iroot,k));
    if(world.rank() == 0) {
      lS0[k].resize(Q::zero(),btas::make_array(qz,qz));
      lS0[k].insert(btas::shape(0ul,0ul),btas::TArray<double,2>(1,1));
      lS0[k].fill(1.0);
      save(lS0[k],getfile("left-S",prefix,0,iroot,k));
    }
  }
}

} // namespace mpsxx

#endif // __MPSXX_MPSGEN_IMPL_HPP
