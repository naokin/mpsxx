#ifndef __MPSXX_POINT_GROUP_H
#define __MPSXX_POINT_GROUP_H

#include <iostream>
#include <cassert>
#include <string>
#include <vector>

#include <boost/serialization/serialization.hpp>

namespace mpsxx {

typedef unsigned int IRREP;

enum GROUP { _C1, _Ci, _C2, _Cs, _D2, _C2h, _C2v, _D2h };

/// Point group symmetry
class PointGroup {

private:

  friend class boost::serialization::access;

  template<class Archive>
  void serialize (Archive& ar, const unsigned int version) { ar & irrep_; }

public:

  static size_t size () { return N_reps_; }

  static PointGroup zero () { return PointGroup(0); }

  static void set (GROUP g);

  explicit
  PointGroup (const IRREP& irrep = 0) : irrep_(irrep)
  { assert(irrep < N_reps_); }

  PointGroup (const PointGroup& x) : irrep_(x.irrep_)
  { }

// FIXME: In C++11, copy assignment should be implemented with move assignment?
//        For the time, I used implicit copy assignment.
//PointGroup& operator= (const PointGroup& x)
//{ irrep_ = x.irrep_; return *this; }

  bool operator== (const PointGroup& x) const { return irrep_ == x.irrep_; }
  bool operator!= (const PointGroup& x) const { return irrep_ != x.irrep_; }
  bool operator<  (const PointGroup& x) const { return irrep_ <  x.irrep_; }
  bool operator>  (const PointGroup& x) const { return irrep_ >  x.irrep_; }

  PointGroup& operator*= (const PointGroup& x) { irrep_ = product_table_[irrep_*N_reps_+x.irrep_]; return *this; }

  const IRREP& irrep() const { return irrep_; }

  std::string label () const { return label_[irrep_]; }

private:

  /// Symmetry group
  static GROUP symm_;

  /// Number of irreps
  static size_t N_reps_;

  /// Product table
  static std::vector<IRREP> product_table_;

  /// Irrep labels
  static std::vector<std::string> label_;

  /// Irreducible representation
  IRREP
    irrep_;
};

} // namespace mpsxx

inline mpsxx::PointGroup operator* (const mpsxx::PointGroup& x, const mpsxx::PointGroup& y)
{ mpsxx::PointGroup t(x); t *= y; return t; }

std::ostream& operator<< (std::ostream& ost, const mpsxx::PointGroup& x);

#endif // __MPSXX_POINT_GROUP_H
