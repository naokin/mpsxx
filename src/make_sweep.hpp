#ifndef __MPSXX_MAKE_SWEEP_HPP
#define __MPSXX_MAKE_SWEEP_HPP

#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <string>

#include <boost/serialization/vector.hpp>

#include <btas/QSPARSE/QSTArray.h>

#include "mpidefs.h"
#include "fileio.h"
#include "optimize.hpp"

namespace mpsxx {

/// Make sweep optimization
/// \param D 1 or 2 respectively calling 1-site or 2-site algorithm
/// \param N number of sites
/// \param M number of states to be kept
/// \param prefix file prefix
/// \param iroot target root
/// \param tolerance converge tolerance for Davidson iteration
/// \param noise noise to be added in canonicalization
/// Dependent files:
/// mpo.*    : MPOs
/// wave.*   : wavefunctions (<= iroot)
/// lmps.*   : left rotation matrix (<= iroot)
/// rmps.*   : right rotation matrix (<= iroot)
/// esav.*   : energies (< iroot)
/// left-H.* : left renormalized operators (<= iroot)
/// right-H.*: right renormalized operators (<= iroot)
/// left-S.* : left renormalized overlap (< iroot)
/// right-S.*: right renormalized overlap (< iroot)
template<class Q>
double make_sweep (
  const std::string& name,
  const size_t& D,
  const size_t& N,
  const size_t& M,
  const std::string& prefix,
  const size_t& iroot,
  const double& tolerance = 1.0e-8,
  const double& noise = 0.0,
  const double& shift = 0.0)
{
  using std::endl;
  using std::flush;
  using std::setw;
  using std::setprecision;
  using std::fixed;
  using std::scientific;

  Communicator world;

  // For the time, 1-site or 2-site algorithm is only available.
  assert(D == 1 || D == 2);

  std::vector<double> E;
  if(world.rank() == 0 && iroot > 0) {
    load(E,getfile("esav",prefix,0));
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,E,0);
#endif

  std::vector<btas::QSTArray<double,4,Q>> OpSys;
  std::vector<btas::QSTArray<double,4,Q>> OpEnv;

  std::vector<btas::QSTArray<double,3,Q>> WfSys(iroot+1);
  std::vector<btas::QSTArray<double,3,Q>> WfEnv(iroot+1);
  std::vector<btas::QSTArray<double,3,Q>> WfMps(iroot+1);

  std::vector<std::vector<btas::QSTArray<double,3,Q>>> lHopr(iroot+1);
  std::vector<std::vector<btas::QSTArray<double,3,Q>>> rHopr(iroot+1);

  std::vector<btas::QSTArray<double,2,Q>> lSopr(iroot+1);
  std::vector<btas::QSTArray<double,2,Q>> rSopr(iroot+1);

  double emin = 1.0e8;

  pout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
  pout << "\t\tmax states = " << setw(7) << M << endl;
  pout << "\t\ttole/noise = " << setprecision(1) << setw(7) << scientific << tolerance << "/" << setprecision(1) << setw(7) << scientific << noise << endl;

  pout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
  pout << "\t\t\tFORWARD SWEEP" << endl;
  pout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

  load(OpSys,getfile(name,prefix,0));
  for(size_t k = 0; k <= iroot; ++k) {
    if(world.rank() == 0) {
      load(WfSys[k],getfile("wave",prefix,0,k));
      load(lSopr[k],getfile("left-S",prefix,0,iroot,k));
    }
#ifndef _SERIAL
    boost::mpi::broadcast(world,WfSys[k],0);
    boost::mpi::broadcast(world,lSopr[k],0);
#endif
    load(lHopr[k],getfile("left-H",prefix,0,iroot,k));
  }

  for(size_t i = 0; i < N-1; ++i) {

    pout << "\t====================================================================================================" << endl;
    pout << "\t\tSITE [ " << setw(3) << i << " ] " << endl;
    pout << "\t----------------------------------------------------------------------------------------------------" << endl;

    pout << "\t\tloading operators and wavefunction of next site (env)" << endl;
    load(OpEnv,getfile(name,prefix,i+1));
    for(size_t k = 0; k <= iroot; ++k) {
      if(world.rank() == 0) {
        load(WfEnv[k],getfile("rmps",prefix,i+1,k));
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,WfEnv[k],0);
#endif
    }
    for(size_t k = 0; k < iroot; ++k) {
      if(world.rank() == 0) {
        load(WfMps[k],getfile("lmps",prefix,i,k));
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,WfMps[k],0);
#endif
    }

    double eswp;

    // Optimize for each site
    if(D == 1) {
      pout << "\t\toptimizing wavefunction: 1-site algorithm " << endl;
      for(size_t k = 0; k <= iroot; ++k) {
        load(rHopr[k],getfile("right-H",prefix,i,iroot,k));
        if(world.rank() == 0) {
          load(rSopr[k],getfile("right-S",prefix,i,iroot,k));
        }
#ifndef _SERIAL
        boost::mpi::broadcast(world,rSopr[k],0);
#endif
      }
//    eswp = optimize_onesite_merged(1,E,OpSys,      lHopr,rHopr,lSopr,rSopr,WfMps,WfSys,WfEnv,tolerance,M,noise,shift);
      eswp = optimize_onesite(1,E,OpSys,      lHopr,rHopr,lSopr,rSopr,WfMps,WfSys,WfEnv,tolerance,M,noise,shift);
    }
    else {
      pout << "\t\toptimizing wavefunction: 2-site algorithm " << endl;
      for(size_t k = 0; k <= iroot; ++k) {
        load(rHopr[k],getfile("right-H",prefix,i+1,iroot,k));
        if(world.rank() == 0) {
          load(rSopr[k],getfile("right-S",prefix,i+1,iroot,k));
        }
#ifndef _SERIAL
        boost::mpi::broadcast(world,rSopr[k],0);
#endif
      }
      eswp = optimize_twosite_merged(1,E,OpSys,OpEnv,lHopr,rHopr,lSopr,rSopr,WfMps,WfSys,WfEnv,tolerance,M,noise,shift);
//    eswp = optimize_twosite(1,E,OpSys,OpEnv,lHopr,rHopr,lSopr,rSopr,WfMps,WfSys,WfEnv,tolerance,M,noise,shift);
    }
    if(eswp < emin) emin = eswp;
    // print result
    pout << "\t\t--------------------------------------------------------------------------------" << endl;
    pout.precision(16);
    pout << "\t\t\tEnergy = " << setw(24) << fixed << eswp << endl;
    pout << "\t\t--------------------------------------------------------------------------------" << endl;

    pout << "\t\tsaving operators and wavefunction of this site (sys)" << endl;
    if(world.rank() == 0) {
      save(WfSys[iroot],getfile("lmps",prefix,i,iroot));
    }
    for(size_t k = 0; k <= iroot; ++k) {
      save(lHopr[k],getfile("left-H",prefix,i+1,iroot,k));
      if(world.rank() == 0) {
        save(lSopr[k],getfile("left-S",prefix,i+1,iroot,k));
      }
    }
    // load WfSys for the next loop
    for(size_t k = 0; k < iroot; ++k) {
      if(world.rank() == 0) {
        load(WfSys[k],getfile("wave",prefix,i+1,k));
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,WfSys[k],0);
#endif
    }
    OpSys = OpEnv;
    WfSys[iroot] = WfEnv[iroot];
    if(world.rank() == 0) {
      save(WfSys[iroot],getfile("wave",prefix,i+1,iroot));
    }
  }

  pout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
  pout << "\t\t\tBACKWARD SWEEP" << endl;
  pout << "\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

  for(size_t k = 0; k <= iroot; ++k) {
    load(rHopr[k],getfile("right-H",prefix,N-1,iroot,k));
    if(world.rank() == 0) {
      load(rSopr[k],getfile("right-S",prefix,N-1,iroot,k));
    }
#ifndef _SERIAL
    boost::mpi::broadcast(world,rSopr[k],0);
#endif
  }

  for(size_t i = N-1; i > 0; --i) {

    pout << "\t====================================================================================================" << endl;
    pout << "\t\tSITE [ " << setw(3) << i << " ] " << endl;
    pout << "\t----------------------------------------------------------------------------------------------------" << endl;

    pout << "\t\tloading operators and wavefunction of next site (env)" << endl;
    load(OpEnv,getfile(name,prefix,i-1));
    for(size_t k = 0; k <= iroot; ++k) {
      if(world.rank() == 0) {
        load(WfEnv[k],getfile("lmps",prefix,i-1,k));
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,WfEnv[k],0);
#endif
    }
    for(size_t k = 0; k < iroot; ++k) {
      if(world.rank() == 0) {
        load(WfMps[k],getfile("rmps",prefix,i,k));
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,WfMps[k],0);
#endif
    }

    double eswp;

    // Optimize for each site
    if(D == 1) {
      pout << "\t\toptimizing wavefunction: 1-site algorithm " << endl;
      for(size_t k = 0; k <= iroot; ++k) {
        load(lHopr[k],getfile("left-H",prefix,i,iroot,k));
        if(world.rank() == 0) {
          load(lSopr[k],getfile("left-S",prefix,i,iroot,k));
        }
#ifndef _SERIAL
        boost::mpi::broadcast(world,lSopr[k],0);
#endif
      }
//    eswp = optimize_onesite_merged(0,E,      OpSys,lHopr,rHopr,lSopr,rSopr,WfMps,WfEnv,WfSys,tolerance,M,noise,shift);
      eswp = optimize_onesite(0,E,      OpSys,lHopr,rHopr,lSopr,rSopr,WfMps,WfEnv,WfSys,tolerance,M,noise,shift);
    }
    else {
      pout << "\t\toptimizing wavefunction: 2-site algorithm " << endl;
      for(size_t k = 0; k <= iroot; ++k) {
        load(lHopr[k],getfile("left-H",prefix,i-1,iroot,k));
        if(world.rank() == 0) {
          load(lSopr[k],getfile("left-S",prefix,i-1,iroot,k));
        }
#ifndef _SERIAL
        boost::mpi::broadcast(world,lSopr[k],0);
#endif
      }
      eswp = optimize_twosite_merged(0,E,OpEnv,OpSys,lHopr,rHopr,lSopr,rSopr,WfMps,WfEnv,WfSys,tolerance,M,noise,shift);
//    eswp = optimize_twosite(0,E,OpEnv,OpSys,lHopr,rHopr,lSopr,rSopr,WfMps,WfEnv,WfSys,tolerance,M,noise,shift);
    }
    if(eswp < emin) emin = eswp;
    // print result
    pout << "\t\t--------------------------------------------------------------------------------" << endl;
    pout.precision(16);
    pout << "\t\t\tEnergy = " << setw(24) << fixed << eswp << endl;
    pout << "\t\t--------------------------------------------------------------------------------" << endl;

    pout << "\t\tsaving operators and wavefunction for this site (sys)" << endl;
    if(world.rank() == 0) {
      save(WfSys[iroot],getfile("rmps",prefix,i,iroot));
    }
    for(size_t k = 0; k <= iroot; ++k) {
      save(rHopr[k],getfile("right-H",prefix,i-1,iroot,k));
      if(world.rank() == 0) {
        save(rSopr[k],getfile("right-S",prefix,i-1,iroot,k));
      }
    }
    // load WfSys for the next loop
    for(size_t k = 0; k < iroot; ++k) {
      if(world.rank() == 0) {
        load(WfSys[k],getfile("wave",prefix,i-1,k));
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,WfSys[k],0);
#endif
    }
    OpSys = OpEnv;
    WfSys[iroot] = WfEnv[iroot];
    if(world.rank() == 0) {
      save(WfSys[iroot],getfile("wave",prefix,i-1,iroot));
    }
  }

  pout << "\t====================================================================================================" << endl;

  return emin;
}

} // namespace mpsxx

#endif // __MPSXX_MAKE_SWEEP_HPP
