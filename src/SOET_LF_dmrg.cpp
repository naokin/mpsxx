#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

#include <btas/DENSE/TArray.h>

#include <time_stamp.h>

#include "mpidefs.h" // Communicator

#include "input.h"
#include "dmrg.hpp"
#include "mpogen_impl.hpp"
#include "mpogen_kmpos_impl.hpp"
#include "fermion.hpp"
#include "point_group.h"
#include "parsing_integral.h"
#include "BFGS.hpp"
#include "make_restart_mpss.hpp"

std::vector<std::vector<double>> compute_occup_1 (const mpsxx::DMRGInput& input, const std::string& pfix)
{
  size_t N = input.N_sites;
  size_t M = input.N_roots;
  //
  btas::Qshapes<fermion<mpsxx::PointGroup>> qn = mpsxx::PhysStates<fermion<mpsxx::PointGroup>>::quanta();
  btas::Dshapes                             dn(qn.size(),1);
  //
  btas::QSTArray<double,2ul,fermion<mpsxx::PointGroup>>
  Oi(fermion<mpsxx::PointGroup>::zero(),btas::make_array(qn,-qn),btas::make_array(dn,dn),false);
  //
  btas::TArray<double,2ul> I(1,1);
  I = 1.0;
  Oi.insert(btas::shape(1ul,1ul),I);
  Oi.insert(btas::shape(2ul,2ul),I);
  I = 2.0;
  Oi.insert(btas::shape(3ul,3ul),I);
  //
  std::vector<std::vector<double>> occn(M);
  for(size_t k = 0; k < M; ++k) {
    std::vector<double> occ1(N,0.0);
    for(size_t i = 0; i < N; ++i) {
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Ai;
      load(Ai,mpsxx::getfile("wave",pfix,i,k));
      //
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Bi;
      btas::Contract(1.0,Ai,btas::shape(0ul,3ul,1ul),Oi,btas::shape(2ul,3ul),1.0,Bi,btas::shape(0ul,2ul,1ul));
      //
      occ1[i] = btas::Dotc(Ai,Bi);
    }
    occn[k] = occ1;
  }
  //
  return occn;
}

std::vector<std::vector<double>> compute_occup_2 (const mpsxx::DMRGInput& input, const std::string& pfix)
{
  size_t N = input.N_sites;
  size_t M = input.N_roots;
  //
  btas::Qshapes<fermion<mpsxx::PointGroup>> qn = mpsxx::PhysStates<fermion<mpsxx::PointGroup>>::quanta();
  btas::Dshapes                             dn(qn.size(),1);
  //
  btas::QSTArray<double,2ul,fermion<mpsxx::PointGroup>>
  Oi(fermion<mpsxx::PointGroup>::zero(),btas::make_array(qn,-qn),btas::make_array(dn,dn),false);
  //
  btas::TArray<double,2ul> I(1,1);
  I = 1.0;
  Oi.insert(btas::shape(3ul,3ul),I);
  //
  std::vector<std::vector<double>> occn(M);
  for(size_t k = 0; k < M; ++k) {
    std::vector<double> occ2(N,0.0);
    for(size_t i = 0; i < N; ++i) {
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Ai;
      load(Ai,mpsxx::getfile("wave",pfix,i,k));
      //
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Bi;
      btas::Contract(1.0,Ai,btas::shape(0ul,3ul,1ul),Oi,btas::shape(2ul,3ul),1.0,Bi,btas::shape(0ul,2ul,1ul));
      //
      occ2[i] = btas::Dotc(Ai,Bi);
    }
    occn[k] = occ2;
  }
  //
  return occn;
}

void rundmrg (
        mpsxx::DMRGInput& input,
  const btas::TArray<double,2ul>& oneint,
  const btas::TArray<double,4ul>& twoint,
  const std::vector<mpsxx::PointGroup>& orbsym,
  const bool& hubbard,
  const std::string& pfix)
{
  using std::endl;
  using std::setw;
  using std::fixed;
  using std::scientific;
  //
  size_t Norbs = input.N_sites;
  size_t Nelec = input.N_elecs;
  double Ecore = 0.0;
  //
  Communicator world;
  size_t divide = world.size();
  //
  std::vector<int> rmdocc(Norbs,0);

  pout << input << endl;

  time_stamp ts;

  if(divide > 0)
    mpsxx::mpogen_impl(divide,Norbs,Ecore,orbsym,rmdocc,oneint,twoint,"mpo",pfix,true,hubbard,true);
  else
    mpsxx::mpogen_kmpos_impl<1>(Norbs,Ecore,orbsym,rmdocc,oneint,twoint,"mpo",pfix,true,hubbard,true);

  world.barrier();

  pout << endl;
  pout.precision(2);
  pout << "\t\t\tWall time for MPOs construction: " << setw(8) << fixed << ts.lap() << endl;
  //
  if(input.restart)
    make_restart_mpss("mpo",pfix,Norbs,0,fermion<mpsxx::PointGroup>(input.N_elecs,input.N_spins,mpsxx::PointGroup(input.irrep)),0);
  //
  input.energy = mpsxx::dmrg<fermion<mpsxx::PointGroup>>(
    input.name,
    input.restart,

    fermion<mpsxx::PointGroup>(
      input.N_elecs,
      input.N_spins,
      mpsxx::PointGroup(input.irrep)),

    input.algorithm,
    input.N_sites,
    input.N_max_states,
    pfix,
    input.N_roots,
    input.tolerance,
    input.noise,
    input.shift,
    input.N_max_sweep_iter
  );

  world.barrier();

  pout << endl;
  pout.precision(2);
  pout << "\t\t\tWall time for DMRG optimization: " << setw(8) << fixed << ts.lap() << endl;
  pout << "\t\t\tWall time for Total calculation: " << setw(8) << fixed << ts.elapsed() << endl;
}

double LF_function (
        mpsxx::DMRGInput& input,
  const std::vector<size_t>& imp,
  const btas::TArray<double,2ul>& oneint,
  const btas::TArray<double,4ul>& twoint,
  const std::vector<mpsxx::PointGroup>& orbsym,
  const std::vector<double>& vpot,
  const std::vector<double>& occp,
  const bool& hubbard,
  const std::string& pfix)
{
  using std::endl;
  using std::setw;
  using std::fixed;
  using std::scientific;
  //
  size_t N = input.N_sites;
  //
  btas::TArray<double,2ul> onepot(oneint);
  for(size_t i = 0; i < N; ++i)
    if(!imp[i]) onepot(i,i) = vpot[i];
  //
  rundmrg(input,onepot,twoint,orbsym,hubbard,pfix);
  //
  double Ev = input.energy[0];
  //
  double vn = 0.0;
  for(size_t i = 0; i < N; ++i) vn += vpot[i]*occp[i];
  pout.precision(8);
  pout << "\tEv - (v|n) = " << setw(16) << fixed << Ev-vn << endl;
  //
  input.algorithm = 1;
  //
  return Ev-vn;
}

std::vector<double> LF_gradient (
        mpsxx::DMRGInput& input,
  const std::vector<size_t>& imp,
  const btas::TArray<double,2ul>& oneint,
  const btas::TArray<double,4ul>& twoint,
  const std::vector<mpsxx::PointGroup>& orbsym,
  const std::vector<double>& vpot,
  const std::vector<double>& occp,
  const bool& hubbard,
  const std::string& pfix)
{
  using std::endl;
  using std::setw;
  using std::fixed;
  using std::scientific;
  //
  size_t N = input.N_sites;
  //
  btas::TArray<double,2ul> onepot(oneint);
  for(size_t i = 0; i < N; ++i)
    if(!imp[i]) onepot(i,i) = vpot[i];
  //
  rundmrg(input,onepot,twoint,orbsym,hubbard,pfix);
  //
  double Ev = input.energy[0];
  //
  std::vector<std::vector<double>> occ1 = compute_occup_1(input,pfix);
  std::vector<std::vector<double>> occ2 = compute_occup_2(input,pfix);
  //
  std::vector<double> grad(N,0.0);
//for(size_t i = 0; i < N; ++i)
//  if(!imp[i]) grad[i] = occp[i]-occ1[0][i];
  double g0 = occp[0]-occ1[0][0];
  for(size_t i = 1; i < N; ++i)
    grad[i] = occp[i]-occ1[0][i]-g0;
  //
  double vn = 0.0;
  double gn = 0.0;
  pout.precision(8);
  pout << "\t======================================================================================" << endl;
  pout << "\t Site | Potential     | Occ. (calc.)  | Double Occ.   | Occ. (input)  | Gradient      " << endl;
  pout << "\t--------------------------------------------------------------------------------------" << endl;
  for(size_t i = 0; i < N; ++i) {
    pout << "\t"
      << setw( 5) << i
      << setw(16) << scientific << vpot[i]
      << setw(16) << scientific << occ1[0][i]
      << setw(16) << scientific << occ2[0][i]
      << setw(16) << scientific << occp[i]
      << setw(16) << scientific << grad[i]
      << endl;
    vn += vpot[i]*occp[i];
    gn += grad[i]*grad[i];
  }
  pout << "\t--------------------------------------------------------------------------------------" << endl;
  pout << "\tEv - (v|n) = " << setw(16) << fixed << Ev-vn << endl;
  pout << "\t======================================================================================" << endl;
  //
  gn = sqrt(gn/N);
  if(gn < 1.0e-3) {
//  input.restart = true;
    input.algorithm = 1;
  }
  else {
//  input.restart = false;
    input.algorithm = 2;
  }
  //
  return grad;
}

int main (int argc, char* argv[])
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;
  using std::scientific;

#ifndef _SERIAL
  boost::mpi::environment env(argc,argv);
#endif
  Communicator world;

  std::string finp = "dmrg.conf";
  std::string fout;
  std::string fdmp = "FCIDUMP";
  std::string fcas;
  std::string focc;

  std::string pfix = ".";

  bool swapsweep = true;
  bool compress = true;

  size_t divide = world.size();

  for(int iarg = 0; iarg < argc; ++iarg) {
    if(strcmp(argv[iarg],"-i"  ) == 0) finp = argv[++iarg];
    if(strcmp(argv[iarg],"-o"  ) == 0) fout = argv[++iarg];
    if(strcmp(argv[iarg],"-s"  ) == 0) pfix = argv[++iarg];
    if(strcmp(argv[iarg],"-f"  ) == 0) fdmp = argv[++iarg];
    if(strcmp(argv[iarg],"-cas") == 0) fcas = argv[++iarg];
    if(strcmp(argv[iarg],"-occ") == 0) focc = argv[++iarg];
  }

  mpsxx::DMRGInput input(finp);

  //
  // assign cout as alias to ofst
  //
  std::streambuf *backup;
  backup = cout.rdbuf();
  std::ofstream ofst;
  if(fout.size() > 0) {
    std::ostringstream oss;
    oss << fout << "." << world.rank();
    ofst.open(oss.str().c_str());
    cout.rdbuf(ofst.rdbuf());
  }

  int Norbs;
  int Nelec;
  double Ecore;
  std::vector<int> irreps;
  btas::TArray<double,2> oneint;
  btas::TArray<double,4> twoint;

  cout << "\t****************************************************************************************************" << endl;
  cout << "\t\t\t\tMPSXX::PROTOTYPE::MPO GENERATOR FOR QC "                                                        << endl;
  cout << "\t****************************************************************************************************" << endl;
  cout << endl;
  cout << "\t====================================================================================================" << endl;
  cout << "\t\tLoading integral information from FCIDUMP "                                                         << endl;
  cout << "\t====================================================================================================" << endl;
  cout << endl;

  if(world.rank() == 0) {
    std::ifstream ifdmp(fdmp.c_str());
    parsing_fcidump(ifdmp,Norbs,Nelec,Ecore,irreps,oneint,twoint);
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,Norbs,0);
  boost::mpi::broadcast(world,Nelec,0);
  boost::mpi::broadcast(world,Ecore,0);
  boost::mpi::broadcast(world,irreps,0);
  boost::mpi::broadcast(world,oneint,0);
  boost::mpi::broadcast(world,twoint,0);
#endif

  input.tolerance /= 1000; // DMRG tolerance is set to be tight
  input.N_sites = Norbs;

  std::vector<size_t> imp(Norbs,0);
  if(world.rank() == 0) {
    if(!fcas.empty()) {
      std::ifstream ifcas(fcas.c_str());
      std::vector<std::string> tok = gettoken(ifcas);
      for(size_t i = 0; i < tok.size(); ++i)
        imp[atoi(tok[i].c_str())-1] = 1;
    }
  }

  std::vector<double> occ;
  if(world.rank() == 0) {
    if(!focc.empty()) {
      occ.resize(Norbs,0.0);
      std::ifstream ifocc(focc.c_str());
      for(size_t i = 0; i < Norbs; ++i) ifocc >> occ[i];
    }
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,imp,0);
  boost::mpi::broadcast(world,occ,0);
#endif

  cout << "\t====================================================================================================" << endl;
  cout << "\t\tGenerating QC MPOs from 1- and 2-particle integrals"                                                << endl;
  cout << "\t====================================================================================================" << endl;
  cout << endl;

  // Set symmetry
       if(input.symmetry == "D2h") { mpsxx::PointGroup::set(mpsxx::_D2h); }
  else if(input.symmetry == "C2v") { mpsxx::PointGroup::set(mpsxx::_C2v); }
  else if(input.symmetry == "C2h") { mpsxx::PointGroup::set(mpsxx::_C2h); }
  else if(input.symmetry == "D2" ) { mpsxx::PointGroup::set(mpsxx::_D2 ); }
  else if(input.symmetry == "Cs" ) { mpsxx::PointGroup::set(mpsxx::_Cs ); }
  else if(input.symmetry == "C2" ) { mpsxx::PointGroup::set(mpsxx::_C2 ); }
  else if(input.symmetry == "Ci" ) { mpsxx::PointGroup::set(mpsxx::_Ci ); }
  else if(input.symmetry == "C1" ) { mpsxx::PointGroup::set(mpsxx::_C1 ); }
  else {
    pout << "Requested symmetry [" << input.symmetry << "] is not supported." << endl;
    abort();
  }

  std::vector<mpsxx::PointGroup> orbsym(irreps.size());
  for(size_t i = 0; i < irreps.size(); ++i) orbsym[i] = mpsxx::PointGroup(irreps[i]);

  rundmrg(input,oneint,twoint,orbsym,true,pfix);

  std::vector<std::vector<double>> occ1 = compute_occup_1(input,pfix);
  std::vector<std::vector<double>> occ2 = compute_occup_2(input,pfix);

  cout.precision(16);
  cout << "\t====================================================" << endl;
  cout << "\tE0 = " << setw(24) << fixed << input.energy[0] << endl;
  cout << setw(4) << input.N_sites << endl;
  cout << "\t----------------------------------------------------" << endl;
  cout << "\tSite|       Occupation      |   Double Occupation   " << endl;
  cout << "\t----------------------------------------------------" << endl;
  for(size_t i = 0; i < input.N_sites; ++i) {
    cout << "\t" << setw(4) << i+1 << setw(24) << scientific << occ1[0][i] << setw(24) << scientific << occ2[0][i] << endl;
  }
  cout << "\t====================================================" << endl;

  // potential to be optimized
  std::vector<double> vpot(Norbs,0.0);

  for(size_t i = 0; i < Norbs; ++i) {
    if(!imp[i]) vpot[i] = twoint(i,i,i,i)*Nelec/Norbs/2;
  }

  size_t Norbs2 = Norbs*Norbs;
  size_t Norbs3 = Norbs*Norbs2;
  double* p = twoint.data();
  for(size_t i = 0; i < Norbs; ++i) {
    if(!imp[i]) {
      memset(p,0,sizeof(double)*Norbs3);
      p += Norbs3;
    }
    else {
      for(size_t j = 0; j < Norbs; ++j) {
        if(!imp[j]) {
          memset(p,0,sizeof(double)*Norbs2);
          p += Norbs2;
        }
        else {
          for(size_t k = 0; k < Norbs; ++k) {
            if(!imp[k]) {
              memset(p,0,sizeof(double)*Norbs);
              p += Norbs;
            }
            else {
              for(size_t l = 0; l < Norbs; ++l, ++p) {
                if(!imp[l]) *p = 0.0;
              }
            }
          }
        }
      }
    }
  }

  if(occ.empty()) occ = occ1[0];

  input.restart = true;
  boost::function<            double (const std::vector<double>&)> Fn;
  boost::function<std::vector<double>(const std::vector<double>&)> Gd;
  Fn = boost::bind(LF_function,input,imp,oneint,twoint,orbsym,_1,occ,true,pfix);
  Gd = boost::bind(LF_gradient,input,imp,oneint,twoint,orbsym,_1,occ,true,pfix);

  BFGS solver(Fn,Gd,vpot,input.tolerance*1000);

  cout.rdbuf(backup);
  ofst.close();

  return 0;
}
