#ifndef __MPSXX_MPSBLAS_DOT_HPP
#define __MPSXX_MPSBLAS_DOT_HPP

#include <string>
#include <vector>

#include <btas/QSPARSE/QSTArray.h>

#include "fileio.h"

namespace mpsxx {

/// MPS x MPS (or MPO x MPO) dot function
template<class Q, size_t N = 3ul>
double dot (
  const std::vector<btas::QSTArray<double,N,Q>>& x,
  const std::vector<btas::QSTArray<double,N,Q>>& y)
{
  assert(x.size() == y.size());

  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);

  // 1 x 1 matrix as an overlap element on the left, filled by 1.0
  btas::QSTArray<double,2,Q> l(Q::zero(),btas::make_array(q0,q0),btas::make_array(d0,d0),1.0);
  for(size_t i = 0; i < x.size(); ++i) {
    btas::QSTArray<double,N,Q> temp;
    btas::Gemm(CblasTrans,CblasNoTrans,1.0,l,x[i].conjugate(),1.0,temp);
    l.clear();
    btas::Gemm(CblasTrans,CblasNoTrans,1.0,temp,y[i],1.0,l);
  }

  return l.begin()->second(0,0);
}

/// MPS x MPO x MPS dot function
template<class Q>
double dot (
  const std::vector<btas::QSTArray<double,3,Q>>& x,
  const std::vector<btas::QSTArray<double,4,Q>>& H,
  const std::vector<btas::QSTArray<double,3,Q>>& y)
{
  assert(x.size() == H.size());
  assert(y.size() == H.size());

  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);

  // 1 x 1 x 1 tensor as an operator element on the left, filled by 1.0
  btas::QSTArray<double,3,Q> l(Q::zero(),btas::make_array(q0,q0,q0),btas::make_array(d0,d0,d0),1.0);
  for(size_t i = 0; i < H.size(); ++i) {
    btas::QSTArray<double,4,Q> tmp1;
    btas::Contract(1.0,l,btas::shape(0ul),x[i].conjugate(),btas::shape(0ul),1.0,tmp1);
    btas::QSTArray<double,4,Q> tmp2;
    btas::Contract(1.0,tmp1,btas::shape(0ul,2ul),H[i],btas::shape(0ul,1ul),1.0,tmp2);
    l.clear();
    btas::Contract(1.0,tmp2,btas::shape(0ul,2ul),y[i],btas::shape(0ul,1ul),1.0,l);
  }

  return l.begin()->second(0,0,0);
}

/// MPS x MPS (or MPO x MPO) dot function (disk base)
template<class Q, size_t N = 3ul>
double dot (
  const std::string& xname,
  const std::string& yname,
  const std::string& prefix = ".")
{
  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);

  // 1 x 1 matrix as an overlap element on the left, filled by 1.0
  btas::QSTArray<double,2,Q> l(Q::zero(),btas::make_array(q0,q0),btas::make_array(d0,d0),1.0);
  for(size_t i = 0; i < x.size(); ++i) {
    btas::QSTArray<double,N,Q> xmps; load(xmps,getfile(xname,prefix,i));
    btas::QSTArray<double,N,Q> ymps; load(ymps,getfile(yname,prefix,i));
    btas::QSTArray<double,N,Q> temp;
    btas::Gemm(CblasTrans,CblasNoTrans,1.0,l,xmps.conjugate(),1.0,temp);
    l.clear();
    btas::Gemm(CblasTrans,CblasNoTrans,1.0,temp,ymps,1.0,l);
  }

  return l.begin()->second(0,0);
}

/// MPS x MPO x MPS dot function (disk base)
template<class Q>
double dot (
  const std::string& xname,
  const std::string& Hname,
  const std::string& yname,
  const std::string& prefix = ".")
{
  assert(x.size() == H.size());
  assert(y.size() == H.size());

  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);

  // 1 x 1 x 1 tensor as an operator element on the left, filled by 1.0
  btas::QSTArray<double,3,Q> l(Q::zero(),btas::make_array(q0,q0,q0),btas::make_array(d0,d0,d0),1.0);
  for(size_t i = 0; i < H.size(); ++i) {
    btas::QSTArray<double,3,Q> xmps; load(xmps,getfile(xname,prefix,i));
    btas::QSTArray<double,4,Q> Hmpo; load(Hmpo,getfile(Hname,prefix,i));
    btas::QSTArray<double,3,Q> ymps; load(ymps,getfile(yname,prefix,i));
    btas::QSTArray<double,4,Q> tmp1;
    btas::Contract(1.0,l,btas::shape(0ul),xmps.conjugate(),btas::shape(0ul),1.0,tmp1);
    btas::QSTArray<double,4,Q> tmp2;
    btas::Contract(1.0,tmp1,btas::shape(0ul,2ul),Hmpo,btas::shape(0ul,1ul),1.0,tmp2);
    l.clear();
    btas::Contract(1.0,tmp2,btas::shape(0ul,2ul),ymps,btas::shape(0ul,1ul),1.0,l);
  }

  return l.begin()->second(0,0,0);
}

} // namespace mpsxx

#endif // __MPSXX_MPSBLAS_DOT_HPP
