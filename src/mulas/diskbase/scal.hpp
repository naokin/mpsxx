#ifndef __MPSXX_MPSBLAS_SCAL_HPP
#define __MPSXX_MPSBLAS_SCAL_HPP

#include <string>
#include <vector>

#include <btas/QSPARSE/QSTArray.h>

#include "fileio.h"

namespace mpsxx {

/// alpha x MPS (alpha x MPO) scal function
/// the left-most site is actually scaled by alpha
template<class Q, size_t N>
void scal (
  const double& alpha,
  const std::vector<btas::QSTArray<double,N,Q>>& x)
{
  btas::Scal(alpha,x[0]);
}

/// alpha x MPS (alpha x MPO) scal function (disk base)
/// the left-most site is actually scaled by alpha
template<class Q, size_t N>
void scal (
  const double& alpha,
  const std::string& xname,
  const std::string& prefix = ".")
{
  btas::QSTArray<double,N,Q> xmps; load(xmps,getfile(xname,prefix,0));
  btas::Scal(alpha,xmps);
}

} // namespace mpsxx

#endif // __MPSXX_MPSBLAS_SCAL_HPP
