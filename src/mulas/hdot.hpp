#ifndef __MPSXX_MULAS_HDOT_HPP
#define __MPSXX_MULAS_HDOT_HPP

#include <complex>

#include <btas/SPARSE/STConj.h>

#include "mulas/mpdefs.h"

namespace mpsxx {

/// MPS x MPO x MPS dot function
template<class Q>
double hdot (
  const MPSXX_MPS_array<double,Q>& x,
  const MPSXX_MPO_array<double,Q>& H,
  const MPSXX_MPS_array<double,Q>& y)
{
  size_t N = H.size();
  assert(x.size() == N);
  assert(y.size() == N);
  // zero quanta
  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);
  // 1 x 1 x 1 tensor as an operator element on the left, filled by 1.0
  MPSXX_tensor_type<double,3,Q> l(Q::zero(),btas::make_array(q0,q0,q0),btas::make_array(d0,d0,d0),1.0);
  // sweep to compute <x|H|y>
  for(size_t i = 0; i < N; ++i) {
    MPSXX_tensor_type<double,4,Q> tmp1;
    btas::Contract(1.0,l,btas::shape(0ul),x[i].conjugate(),btas::shape(0ul),1.0,tmp1);
    MPSXX_tensor_type<double,4,Q> tmp2;
    btas::Contract(1.0,tmp1,btas::shape(0ul,2ul),H[i],btas::shape(0ul,1ul),1.0,tmp2);
    l.clear();
    btas::Contract(1.0,tmp2,btas::shape(0ul,2ul),y[i],btas::shape(0ul,1ul),1.0,l);
  }
  return (*l.begin()->second)(0,0,0);
}

/// MPS x MPO x MPS dot function
template<class Q>
std::complex<double> hdot (
  const MPSXX_MPS_array<std::complex<double>,Q>& x,
  const MPSXX_MPO_array<std::complex<double>,Q>& H,
  const MPSXX_MPS_array<std::complex<double>,Q>& y)
{
  std::complex<double> ONE(1.0,0.0);
  size_t N = H.size();
  assert(x.size() == N);
  assert(y.size() == N);
  // zero quanta
  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);
  // 1 x 1 x 1 tensor as an operator element on the left, filled by 1.0
  MPSXX_tensor_type<std::complex<double>,3,Q> l(Q::zero(),btas::make_array(q0,q0,q0),btas::make_array(d0,d0,d0),ONE);
  // sweep to compute <x|H|y>
  for(size_t i = 0; i < N; ++i) {
    MPSXX_tensor_type<std::complex<double>,4,Q> tmp1;
    MPSXX_tensor_type<std::complex<double>,3,Q> xconj(x[i].conjugate());
    btas::Conj(xconj); // conjugate elements themselves
    btas::Contract(ONE,l,btas::shape(0ul),xconj,btas::shape(0ul),ONE,tmp1);
    MPSXX_tensor_type<std::complex<double>,4,Q> tmp2;
    btas::Contract(ONE,tmp1,btas::shape(0ul,2ul),H[i],btas::shape(0ul,1ul),ONE,tmp2);
    l.clear();
    btas::Contract(ONE,tmp2,btas::shape(0ul,2ul),y[i],btas::shape(0ul,1ul),ONE,l);
  }
  return (*l.begin()->second)(0,0,0);
}

} // namespace mpsxx

#endif // __MPSXX_MULAS_HDOT_HPP
