#ifndef __MPSXX_MPBLAS_COMPRESS_HPP
#define __MPSXX_MPBLAS_COMPRESS_HPP

#include <complex>
#include <cmath>

#include "mpdefs.h"

namespace mpsxx {

// Default tolerance for SVD compression
#define _MPSXX_COMPRESS_DEFAULT_TOL_ 1.0e-12

namespace Gauge { enum GAUGE_TYPE { LEFT, RIGHT, SYMMETRIC }; }

/// Compress 2-site matrix product
/// Take a contraction of x and y, and divided back into x and y
/// \param g choice of gauge in the resulting matrix pair
/// \param D number of singular values to be kept
/// \return number of singular values kept as a result
template<class Q, size_t N>
void compress (
  const Gauge::GAUGE_TYPE& g,
        MPSXX_tensor_type<double,N,Q>& x,
        MPSXX_tensor_type<double,N,Q>& y,
  const size_t& D = 0ul,
  const double& T = _MPSXX_COMPRESS_DEFAULT_TOL_)
{
  // Contract 2 MPS/MPO
  MPSXX_tensor_type<double,N+N-2,Q> xy;
  btas::Gemm(CblasNoTrans,CblasNoTrans,1.0,x,y,1.0,xy);
  // SVD
  MPSXX_vector_type<double> s;
  if(g != Gauge::RIGHT)
    btas::Gesvd<double,N+N-2,N,Q,btas::QST::USE_L_QUANTA>(xy,s,x,y,D,T);
  else
    btas::Gesvd<double,N+N-2,N,Q,btas::QST::USE_R_QUANTA>(xy,s,x,y,D,T);
  // Symmetric gauge: left and right singular vectors are multiplied by s^(1/2)
  if(g == Gauge::SYMMETRIC) {
    for(auto it = s.begin(); it != s.end(); ++it)
      for(auto jt = it->second->begin(); jt != it->second->end(); ++jt)
        *jt = sqrt(*jt);
  }
  if(g != Gauge::RIGHT) btas::Dimm(s,y);
  if(g != Gauge::LEFT ) btas::Dimm(x,s);
}

/// One-sweep MPS compression
/// \param g choice of gauge on exit.
template<class Q>
void compress (
  const Gauge::GAUGE_TYPE& g,
        MPSXX_MPS_array<double,Q>& x,
  const size_t& D = 0ul,
  const double& T = _MPSXX_COMPRESS_DEFAULT_TOL_)
{
  size_t N = x.size();
  if(g != Gauge::RIGHT) {
    for(size_t i =   1; i < N; ++i) compress(g,x[i-1],x[i],D,T);
  }
  else {
    for(size_t i = N-1; i > 0; --i) compress(g,x[i-1],x[i],D,T);
  }
}

/// Compress 2-site matrix product
/// Take a contraction of x and y, and divided back into x and y
/// \param g choice of gauge in the resulting matrix pair
/// \param D number of singular values to be kept
/// \return number of singular values kept as a result
template<class Q, size_t N>
void compress (
  const Gauge::GAUGE_TYPE& g,
        MPSXX_tensor_type<std::complex<double>,N,Q>& x,
        MPSXX_tensor_type<std::complex<double>,N,Q>& y,
  const size_t& D = 0ul,
  const double& T = _MPSXX_COMPRESS_DEFAULT_TOL_)
{
  std::complex<double> ONE(1.0,0.0);
  // Contract 2 MPS/MPO
  MPSXX_tensor_type<std::complex<double>,N+N-2,Q> xy;
  btas::Gemm(CblasNoTrans,CblasNoTrans,ONE,x,y,ONE,xy);
  // SVD
  MPSXX_vector_type<double> s;
  if(g != Gauge::RIGHT)
    btas::Gesvd<std::complex<double>,N+N-2,N,Q,btas::QST::USE_L_QUANTA>(xy,s,x,y,D,T);
  else
    btas::Gesvd<std::complex<double>,N+N-2,N,Q,btas::QST::USE_R_QUANTA>(xy,s,x,y,D,T);
  // Symmetric gauge: left and right singular vectors are multiplied by s^(1/2)
  if(g == Gauge::SYMMETRIC) {
    for(auto it = s.begin(); it != s.end(); ++it)
      for(auto jt = it->second->begin(); jt != it->second->end(); ++jt)
        *jt = sqrt(*jt);
  }
  if(g != Gauge::RIGHT) btas::Dimm(s,y);
  if(g != Gauge::LEFT ) btas::Dimm(x,s);
}

/// One-sweep MPS compression
/// \param g choice of gauge on exit.
template<class Q>
void compress (
  const Gauge::GAUGE_TYPE& g,
        MPSXX_MPS_array<std::complex<double>,Q>& x,
  const size_t& D = 0ul,
  const double& T = _MPSXX_COMPRESS_DEFAULT_TOL_)
{
  size_t N = x.size();
  if(g != Gauge::RIGHT) {
    for(size_t i =   1; i < N; ++i) compress(g,x[i-1],x[i],D,T);
  }
  else {
    for(size_t i = N-1; i > 0; --i) compress(g,x[i-1],x[i],D,T);
  }
}

} // namespace mpsxx

#endif // __MPSXX_MPBLAS_COMPRESS_HPP
