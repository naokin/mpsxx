#ifndef __MPSXX_MPDEFS_H
#define __MPSXX_MPDEFS_H

#include <vector>

#include <btas/QSPARSE/QSTArray.h>

namespace mpsxx {

template<typename T>
using MPSXX_vector_type = btas::STArray<T,1ul>;

template<typename T, size_t N, class Q>
using MPSXX_tensor_type = btas::QSTArray<T,N,Q>;

template<typename T, class Q>
using MPSXX_MPS_array = std::vector<MPSXX_tensor_type<T,3ul,Q>>;

template<typename T, class Q>
using MPSXX_MPO_array = std::vector<MPSXX_tensor_type<T,4ul,Q>>;

} // namespace mpsxx

#endif // __MPSXX_MPDEFS_H
