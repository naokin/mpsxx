#ifndef __MPSXX_LOAD_FROM_DISK_HPP
#define __MPSXX_LOAD_FROM_DISK_HPP

#include "mpdefs.h"
#include "fileio.h"

namespace mpsxx {

/// Load MPSs from disk as a single MPSs object
/// This function is temporary fix to convert MPSs format.
template<typename T, class Q>
void load_mpss (
  const size_t& N,
        MPSXX_MPS_array<T,Q>& mpss,
  const size_t& kroot,
  const std::string& prefix)
{
  mpss.resize(N);
  load(mpss[0],getfile("wave",prefix,0,kroot));
  for(size_t i = 1; i < N; ++i) {
    load(mpss[i],getfile("rmps",prefix,i,kroot));
  }
}

/// Load MPOs from disk as a single MPOs object
/// This function is temporary fix to convert MPOs format.
template<typename T, class Q>
void load_mpos (
  const size_t& N,
  const std::string& opname,
        MPSXX_MPO_array<T,Q>& mpos,
  const std::string& prefix)
{
  mpos.resize(N);
  for(size_t i = 0; i < N; ++i) {
    std::vector<btas::QSTArray<T,4ul,Q>> tmp;
    load(tmp,getfile(opname,prefix,i));
    assert(tmp.size() == 1); // k-MPO is not available...
    mpos[i] = tmp[0];
  }
}

template<typename T, size_t N, class Q>
void convert_real_to_complex (const MPSXX_tensor_type<T,N,Q>& real, MPSXX_tensor_type<std::complex<T>,N,Q>& comp)
{
  std::complex<T> ZERO(0.0,0.0);
  comp.resize(real.q(),real.qshape(),real.dshape(),ZERO);
  auto itRs = real.begin();
  auto itCs = comp.begin();
  for(; itRs != real.end(); ++itRs, ++itCs) {
    auto itRd = itRs->second->begin();
    auto itCd = itCs->second->begin();
    for(; itRd != itRs->second->end(); ++itRd, ++itCd)
      *itCd = std::complex<T>(*itRd,0.0);
  }
}

/// Load MPSs from disk as a single MPSs object
/// This function is temporary fix to convert MPSs format.
template<typename T, class Q>
void load_mpss_complex (
  const size_t& N,
        MPSXX_MPS_array<std::complex<T>,Q>& mpss,
  const size_t& kroot,
  const std::string& prefix)
{
  mpss.resize(N);
  MPSXX_tensor_type<T,3,Q> temp;
  load(temp,getfile("wave",prefix,0,kroot));
  convert_real_to_complex(temp,mpss[0]);
  for(size_t i = 1; i < N; ++i) {
    load(temp,getfile("rmps",prefix,i,kroot));
    convert_real_to_complex(temp,mpss[i]);
  }
}

/// Load MPOs from disk as a single MPOs object
/// This function is temporary fix to convert MPOs format.
template<typename T, class Q>
void load_mpos_complex (
  const size_t& N,
  const std::string& opname,
        MPSXX_MPO_array<std::complex<T>,Q>& mpos,
  const std::string& prefix)
{
  mpos.resize(N);
  for(size_t i = 0; i < N; ++i) {
    std::vector<MPSXX_tensor_type<T,4,Q>> temp;
    load(temp,getfile(opname,prefix,i));
    assert(temp.size() == 1); // k-MPO is not available...
    convert_real_to_complex(temp[0],mpos[i]);
  }
}

} // namespace mpsxx

#endif // __MPSXX_LOAD_FROM_DISK_HPP
