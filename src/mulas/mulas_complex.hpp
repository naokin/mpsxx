//
// MPO-MPS multi-linear algebra subprograms (MuLAS, alpha-version)
// Written by Naoki Nakatani, 2016
// Copyright (c) 2016, Naoki Nakatani
//

#ifndef __MPSXX_MULAS_COMPLEX_HPP
#define __MPSXX_MULAS_COMPLEX_HPP

#include <complex>

#include <btas/SPARSE/STConj.h>

#include "mpdefs.h"
#include "compress.hpp"

namespace mpsxx {

// **************************************************************************************************** 
// BLAS lv. 1
// **************************************************************************************************** 

/// Compute <MPS|MPS>
template<class Q>
std::complex<double> dotc (
  const MPSXX_MPS_array<std::complex<double>,Q>& x,
  const MPSXX_MPS_array<std::complex<double>,Q>& y)
{
  std::complex<double> ONE(1.0,0.0);
  assert(x.size() == y.size());
  // 1 x 1 matrix as an overlap element on the left, filled by 1.0
  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);
  MPSXX_tensor_type<std::complex<double>,2,Q> l(Q::zero(),btas::make_array(q0,q0),btas::make_array(d0,d0),ONE);
  // Contract from left to right
  for(size_t i = 0; i < x.size(); ++i) {
    typename MPSXX_MPS_array<std::complex<double>,Q>::value_type t;
    MPSXX_tensor_type<std::complex<double>,3,Q> xconj(x[i].conjugate());
    btas::Conj(xconj);
    btas::Gemm(CblasTrans,CblasNoTrans,ONE,l,xconj,ONE,t);
    l.clear();
    btas::Gemm(CblasTrans,CblasNoTrans,ONE,t,y[i],ONE,l);
  }
  return (*l.begin()->second)(0,0);
}

/// Compute Tr(MPO^t*MPO)
template<class Q>
std::complex<double> dotc (
  const MPSXX_MPO_array<std::complex<double>,Q>& x,
  const MPSXX_MPO_array<std::complex<double>,Q>& y)
{
  std::complex<double> ONE(1.0,0.0);
  assert(x.size() == y.size());
  // 1 x 1 matrix as an overlap element on the left, filled by 1.0
  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);
  MPSXX_tensor_type<std::complex<double>,2,Q> l(Q::zero(),btas::make_array(q0,q0),btas::make_array(d0,d0),ONE);
  // Contract from left to right
  for(size_t i = 0; i < x.size(); ++i) {
    typename MPSXX_MPO_array<std::complex<double>,Q>::value_type t;
    MPSXX_tensor_type<std::complex<double>,4,Q> xconj(x[i].conjugate());
    btas::Conj(xconj);
    btas::Gemm(CblasTrans,CblasNoTrans,ONE,l,xconj,ONE,t);
    l.clear();
    btas::Gemm(CblasTrans,CblasNoTrans,ONE,t,y[i],ONE,l);
  }
  return (*l.begin()->second)(0,0);
}

/// Compute alpha*|MPS>
/// The first element is scaled.
template<class Q>
void scal (
  const std::complex<double>& alpha,
        MPSXX_MPS_array<std::complex<double>,Q>& x) ///< in/out
{
  if(x.empty()) return;
  btas::Scal(alpha,x[0]); // multiply alpha at the first element
}

/// Compute alpha*MPO
/// The first element is scaled.
template<class Q>
void scal (
  const std::complex<double>& alpha,
        MPSXX_MPO_array<std::complex<double>,Q>& x) ///< in/out
{
  if(x.empty()) return;
  btas::Scal(alpha,x[0]); // multiply alpha at the first element
}

/// Compute MPS+MPS w/ compression
/// \param mkeep # bond to be kept on SVD compression, 0 for keeping all the singular values.
template<class Q>
void axpy (
  const std::complex<double>& alpha,
  const MPSXX_MPS_array<std::complex<double>,Q>& x, ///< in
        MPSXX_MPS_array<std::complex<double>,Q>& y, ///< out
  const size_t& mkeep = 0)
{
  if(y.empty()) {
    y = x;
    scal(alpha,y);
  }
  else {
    size_t N = x.size(); assert(y.size() == N);
    // the first element
    typename MPSXX_MPS_array<std::complex<double>,Q>::value_type t(x[0]);
    btas::Scal(alpha,t);
    typename MPSXX_MPS_array<std::complex<double>,Q>::value_type u;
    btas::QSTdsum(t,y[0],btas::shape(0ul,1ul),u);
    y[0] = u;
    // sweep from 1 to N-2
    for(size_t i = 1; i < N-1; ++i) {
      u.clear();
      btas::QSTdsum(x[i],y[i],btas::shape(1ul),u);
      y[i] = u;
      compress(Gauge::LEFT,y[i-1],y[i],0);
    }
    // the last element
    u.clear();
    btas::QSTdsum(x[N-1],y[N-1],btas::shape(1ul,2ul),u);
    y[N-1] = u;
    // backward sweep to truncate size and fix the gauge
    for(size_t i = N-1; i > 0; --i) {
      compress(Gauge::RIGHT,y[i-1],y[i],mkeep);
    }
  }
}

/// Compute MPO+MPO w/ compression
/// \param mkeep # bond to be kept on SVD compression, 0 for keeping all the singular values.
template<class Q>
void axpy (
  const std::complex<double>& alpha,
  const MPSXX_MPO_array<std::complex<double>,Q>& x, ///< in
        MPSXX_MPO_array<std::complex<double>,Q>& y, ///< out
  const size_t& mkeep = 0)
{
  if(y.empty()) {
    y = x;
    scal(alpha,y);
  }
  else {
    size_t N = x.size(); assert(y.size() == N);
    // the first element
    typename MPSXX_MPO_array<std::complex<double>,Q>::value_type t(x[0]);
    btas::Scal(alpha,t);
    typename MPSXX_MPO_array<std::complex<double>,Q>::value_type u;
    btas::QSTdsum(t,y[0],btas::shape(0ul,1ul,2ul),u);
    y[0] = u;
    // sweep from 1 to N-2
    for(size_t i = 1; i < N-1; ++i) {
      u.clear();
      btas::QSTdsum(x[i],y[i],btas::shape(1ul,2ul),u);
      y[i] = u;
      compress(Gauge::LEFT,y[i-1],y[i],0);
    }
    // the last element
    u.clear();
    btas::QSTdsum(x[N-1],y[N-1],btas::shape(1ul,2ul,3ul),u);
    y[N-1] = u;
    // backward sweep to truncate size and fix the gauge
    for(size_t i = N-1; i > 0; --i) {
      compress(Gauge::RIGHT,y[i-1],y[i],mkeep);
    }
  }
}

// **************************************************************************************************** 
// BLAS lv. 2
// **************************************************************************************************** 

/// Compute |MPS> += MPO|MPS>
template<class Q>
void gemv (
  const CBLAS_TRANSPOSE& transa,
  const std::complex<double>& alpha,
  const MPSXX_MPO_array<std::complex<double>,Q>& a, ///< in
  const MPSXX_MPS_array<std::complex<double>,Q>& x, ///< in
  const std::complex<double>& beta,
        MPSXX_MPS_array<std::complex<double>,Q>& y, ///< out
  const size_t& mkeep = 0)
{
  std::complex<double> ONE(1.0,0.0);
  size_t N = a.size();
  assert(x.size() == N);
  assert(y.size() == N || y.empty());
  // ax (temporary obj.)
  MPSXX_MPS_array<std::complex<double>,Q> ax(N);
  // NOTE: MPSXX_tensor_type<T,N,Q> = btas::QSTArray<T,N,Q>
  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);
  MPSXX_tensor_type<std::complex<double>,3ul,Q> g(Q::zero(),btas::make_array(q0,q0,q0),btas::make_array(d0,d0,d0),alpha);
  // (1) t(i,j,p,l) = \sum_{k}   g(i,j,k)  *x[0](k,q,l)
  // (2) u(i,p,l,m) = \sum_{j,q} t(i,j,q,l)*a[0](j,p,q,m)
  MPSXX_tensor_type<std::complex<double>,4ul,Q> tmp1;
  btas::Contract(ONE,g,btas::shape(2ul),x[0],btas::shape(0ul),ONE,tmp1);
  // transposition of physical index
  size_t ia = (transa == CblasNoTrans) ? 2 : 1;
  MPSXX_tensor_type<std::complex<double>,4ul,Q> tmp2;
  btas::Contract(ONE,tmp1,btas::shape(1ul,2ul),a[0],btas::shape(0ul,ia),ONE,tmp2);
  MPSXX_tensor_type<std::complex<double>,4ul,Q> tmp3;
  btas::Permute(tmp2,btas::shape(0ul,2ul,3ul,1ul),tmp3);
  // SVD for the first element
  MPSXX_vector_type<double> s; // = btas::STArray<T,1ul>
  btas::Gesvd<std::complex<double>,4,3,Q,btas::QST::USE_L_QUANTA>(tmp3,s,ax[0],g,mkeep);
  btas::Dimm(s,g);
  // Sweep to compress
  for(size_t i = 1; i < N; ++i) {
    tmp1.clear();
    btas::Contract(ONE,g,btas::shape(2ul),x[i],btas::shape(0ul),ONE,tmp1);
    tmp2.clear();
    btas::Contract(ONE,tmp1,btas::shape(1ul,2ul),a[i],btas::shape(0ul,ia),ONE,tmp2);
    tmp3.clear();
    btas::Permute(tmp2,btas::shape(0ul,2ul,3ul,1ul),tmp3);
    if(i == N-1) // for the last site, keep total q# w/ MPS matrix
      btas::Gesvd<std::complex<double>,4,3,Q,btas::QST::USE_R_QUANTA>(tmp3,s,ax[i],g,mkeep);
    else
      btas::Gesvd<std::complex<double>,4,3,Q,btas::QST::USE_L_QUANTA>(tmp3,s,ax[i],g,mkeep);
    btas::Dimm(s,g);
  }
  // multiply the last element by the last scalar (=SV^t)
  btas::Scal((*g.begin()->second)(0,0,0),ax[N-1]);
  // Going back from right to left to truncate size and fix the gauge
  for(size_t i = N-1; i > 0; --i) {
    compress(Gauge::RIGHT,ax[i-1],ax[i],mkeep);
  }
  // y := ax + beta*y
  if(!y.empty()) axpy(beta,y,ax,mkeep);
  y.swap(ax);
}

// **************************************************************************************************** 
// BLAS lv. 3
// **************************************************************************************************** 

/// Compute MPO += MPO*MPO
template<class Q>
void gemm (
  const CBLAS_TRANSPOSE& transa,
  const CBLAS_TRANSPOSE& transb,
  const std::complex<double>& alpha,
  const MPSXX_MPO_array<std::complex<double>,Q>& a, ///< in
  const MPSXX_MPO_array<std::complex<double>,Q>& b, ///< in
  const std::complex<double>& beta,
        MPSXX_MPO_array<std::complex<double>,Q>& c, ///< out
  const size_t& mkeep = 0)
{
  std::complex<double> ONE(1.0,0.0);
  size_t N = a.size();
  assert(b.size() == N);
  assert(c.size() == N || c.empty());
  // ax (temporary obj.)
  MPSXX_MPO_array<std::complex<double>,Q> ab(N);
  // NOTE: MPSXX_tensor_type<T,N,Q> = btas::QSTArray<T,N,Q>
  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);
  MPSXX_tensor_type<std::complex<double>,3ul,Q> g(Q::zero(),btas::make_array(q0,q0,q0),btas::make_array(d0,d0,d0),alpha);
  // (1) t(i,k,p,q,l) = \sum_{j}   g(i,j,k)    *a[0](j,p,q,l)
  // (2) u(i,p,r,l,m) = \sum_{k,q} t(i,k,p,q,l)*b[0](k,q,r,m)
  MPSXX_tensor_type<std::complex<double>,5ul,Q> tmp1;
  btas::Contract(ONE,g,btas::shape(1ul),a[0],btas::shape(0ul),ONE,tmp1);
  // transposition of physical index
  size_t ia = (transa == CblasNoTrans) ? 3 : 2;
  size_t ib = (transa == CblasNoTrans) ? 1 : 2;
  MPSXX_tensor_type<std::complex<double>,5ul,Q> tmp2;
  btas::Contract(ONE,tmp1,btas::shape(1ul,ia),b[0],btas::shape(0ul,ib),ONE,tmp2);
  MPSXX_tensor_type<std::complex<double>,5ul,Q> tmp3;
  btas::Permute(tmp2,btas::shape(0ul,1ul,3ul,2ul,4ul),tmp3);
  // SVD for the first element
  MPSXX_vector_type<double> s; // = btas::STArray<T,1ul>
  btas::Gesvd<std::complex<double>,5,4,Q,btas::QST::USE_L_QUANTA>(tmp3,s,ab[0],g,mkeep);
  btas::Dimm(s,g);
  // Sweep to compress
  for(size_t i = 1; i < N; ++i) {
    tmp1.clear();
    btas::Contract(ONE,g,btas::shape(1ul),a[i],btas::shape(0ul),ONE,tmp1);
    tmp2.clear();
    btas::Contract(ONE,tmp1,btas::shape(1ul,ia),b[i],btas::shape(0ul,ib),ONE,tmp2);
    tmp3.clear();
    btas::Permute(tmp2,btas::shape(0ul,1ul,3ul,2ul,4ul),tmp3);
std::cout << "DEBUG :: gemm :: tmp3.shape() = " << tmp3.shape() << std::endl;
std::cout << "DEBUG :: gemm :: tmp3.dshape() = " << tmp3.dshape() << std::endl;
    if(i == N-1) // for the last site, keep total q# w/ MPS matrix
      btas::Gesvd<std::complex<double>,5,4,Q,btas::QST::USE_R_QUANTA>(tmp3,s,ab[i],g,mkeep);
    else
      btas::Gesvd<std::complex<double>,5,4,Q,btas::QST::USE_L_QUANTA>(tmp3,s,ab[i],g,mkeep);
    btas::Dimm(s,g);
  }
  // multiply the last element by the last scalar (=SV^t)
  btas::Scal((*g.begin()->second)(0,0,0),ab[N-1]);
  // Going back from right to left to truncate size and fix the gauge
  for(size_t i = N-1; i > 0; --i) {
    compress(Gauge::RIGHT,ab[i-1],ab[i],mkeep);
  }
  // c := ab + beta*c
  if(!c.empty()) axpy(beta,c,ab,mkeep);
  c.swap(ab);
}

} // namespace mpsxx

#endif // __MPSXX_MULAS_COMPLEX_HPP
