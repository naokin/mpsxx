//
// MPO-MPS multi-linear algebra subprograms (MuLAS, alpha-version)
// Written by Naoki Nakatani, 2016
// Copyright (c) 2016, Naoki Nakatani
//

#ifndef __MPSXX_MUEXP_HPP
#define __MPSXX_MUEXP_HPP

#include "mulas.hpp"
#include "mulas_complex.hpp"

namespace mpsxx {

/// Compute y = exp(x)
/// \param order order of approximation, in case 2, y = 1 + x + x/2
/// \param mkeep # of bond to be kept
template<class Q>
void muexp (
  const MPSXX_MPO_array<double,Q>& x, ///< in
        MPSXX_MPO_array<double,Q>& y, ///< out
  const size_t& order = 2,
  const size_t& mkeep = 0)
{
  if(!y.empty()) y.clear();
  // Construct identity operator as an MPO
  y.resize(x.size());
  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);
  for(size_t i = 0; i < x.size(); ++i) {
    y[i].resize(Q::zero(),
                btas::make_array(q0,x[i].qshape(1),x[i].qshape(2),q0),
                btas::make_array(d0,x[i].dshape(1),x[i].dshape(2),d0),1.0);
  }
  // Compute y = exp(x), approximately
  if(order > 0) {
    axpy(1.0,x,y,mkeep);
    if(order > 1) {
      double factor = 1.0;
      MPSXX_MPO_array<double,Q> t = x;
      for(size_t i = 1; i < order; ++i) {
        MPSXX_MPO_array<double,Q> u;
        gemm(CblasNoTrans,CblasNoTrans,1.0,t,x,1.0,u,mkeep);
        factor *= static_cast<double>(i+1);
        axpy(1.0/factor,u,y,mkeep);
        t.swap(u);
      }
    }
  }
}

/// Compute y = exp(x)
/// \param order order of approximation, in case 2, y = 1 + x + x/2
/// \param mkeep # of bond to be kept
template<class Q>
void muexp (
  const MPSXX_MPO_array<std::complex<double>,Q>& x, ///< in
        MPSXX_MPO_array<std::complex<double>,Q>& y, ///< out
  const size_t& order = 2,
  const size_t& mkeep = 0)
{
  std::complex<double> ONE(1.0,0.0);
  if(!y.empty()) y.clear();
  // Construct identity operator as an MPO
  y.resize(x.size());
  btas::Qshapes<Q> q0(1,Q::zero());
  btas::Dshapes    d0(1,1);
std::cout << "DEBUG :: muexp :: 01" << std::endl;
  for(size_t i = 0; i < x.size(); ++i) {
    y[i].resize(Q::zero(),
                btas::make_array(q0,x[i].qshape(1),x[i].qshape(2),q0),
                btas::make_array(d0,x[i].dshape(1),x[i].dshape(2),d0),ONE);
  }
std::cout << "DEBUG :: muexp :: 02" << std::endl;
  // Compute y = exp(x), approximately
  if(order > 0) {
std::cout << "DEBUG :: muexp :: 03" << std::endl;
    axpy(ONE,x,y,mkeep);
    if(order > 1) {
std::cout << "DEBUG :: muexp :: 04" << std::endl;
      double factor = 1.0;
      MPSXX_MPO_array<std::complex<double>,Q> t = x;
      MPSXX_MPO_array<std::complex<double>,Q> u;
      for(size_t i = 1; i < order; ++i) {
std::cout << "DEBUG :: muexp :: 05" << std::endl;
        u.clear();
        gemm(CblasNoTrans,CblasNoTrans,ONE,t,x,ONE,u,mkeep);
std::cout << "DEBUG :: muexp :: 06" << std::endl;
        factor *= static_cast<double>(i+1);
        axpy(ONE/factor,u,y,mkeep);
        t.swap(u);
      }
    }
  }
std::cout << "DEBUG :: muexp :: 07" << std::endl;
}

} // namespace mpsxx

#endif // __MPSXX_MUEXP_HPP
