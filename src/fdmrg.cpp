#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <cstdlib>

#include <boost/filesystem.hpp>
#include <btas/DENSE/TArray.h>

#include <time_stamp.h>

#include "mpidefs.h" // Communicator

#include "input.h"
#include "fileio.h"
#include "mpogen_impl.hpp"
#include "fermion.hpp"
#include "point_group.h"
#include "parsing_integral.h"
#include "mpsgen_impl.hpp"

#include "mulas/mulas.hpp"
#include "mulas/muexp.hpp"
#include "mulas/hdot.hpp"
#include "mulas/load_from_disk.hpp"

/// Do DMRG sweeps with compressed MPOs and then, evaluate the energy in 1 sweep.
/// \param finp input  file name.
/// \param fout output file name, output to stream if it is empty.
/// \param fdmp FCIDUMP file name.
/// \param fodr orbital ordering, no-reorder if it is empty.
/// \param pfix scratch file prefix.
int fdmrg (
  const std::string& finp,
  const std::string& fout,
  const std::string& fdmp,
  const std::string& fodr,
  const std::string& pfix,
        size_t Mw,
        size_t Kmin)
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

  Communicator world;

  // Check file specifications
  assert(!finp.empty());
  assert(!fdmp.empty());
  assert(!pfix.empty());

  bool enable_swap_sweep = false;
  bool do_compress = false;

  size_t nprocs = world.size();
  size_t iprint = 0;

  mpsxx::DMRGInput inp(finp);

  //
  // assign cout as alias to ofst
  //
  std::streambuf *backup;
  backup = cout.rdbuf();
  std::ofstream ofst;
  if(!fout.empty() > 0) {
    std::ostringstream oss;
    oss << fout << "." << world.rank();
    ofst.open(oss.str().c_str());
    cout.rdbuf(ofst.rdbuf());
  }

  int Norbs;
  int Nelec;
  double Ecore;
  std::vector<int> irreps;
  btas::TArray<double,2> oneint;
  btas::TArray<double,4> twoint;
  std::vector<int> reorder;

  if(world.rank() == 0) {
    std::ifstream idmp(fdmp.c_str());
    if(!fodr.empty()) {
      std::ifstream iodr(fodr.c_str());
      parsing_reorder(iodr,reorder);
      parsing_fcidump(idmp,Norbs,Nelec,Ecore,irreps,oneint,twoint,reorder);
    }
    else {
      parsing_fcidump(idmp,Norbs,Nelec,Ecore,irreps,oneint,twoint);
    }
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,Norbs,0);
  boost::mpi::broadcast(world,Nelec,0);
  boost::mpi::broadcast(world,Ecore,0);
  boost::mpi::broadcast(world,irreps,0);
  boost::mpi::broadcast(world,oneint,0);
  boost::mpi::broadcast(world,twoint,0);
  boost::mpi::broadcast(world,reorder,0);
#endif

  inp.N_sites = Norbs;

  cout << "\t====================================================================================================" << endl;
  cout << "\t\tGenerating QC MPOs from 1- and 2-particle integrals"                                                << endl;
  cout << "\t====================================================================================================" << endl;
  cout << endl;

  // Set symmetry
       if(inp.symmetry == "D2h") { mpsxx::PointGroup::set(mpsxx::_D2h); }
  else if(inp.symmetry == "C2v") { mpsxx::PointGroup::set(mpsxx::_C2v); }
  else if(inp.symmetry == "C2h") { mpsxx::PointGroup::set(mpsxx::_C2h); }
  else if(inp.symmetry == "D2" ) { mpsxx::PointGroup::set(mpsxx::_D2 ); }
  else if(inp.symmetry == "Cs" ) { mpsxx::PointGroup::set(mpsxx::_Cs ); }
  else if(inp.symmetry == "C2" ) { mpsxx::PointGroup::set(mpsxx::_C2 ); }
  else if(inp.symmetry == "Ci" ) { mpsxx::PointGroup::set(mpsxx::_Ci ); }
  else if(inp.symmetry == "C1" ) { mpsxx::PointGroup::set(mpsxx::_C1 ); }
  else {
    pout << "Requested symmetry [" << inp.symmetry << "] is not supported." << endl;
    abort();
  }

  std::vector<mpsxx::PointGroup> orbsym(irreps.size());
  for(size_t i = 0; i < irreps.size(); ++i) orbsym[i] = mpsxx::PointGroup(irreps[i]);

  time_stamp ts;

  if(!inp.restart) {
    //
    // generate exact MPOs
    //
    std::vector<int> rmdocc(Norbs,0); // this is not used.
    mpsxx::mpogen_impl(nprocs,inp.N_sites,Ecore,orbsym,rmdocc,oneint,twoint,"hmpo",pfix,true,false,true);

    inp.name = "hmpo";

    world.barrier();

    pout << endl;
    pout.precision(2);
    pout << "\t\t\tWall time for MPOs construction (1st step): " << setw(8) << fixed << ts.lap() << endl;
  }

  //
  // Load MPOs from Disk to RAM
  //
  mpsxx::MPSXX_MPO_array<double,fermion<mpsxx::PointGroup>> H;
  mpsxx::load_mpos(inp.N_sites,inp.name,H,pfix);

  pout << endl;
  pout.precision(2);
  pout << "\t\t\tWall time for setting H: " << setw(8) << fixed << ts.lap() << " sec." << endl;

  //
  // Make compressed H and H^2 for preconditioner
  //
  pout << "\t\tGenerating approximate H inverse for a preconditioner: Mw = " << Mw << endl;
  mpsxx::MPSXX_MPO_array<double,fermion<mpsxx::PointGroup>> D1(H);
  for(size_t i = 1; i < inp.N_sites; ++i) {
    mpsxx::compress(mpsxx::Gauge::LEFT,D1[i-1],D1[i],Mw);
  }
  for(size_t i = inp.N_sites-1; i > 0; --i) {
    mpsxx::compress(mpsxx::Gauge::RIGHT,D1[i-1],D1[i],Mw);
  }
  mpsxx::MPSXX_MPO_array<double,fermion<mpsxx::PointGroup>> D2;
  mpsxx::gemm(CblasNoTrans,CblasNoTrans,1.0,D1,D1,1.0,D2,Mw);

  //
  // Krylov subspace algorithm
  //
  if(Kmin < inp.N_roots) Kmin = inp.N_roots;
  size_t Kmax = 2*Kmin;
  std::vector<mpsxx::MPSXX_MPS_array<double,fermion<mpsxx::PointGroup>>> C(Kmax);

  double norm;

  if(world.rank() == 0) {
    if(!inp.restart) {
      fermion<mpsxx::PointGroup> qt(inp.N_elecs,inp.N_spins,mpsxx::PointGroup(inp.irrep));
      std::vector<fermion<mpsxx::PointGroup>> guess;
      mpsxx::mpsgen_impl(inp.name,pfix,inp.N_sites,0,qt,guess,inp.N_max_states);
    }
    mpsxx::load_mpss(inp.N_sites,C[0],0,pfix);
    mpsxx::compress(mpsxx::Gauge::RIGHT,C[0],inp.N_max_states);

    norm = mpsxx::dotc(C[0],C[0]);
    mpsxx::scal(1.0/sqrt(norm),C[0]);
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,C,0);
#endif

  pout << "\t\tGenerating initial vectors [ K = " << Kmin << " ] " << endl;
  for(size_t k = 1; k < Kmin; ++k) {
    mpsxx::gemv(CblasNoTrans,1.0,H,C[k-1],1.0,C[k],inp.N_max_states);
    norm = mpsxx::dotc(C[k],C[k]);
    mpsxx::scal(1.0/sqrt(norm),C[k]);
    for(size_t l = 0; l < k; ++l) {
      double ovlp = mpsxx::dotc(C[l],C[k]);
      mpsxx::axpy(-ovlp,C[l],C[k],inp.N_max_states);
      norm = mpsxx::dotc(C[k],C[k]);
      mpsxx::scal(1.0/sqrt(norm),C[k]);
    }
  }

  pout.precision(2);
  pout << "\t\t\tWall time: " << setw(8) << fixed << ts.lap() << endl;
  pout.precision(8);

  std::vector<double> esav(inp.N_roots,0.0);
  size_t iter = 0;
  size_t conv = 0;
  size_t M0 = 20; if(M0 > inp.N_max_states) M0 = inp.N_max_states;
  while(conv < inp.N_roots && iter < 100) {
    pout << "\t\tStarting Davidson iteration :: " << iter << endl;
    pout << "\t\t\tCurrent M0 = " << M0 << endl;
    // generate Krylov subspace
    for(size_t k = Kmin; k <= Kmax; ++k) {
      pout << "\t\t\tMicro iteration :: Size of trial space (k) = " << k << " / " << Kmax << endl;
      btas::TArray<double,2> HK(k,k);
      btas::TArray<double,2> SK(k,k);
      for(size_t i = 0; i < k; ++i) {
        HK(i,i) = mpsxx::hdot(C[i],H,C[i]);
        SK(i,i) = mpsxx::dotc(C[i],  C[i]);
        for(size_t j = 0; j < i; ++j) {
          HK(i,j) = mpsxx::hdot(C[i],H,C[j]);
          HK(j,i) = HK(i,j);
          SK(i,j) = mpsxx::dotc(C[i],  C[j]);
          SK(j,i) = SK(i,j);
        }
      }
//    pout << "\tDEBUG :: HK " << endl << HK << endl;
//    pout << "\tDEBUG :: SK " << endl << SK << endl;
      pout.precision(2);
      pout << "\t\t\tWall time for small matrix constructions: " << setw(8) << fixed << ts.lap() << endl;
      btas::TArray<double,2> UK(k,k);
      btas::TArray<double,1> EK(k);
      btas::Sygv(1,'V','U',HK,SK,EK,UK);

      if(M0 == inp.N_max_states) {
        for(conv = 0; conv < inp.N_roots; ++conv)
          if(fabs(EK(conv)-esav[conv]) >= inp.tolerance) break;
      }

      pout.precision(8);
      for(size_t i = 0; i < inp.N_roots; ++i) {
        esav[i] = EK(i);
        pout << "\t\t\tTrial Energy [" << setw(2) << i << "] :: " << setw(12) << fixed << esav[i] << endl;
      }

      pout.precision(2);
      pout << "\t\t\tWall time for diagonalization: " << setw(8) << fixed << ts.lap() << endl;

      std::vector<mpsxx::MPSXX_MPS_array<double,fermion<mpsxx::PointGroup>>> Ct = C;
      for(int i = 0; i < k; ++i) {
        mpsxx::scal(UK(i,i),C[i]);
      }
      for(int i = 0; i < k; ++i)
        for(int j = 0; j < k; ++j)
          if(i != j) {
            mpsxx::axpy(UK(i,j),Ct[i],C[j],inp.N_max_states);
          }

      pout.precision(2);
      pout << "\t\t\tWall time for rotations by Ritz vectors: " << setw(8) << fixed << ts.lap() << endl;

      if(conv < inp.N_roots && k < Kmax) {
        pout << "\t\t\tCompute correction vector for the root " << conv+1 << " and orthonormalize" << endl;
        double einv = 1.0/esav[conv];
        mpsxx::MPSXX_MPS_array<double,fermion<mpsxx::PointGroup>> R(C[conv]);
        mpsxx::gemv(CblasNoTrans,einv,H,C[conv],-1.0,R,inp.N_max_states);
        C[k] = R;
        mpsxx::gemv(CblasNoTrans,einv,D1,R,1.0,C[k],inp.N_max_states);
        R.swap(C[k]);
        mpsxx::gemv(CblasNoTrans,einv,D1,R,1.0,C[k],inp.N_max_states);
//
        pout.precision(2);
        pout << "\t\t\tWall time for correction equation: " << setw(8) << fixed << ts.lap() << endl;

        norm = mpsxx::dotc(C[k],C[k]);
        mpsxx::scal(1.0/sqrt(norm),C[k]);
        for(size_t l = 0; l < k; ++l) {
          double ovlp = mpsxx::dotc(C[l],C[k]);
          mpsxx::axpy(-ovlp,C[l],C[k],inp.N_max_states);
          norm = mpsxx::dotc(C[k],C[k]);
          mpsxx::scal(1.0/sqrt(norm),C[k]);
        }

        pout.precision(2);
        pout << "\t\t\tWall time for orthogonalization: " << setw(8) << fixed << ts.lap() << endl;
      }
      else {
        // Deflation
        pout << "\t\t\tDeflation to " << Kmin << endl;
        for(size_t k = Kmin; k < Kmax; ++k) C[k].clear();

        pout.precision(2);
        pout << "\t\t\tWall time for deflation: " << setw(8) << fixed << ts.lap() << endl;
      }
    }
    // increase M
    M0 *= 2; if(M0 > inp.N_max_states) M0 = inp.N_max_states;

    ++iter;
  }

  pout.precision(8);
  for(size_t i = 0; i < inp.N_roots; ++i) {
    pout << "\t\tFinal Energy [" << setw(2) << i << "] :: " << setw(12) << fixed << esav[i] << endl;
  }

  cout.rdbuf(backup);
  ofst.close();

  return 0;
}

// --- --- --- ---

int main (int argc, char* argv[])
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

#ifndef _SERIAL
  boost::mpi::environment env(argc,argv);
#endif
  Communicator world;

  std::string finp = "dmrg.conf";
  std::string fout;
  std::string fdmp = "FCIDUMP";
  std::string fodr;
  std::string pfix = ".";

  size_t Mw = 100;
  size_t Kmin = 2;

  for(int iarg = 0; iarg < argc; ++iarg) {
    if(strcmp(argv[iarg],"-i") == 0) finp = argv[++iarg];
    if(strcmp(argv[iarg],"-o") == 0) fout = argv[++iarg];
    if(strcmp(argv[iarg],"-s") == 0) pfix = argv[++iarg];
    if(strcmp(argv[iarg],"-f") == 0) fdmp = argv[++iarg];
    if(strcmp(argv[iarg],"-r") == 0) fodr = argv[++iarg];
    if(strcmp(argv[iarg],"-m") == 0) Mw = atoi(argv[++iarg]);
    if(strcmp(argv[iarg],"-k") == 0) Kmin = atoi(argv[++iarg]);
  }

  return fdmrg(finp,fout,fdmp,fodr,pfix,Mw,Kmin);
}
