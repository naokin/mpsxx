#include <iomanip>

#include "point_group.h"

// add include files hereafter...
#include "C1.h"
#include "Ci.h"
#include "C2.h"
#include "Cs.h"
#include "D2.h"
#include "C2h.h"
#include "C2v.h"
#include "D2h.h"

/// Symmetry group
mpsxx::GROUP mpsxx::PointGroup::symm_ = _C1;

/// Number of irreps
size_t mpsxx::PointGroup::N_reps_ = 1;

/// Product table
std::vector<mpsxx::IRREP> mpsxx::PointGroup::product_table_ = { 0 };

/// Irrep labels
std::vector<std::string> mpsxx::PointGroup::label_ = { "A" };

void mpsxx::PointGroup::set (GROUP g)
{
  symm_ = g;
  switch(g) {
    case _C1:
      std::cout << "\t\t\tSet PointGroup to be C1 symm." << std::endl;
      PG::C1::set(N_reps_,product_table_,label_); break;
    case _Ci:
      std::cout << "\t\t\tSet PointGroup to be Ci symm." << std::endl;
      PG::Ci::set(N_reps_,product_table_,label_); break;
    case _C2:
      std::cout << "\t\t\tSet PointGroup to be C2 symm." << std::endl;
      PG::C2::set(N_reps_,product_table_,label_); break;
    case _Cs:
      std::cout << "\t\t\tSet PointGroup to be Cs symm." << std::endl;
      PG::Cs::set(N_reps_,product_table_,label_); break;
    case _D2:
      std::cout << "\t\t\tSet PointGroup to be D2 symm." << std::endl;
      PG::D2::set(N_reps_,product_table_,label_); break;
    case _C2h:
      std::cout << "\t\t\tSet PointGroup to be C2h symm." << std::endl;
      PG::C2h::set(N_reps_,product_table_,label_); break;
    case _C2v:
      std::cout << "\t\t\tSet PointGroup to be C2v symm." << std::endl;
      PG::C2v::set(N_reps_,product_table_,label_); break;
    case _D2h:
      std::cout << "\t\t\tSet PointGroup to be D2h symm." << std::endl;
      PG::D2h::set(N_reps_,product_table_,label_); break;
    default:
      abort();
  }
}

std::ostream& operator<< (std::ostream& ost, const mpsxx::PointGroup& x)
{
  return ost << x.label();
}
