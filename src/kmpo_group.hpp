#ifndef __MPSXX_K_MPO_GROUP_HPP
#define __MPSXX_K_MPO_GROUP_HPP

#include <vector>
#include <algorithm>
#include <map>

namespace mpsxx {

//

typedef unsigned int OperatorString;

typedef unsigned int GroupIndex;

// N = 1 :  4*Norbs
// N = 2 : 16*Norbs*Norbs
// N = 3 : 64*Norbs*Norbs*Norbs (N/A)
template<size_t N>
size_t get_max_groups_ (const size_t& Norbs)
{
  // 4 (= 1111) : bits for an operator pair (eg. CreA_DesA = 1101)
  return get_max_groups_<N-1>(Norbs)*4*Norbs;
}

template<>
size_t get_max_groups_<0ul> (const size_t& Norbs)
{ return 1ul; }

//

template<size_t N>
GroupIndex get_group_ (const size_t& Norbs, const int& i, const int& j, const OperatorString& opstr)
{
  // NOTE: i <= j
  return get_group_<N-1>(Norbs,0,i,opstr)*Norbs+j; // shift {i,j} by calling function as (0,i)
}

template<>
GroupIndex get_group_<0ul>(const size_t& Norbs, const int& i, const int& j, const OperatorString& opstr)
{ return static_cast<GroupIndex>(opstr); }

//

template<size_t N>
double get_cost_ (const size_t& Norbs, const int& i, const int& j, const OperatorString& opstr)
{
  return 1.0;
}

template<size_t N>
double get_cost_ (const size_t& Norbs, const int& i, const int& j, const int& k, const int& l, const OperatorString& opstr)
{
  // 2p string
  //  0 : 0000 : DesB DesB P1
  //  1 : 0001 : DesB DesA P0
  //  2 : 0010 : DesB CreB Q0
  //  3 : 0011 : DesB CreA Q1
  //  4 : 0100 : DesA DesB P0
  //  5 : 0101 : DesA DesA P1
  //  6 : 0110 : DesA CreB Q1
  //  7 : 0111 : DesA CreA Q0
  //  8 : 1000 : CreB DesB Q0
  //  9 : 1001 : CreB DesA Q1
  // 10 : 1010 : CreB CreB P1
  // 11 : 1011 : CreB CreA P0
  // 12 : 1100 : CreA DesB Q1
  // 13 : 1101 : CreA DesA Q0
  // 14 : 1110 : CreA CreB P0
  // 15 : 1111 : CreA CreA P1
  switch(opstr) {
    // Q0
    case  2u:
    case  8u:
    case  7u:
    case 13u:
      return 16.0;
    // Q1
    case  3u:
    case  6u:
    case  9u:
    case 12u:
      return  8.0;
    // P0
    case  1u:
    case  4u:
    case 11u:
    case 14u:
      return  4.0;
    // P1
    case  0u:
    case  5u:
    case 10u:
    case 15u:
      return  2.0;
    // ELSE?
    default:
      return  1.0;
  }
}

template<>
double get_cost_<1ul> (const size_t& Norbs, const int& i, const int& j, const OperatorString& opstr)
{
  return 4.0;
}

template<>
double get_cost_<1ul> (const size_t& Norbs, const int& i, const int& j, const int& k, const int& l, const OperatorString& opstr)
{
  return 16.0*(Norbs-l);
}

//

template<size_t N>
class kmpo_group {

public:

  kmpo_group (const size_t& Norbs = 0)
  { this->reset(Norbs); }

  void reset (const size_t& Norbs)
  {
    Norbs_ = Norbs;
    //
    std::map<GroupIndex,double> gm;
    // 1p strings
    // 0010 : DesB CreB
    // 1000 : CreB DesB
    // 0111 : DesA CreA
    // 1101 : CreA DesA
    for(int j = 0; j < Norbs_; ++j) {
      for(int i = 0; i <= j; ++i) {
        gm[this->get_group(i,j, 2u)] = get_cost_<N>(Norbs_,i,j, 2u);
        gm[this->get_group(i,j, 8u)] = get_cost_<N>(Norbs_,i,j, 8u);
        gm[this->get_group(i,j, 7u)] = get_cost_<N>(Norbs_,i,j, 7u);
        gm[this->get_group(i,j,13u)] = get_cost_<N>(Norbs_,i,j,13u);
      }
    }
    // 2p stringm
    //  0 : 0000 : DesB DesB P1
    //  1 : 0001 : DesB DesA P0
    //  2 : 0010 : DesB CreB Q0
    //  3 : 0011 : DesB CreA Q1
    //  4 : 0100 : DesA DesB P0
    //  5 : 0101 : DesA DesA P1
    //  6 : 0110 : DesA CreB Q1
    //  7 : 0111 : DesA CreA Q0
    //  8 : 1000 : CreB DesB Q0
    //  9 : 1001 : CreB DesA Q1
    // 10 : 1010 : CreB CreB P1
    // 11 : 1011 : CreB CreA P0
    // 12 : 1100 : CreA DesB Q1
    // 13 : 1101 : CreA DesA Q0
    // 14 : 1110 : CreA CreB P0
    // 15 : 1111 : CreA CreA P1
    for(int l = 0; l < Norbs_; ++l) {
      for(int k = 0; k <= l; ++k) {
        for(int j = 0; j <= k; ++j) {
          for(int i = 0; i <= j; ++i) {
            gm[this->get_group(i,j,k,l, 0u)] = get_cost_<N>(Norbs_,i,j,k,l, 0u);
            gm[this->get_group(i,j,k,l, 1u)] = get_cost_<N>(Norbs_,i,j,k,l, 1u);
            gm[this->get_group(i,j,k,l, 2u)] = get_cost_<N>(Norbs_,i,j,k,l, 2u);
            gm[this->get_group(i,j,k,l, 3u)] = get_cost_<N>(Norbs_,i,j,k,l, 3u);
            gm[this->get_group(i,j,k,l, 4u)] = get_cost_<N>(Norbs_,i,j,k,l, 4u);
            gm[this->get_group(i,j,k,l, 5u)] = get_cost_<N>(Norbs_,i,j,k,l, 5u);
            gm[this->get_group(i,j,k,l, 6u)] = get_cost_<N>(Norbs_,i,j,k,l, 6u);
            gm[this->get_group(i,j,k,l, 7u)] = get_cost_<N>(Norbs_,i,j,k,l, 7u);
            gm[this->get_group(i,j,k,l, 8u)] = get_cost_<N>(Norbs_,i,j,k,l, 8u);
            gm[this->get_group(i,j,k,l, 9u)] = get_cost_<N>(Norbs_,i,j,k,l, 9u);
            gm[this->get_group(i,j,k,l,10u)] = get_cost_<N>(Norbs_,i,j,k,l,10u);
            gm[this->get_group(i,j,k,l,11u)] = get_cost_<N>(Norbs_,i,j,k,l,11u);
            gm[this->get_group(i,j,k,l,12u)] = get_cost_<N>(Norbs_,i,j,k,l,12u);
            gm[this->get_group(i,j,k,l,13u)] = get_cost_<N>(Norbs_,i,j,k,l,13u);
            gm[this->get_group(i,j,k,l,14u)] = get_cost_<N>(Norbs_,i,j,k,l,14u);
            gm[this->get_group(i,j,k,l,15u)] = get_cost_<N>(Norbs_,i,j,k,l,15u);
          }
        }
      }
    }
    //
    groups_.clear(); groups_.reserve(gm.size());
    flopsc_.clear(); flopsc_.reserve(gm.size());
    //
    for(auto it = gm.begin(); it != gm.end(); ++it) {
      groups_.push_back(it->first);
      flopsc_.push_back(it->second);
    }
  }

  const std::vector<GroupIndex>& groups () const { return groups_; }

  const std::vector<double>& flopsc () const { return flopsc_; }

  size_t get_max_groups () const { return get_max_groups_<N>(Norbs_); }

  GroupIndex get_group (const int& i, const int& j, const OperatorString& opstr) const
  {
    // NOTE: i <= j ... on-site operators can be summed w/o increase of M
    return (i == j) ? 0 : get_group_<N>(Norbs_,i,j,opstr);
  }

  GroupIndex get_group (const int& i, const int& j, const int& k, const int& l, const OperatorString& opstr) const
  {
    // NOTE: i <= j <= k <= l ... on-site operators can be summed w/o increase of M
    return (i == l) ? 0 : get_group_<N>(Norbs_,k,l,opstr&0x0f);
  }

  double get_cost (const GroupIndex& g) const
  {
    auto it = std::lower_bound(groups_.begin(),groups_.end(),g);
    assert(it != groups_.end()); // the group not found
    return flopsc_[std::distance(it,groups_.begin())];
  }

private:

  size_t Norbs_;

  std::vector<GroupIndex> groups_;

  std::vector<double> flopsc_;

}; // class kmpo_group

} // namespace mpsxx

#endif // __MPSXX_K_MPO_GROUP_H
