#ifndef __MPSXX_DMRG_INPUT_H
#define __MPSXX_DMRG_INPUT_H

#include <vector>
#include <string>
#include <ostream>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>

namespace mpsxx {

struct DMRGInput {

  std::string
    name; ///< operator name

  bool
    restart;
  bool
    load_mpos;

  std::vector<double>
    energy;

  size_t
    N_sites;

  std::string
    symmetry;

  int
    N_spins;
  int
    N_elecs;
  int
    irrep;

  int
    IonCore;

  size_t
    N_roots;
  int
    N_max_states;

  size_t
    algorithm;

  size_t
    N_max_sweep_iter;
  double
    tolerance;
  double
    noise;
  double
    shift;
  double
    tau;

  DMRGInput ()
  : name             ("mpo"),
    restart          ( 0 ),
    load_mpos        ( 0 ),
    N_sites          ( 0 ),
    symmetry         ( "C1" ),
    N_spins          ( 0 ),
    N_elecs          ( 0 ),
    irrep            ( 0 ),
    IonCore          (-1 ),
    N_roots          ( 1 ),
    N_max_states     ( 0 ),
    algorithm        ( 1 ),
    N_max_sweep_iter ( 100 ),
    tolerance        ( 1.0e-8 ),
    noise            ( 0.0 ),
    shift            ( 0.0 ),
    tau              ( 0.05 )
  { }

  DMRGInput (const std::string& fname)
  : DMRGInput () // Deligation (C++11)
  { this->parse(fname); }

  void parse (const std::string& fname);

private:

  friend class boost::serialization::access;

  /// Boost serialization
  template<class Archive>
  void serialize (Archive& ar, const unsigned int version)
  {
    ar & name;
    ar & restart;
    ar & load_mpos;
    ar & energy;
    ar & N_sites;
    ar & symmetry;
    ar & N_spins;
    ar & N_elecs;
    ar & irrep;
    ar & IonCore;
    ar & N_roots;
    ar & N_max_states;
    ar & algorithm;
    ar & N_max_sweep_iter;
    ar & tolerance;
    ar & noise;
    ar & shift;
    ar & tau;
  }

};

} // namespace mpsxx

std::ostream& operator<< (std::ostream& ost, const mpsxx::DMRGInput& input);

#endif // __MPSXX_DMRG_INPUT_H
