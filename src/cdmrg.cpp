#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

#include <boost/filesystem.hpp>
#include <btas/DENSE/TArray.h>

#include <time_stamp.h>

#include "mpidefs.h" // Communicator

#include "input.h"
#include "fileio.h"
#include "dmrg.hpp"
#include "mpogen_impl.hpp"
#include "mpogen_kmpos_impl.hpp"
#include "fermion.hpp"
#include "point_group.h"
#include "parsing_integral.h"
#include "expectation.hpp"

/// Do DMRG sweeps with compressed MPOs and then, evaluate the energy in 1 sweep.
/// \param finp input  file name.
/// \param fout output file name, output to stream if it is empty.
/// \param fdmp FCIDUMP file name.
/// \param fodr orbital ordering, no-reorder if it is empty.
/// \param pfix scratch file prefix.
int cdmrg (
  const std::string& finp,
  const std::string& fout,
  const std::string& fdmp,
  const std::string& fodr,
  const std::string& pfix,
  const size_t& D)
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

  Communicator world;

  // Check file specifications
  assert(!finp.empty());
  assert(!fdmp.empty());
  assert(!pfix.empty());

  bool enable_swap_sweep = false;
  bool do_compress = false;

  size_t nprocs = world.size();

  /* FIXME */ assert(nprocs == 1); /* for the time, serial run only */

  size_t iprint = 0;

  mpsxx::DMRGInput inp(finp);

  //
  // assign cout as alias to ofst
  //
  std::streambuf *backup;
  backup = cout.rdbuf();
  std::ofstream ofst;
  if(!fout.empty() > 0) {
    std::ostringstream oss;
    oss << fout << "." << world.rank();
    ofst.open(oss.str().c_str());
    cout.rdbuf(ofst.rdbuf());
  }

  int Norbs;
  int Nelec;
  double Ecore;
  std::vector<int> irreps;
  btas::TArray<double,2> oneint;
  btas::TArray<double,4> twoint;
  std::vector<int> reorder;

  if(world.rank() == 0) {
    std::ifstream idmp(fdmp.c_str());
    if(!fodr.empty()) {
      std::ifstream iodr(fodr.c_str());
      parsing_reorder(iodr,reorder);
      parsing_fcidump(idmp,Norbs,Nelec,Ecore,irreps,oneint,twoint,reorder);
    }
    else {
      parsing_fcidump(idmp,Norbs,Nelec,Ecore,irreps,oneint,twoint);
    }
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,Norbs,0);
  boost::mpi::broadcast(world,Nelec,0);
  boost::mpi::broadcast(world,Ecore,0);
  boost::mpi::broadcast(world,irreps,0);
  boost::mpi::broadcast(world,oneint,0);
  boost::mpi::broadcast(world,twoint,0);
  boost::mpi::broadcast(world,reorder,0);
#endif

  inp.N_sites = Norbs;

  cout << "\t====================================================================================================" << endl;
  cout << "\t\tGenerating QC MPOs from 1- and 2-particle integrals"                                                << endl;
  cout << "\t====================================================================================================" << endl;
  cout << endl;

  // Set symmetry
       if(inp.symmetry == "D2h") { mpsxx::PointGroup::set(mpsxx::_D2h); }
  else if(inp.symmetry == "C2v") { mpsxx::PointGroup::set(mpsxx::_C2v); }
  else if(inp.symmetry == "C2h") { mpsxx::PointGroup::set(mpsxx::_C2h); }
  else if(inp.symmetry == "D2" ) { mpsxx::PointGroup::set(mpsxx::_D2 ); }
  else if(inp.symmetry == "Cs" ) { mpsxx::PointGroup::set(mpsxx::_Cs ); }
  else if(inp.symmetry == "C2" ) { mpsxx::PointGroup::set(mpsxx::_C2 ); }
  else if(inp.symmetry == "Ci" ) { mpsxx::PointGroup::set(mpsxx::_Ci ); }
  else if(inp.symmetry == "C1" ) { mpsxx::PointGroup::set(mpsxx::_C1 ); }
  else {
    pout << "Requested symmetry [" << inp.symmetry << "] is not supported." << endl;
    abort();
  }

  std::vector<mpsxx::PointGroup> orbsym(irreps.size());
  for(size_t i = 0; i < irreps.size(); ++i) orbsym[i] = mpsxx::PointGroup(irreps[i]);

  time_stamp ts;

  if(!inp.restart) {
    //
    // generate exact MPOs
    //
    std::vector<int> rmdocc(Norbs,0); // this is not used.
    mpsxx::mpogen_impl(nprocs,inp.N_sites,Ecore,orbsym,rmdocc,oneint,twoint,"hmpo",pfix,true,false,true);

    inp.name = "hmpo";

    world.barrier();

    pout << endl;
    pout.precision(2);
    pout << "\t\t\tWall time for MPOs construction (1st step): " << setw(8) << fixed << ts.lap() << endl;

    //
    // generate compressed MPOs
    //

    if(D > 0) {
      // just copy MPOs on scratch
      for(size_t i = 0; i < inp.N_sites; ++i) {
        boost::filesystem::path orig(mpsxx::getfile("hmpo",pfix,i));
        boost::filesystem::path dest(mpsxx::getfile("cmpo",pfix,i));
        assert(boost::filesystem::exists(orig));
        boost::filesystem::copy_file(orig,dest,boost::filesystem::copy_option::overwrite_if_exists);
      }
      mpsxx::compress_mpos<fermion<mpsxx::PointGroup>>(false,inp.N_sites,"cmpo",pfix,D);

      inp.name = "cmpo";

      world.barrier();

      pout << endl;
      pout.precision(2);
      pout << "\t\t\tWall time for MPOs construction (2nd step): " << setw(8) << fixed << ts.lap() << endl;
    }
  }

  //
  // dmrg optimization
  //
  inp.energy = mpsxx::dmrg<fermion<mpsxx::PointGroup>>(
    inp.name,
    inp.restart,

    fermion<mpsxx::PointGroup>(
      inp.N_elecs,
      inp.N_spins,
      mpsxx::PointGroup(inp.irrep)),

    inp.algorithm,
    inp.N_sites,
    inp.N_max_states,
    pfix,
    inp.N_roots,
    inp.tolerance,
    inp.noise,
    inp.shift,
    inp.N_max_sweep_iter
  );

  world.barrier();

  pout << endl;
  pout.precision(2);
  pout << "\t\t\tWall time for DMRG optimization: " << setw(8) << fixed << ts.lap() << endl;
  pout << "\t\t\tWall time for Total calculation: " << setw(8) << fixed << ts.elapsed() << endl;

  //
  // enegy evaluation
  //
  inp.name = "hmpo";
  inp.energy[0] = mpsxx::expectation<fermion<mpsxx::PointGroup>>(inp.name,inp.N_sites,0,0,pfix);

  pout.precision(8);
  pout << "\t\tFinal energy :: " << setw(12) << inp.energy[0] << std::endl;

  cout.rdbuf(backup);
  ofst.close();

  return 0;
}

// --- --- --- ---

int main (int argc, char* argv[])
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

#ifndef _SERIAL
  boost::mpi::environment env(argc,argv);
#endif
  Communicator world;

  std::string finp = "dmrg.conf";
  std::string fout;
  std::string fdmp = "FCIDUMP";
  std::string fodr;
  std::string pfix = ".";

  size_t M = 0;

  for(int iarg = 0; iarg < argc; ++iarg) {
    if(strcmp(argv[iarg],"-i") == 0) finp = argv[++iarg];
    if(strcmp(argv[iarg],"-o") == 0) fout = argv[++iarg];
    if(strcmp(argv[iarg],"-s") == 0) pfix = argv[++iarg];
    if(strcmp(argv[iarg],"-f") == 0) fdmp = argv[++iarg];
    if(strcmp(argv[iarg],"-r") == 0) fodr = argv[++iarg];
    if(strcmp(argv[iarg],"-m") == 0) M = atoi(argv[++iarg]);
  }

  return cdmrg(finp,fout,fdmp,fodr,pfix,M);
}
