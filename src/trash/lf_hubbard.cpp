#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

#include <btas/DENSE/TArray.h>

#include <time_stamp.h>

#include "mpidefs.h" // Communicator

#include "input.h"
#include "dmrg.hpp"
#include "mpogen_impl.hpp"
#include "mpogen_kmpos_impl.hpp"
#include "fermion.hpp"
#include "point_group.h"
#include "physical_states.hpp"
#include "fileio.h"

#include "BFGS.hpp"

void DMRG (
        mpsxx::DMRGInput& input,
  const std::vector<double>& t,
  const std::vector<double>& U,
  const std::vector<double>& vpot,
  const std::vector<size_t>& reorder,
  const std::string& prefx)
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

  Communicator world;

  size_t Norbs = input.N_sites;
  size_t Nelec = input.N_elecs;
  //
  double Ecore = 0.0;
  btas::TArray<double,2> oneint(Norbs,Norbs);
  btas::TArray<double,4> twoint(Norbs,Norbs,Norbs,Norbs);

  std::fill(oneint.begin(),oneint.end(),0.0);
  for(size_t i = 0; i < Norbs; ++i) {
    size_t it = reorder[i];
    oneint(it,it) = vpot[i];
  }
  for(size_t i = 1; i < Norbs; ++i) {
    size_t it = reorder[i];
    size_t is = reorder[i-1];
    oneint(it,is) = t[i-1];
    oneint(is,it) = t[i-1];
  }
  // w/ periodic boundary
  oneint(reorder[0],reorder[Norbs-1]) = t[Norbs-1];
  oneint(reorder[Norbs-1],reorder[0]) = t[Norbs-1];

  std::fill(twoint.begin(),twoint.end(),0.0);
  for(size_t i = 0; i < Norbs; ++i) {
    size_t it = reorder[i];
    twoint(it,it,it,it) = U[i];
  }
//cout.precision(1);
//cout << "DEBUG :: 1e integrals " << endl << oneint << endl;
//cout << "DEBUG :: 2e integrals " << endl << twoint << endl;

#ifndef _SERIAL
  boost::mpi::broadcast(world,Norbs,0);
  boost::mpi::broadcast(world,Nelec,0);
  boost::mpi::broadcast(world,oneint,0);
  boost::mpi::broadcast(world,twoint,0);
#endif

  // Set symmetry to be C1
  mpsxx::PointGroup::set(mpsxx::_C1);
  std::vector<mpsxx::PointGroup> orbsym(Norbs,mpsxx::PointGroup::zero());

  // This is not used
  std::vector<int> rmdocc(Norbs,0);

  pout << input << endl;

  time_stamp ts;

  if(!input.load_mpos) mpsxx::mpogen_impl(1,Norbs,Ecore,orbsym,rmdocc,oneint,twoint,"mpo",prefx,true,true,true);

  world.barrier();

  pout << endl;
  pout.precision(2);
  pout << "\t\t\tWall time for MPOs construction: " << setw(8) << fixed << ts.lap() << endl;

  //
  // dmrg optimization
  //
  input.name = "mpo";
  input.energy = mpsxx::dmrg<fermion<mpsxx::PointGroup>>(
    input.name,
    input.restart,

    fermion<mpsxx::PointGroup>(
      input.N_elecs,
      input.N_spins,
      mpsxx::PointGroup(input.irrep)),

    input.algorithm,
    input.N_sites,
    input.N_max_states,
    prefx,
    input.N_roots,
    input.tolerance,
    input.noise,
    input.shift,
    input.N_max_sweep_iter
  );

  world.barrier();

  pout << endl;
  pout.precision(2);
  pout << "\t\t\tWall time for DMRG optimization: " << setw(8) << fixed << ts.lap() << endl;
  pout << "\t\t\tWall time for Total calculation: " << setw(8) << fixed << ts.elapsed() << endl;
}

std::vector<std::vector<double>> compute_occup_1 (
  const mpsxx::DMRGInput& input,
  const std::vector<size_t>& reorder,
  const std::string& prefx)
{
  size_t N = input.N_sites;
  size_t M = input.N_roots;
  //
  btas::Qshapes<fermion<mpsxx::PointGroup>> qn = mpsxx::PhysStates<fermion<mpsxx::PointGroup>>::quanta();
  btas::Dshapes                             dn(qn.size(),1);
  //
  btas::QSTArray<double,2ul,fermion<mpsxx::PointGroup>>
  Oi(fermion<mpsxx::PointGroup>::zero(),btas::make_array(qn,-qn),btas::make_array(dn,dn),false);
  //
  btas::TArray<double,2ul> I(1,1);
  I = 1.0;
  Oi.insert(btas::shape(1ul,1ul),I);
  Oi.insert(btas::shape(2ul,2ul),I);
  I = 2.0;
  Oi.insert(btas::shape(3ul,3ul),I);
  //
  std::vector<std::vector<double>> occn(M);
  for(size_t k = 0; k < M; ++k) {
    std::vector<double> occ1(N,0.0);
    for(size_t i = 0; i < N; ++i) {
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Ai;
      load(Ai,mpsxx::getfile("wave",prefx,reorder[i],k));
      //
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Bi;
      btas::Contract(1.0,Ai,btas::shape(0ul,3ul,1ul),Oi,btas::shape(2ul,3ul),1.0,Bi,btas::shape(0ul,2ul,1ul));
      //
      occ1[i] = btas::Dotc(Ai,Bi);
    }
    occn[k] = occ1;
  }
  //
  return occn;
}

std::vector<std::vector<double>> compute_occup_2 (
  const mpsxx::DMRGInput& input,
  const std::vector<size_t>& reorder,
  const std::string& prefx)
{
  size_t N = input.N_sites;
  size_t M = input.N_roots;
  //
  btas::Qshapes<fermion<mpsxx::PointGroup>> qn = mpsxx::PhysStates<fermion<mpsxx::PointGroup>>::quanta();
  btas::Dshapes                             dn(qn.size(),1);
  //
  btas::QSTArray<double,2ul,fermion<mpsxx::PointGroup>>
  Oi(fermion<mpsxx::PointGroup>::zero(),btas::make_array(qn,-qn),btas::make_array(dn,dn),false);
  //
  btas::TArray<double,2ul> I(1,1);
  I = 1.0;
  Oi.insert(btas::shape(3ul,3ul),I);
  //
  std::vector<std::vector<double>> occn(M);
  for(size_t k = 0; k < M; ++k) {
    std::vector<double> occ2(N,0.0);
    for(size_t i = 0; i < N; ++i) {
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Ai;
      load(Ai,mpsxx::getfile("wave",prefx,reorder[i],k));
      //
      btas::QSTArray<double,3ul,fermion<mpsxx::PointGroup>> Bi;
      btas::Contract(1.0,Ai,btas::shape(0ul,3ul,1ul),Oi,btas::shape(2ul,3ul),1.0,Bi,btas::shape(0ul,2ul,1ul));
      //
      occ2[i] = btas::Dotc(Ai,Bi);
    }
    occn[k] = occ2;
  }
  //
  return occn;
}

std::vector<double> LF_gradient (
        mpsxx::DMRGInput& input,
  const size_t& Nimp,
  const std::vector<double>& t,
  const std::vector<double>& U,
  const std::vector<double>& vpot,
  const std::vector<double>& occp,
  const std::vector<size_t>& reorder,
  const std::string& prefx)
{
  DMRG(input,t,U,vpot,reorder,prefx);
  //
  double Ev = input.energy[0];
  //
  std::vector<std::vector<double>> occ1 = compute_occup_1(input,reorder,prefx);
  std::vector<std::vector<double>> occ2 = compute_occup_2(input,reorder,prefx);
  //
  size_t N = input.N_sites;
  std::vector<double> grad(N,0.0);
  for(size_t i = Nimp; i < N; ++i) grad[i] = occp[i]-occ1[0][i];
  //
  double vn = 0.0;
  double gn = 0.0;
  std::cout.precision(8);
  std::cout << "\t======================================================================================" << std::endl;
  std::cout << "\t Site | Potential     | Occ. (calc.)  | Double Occ.   | Occ. (input)  | Gradient      " << std::endl;
  std::cout << "\t--------------------------------------------------------------------------------------" << std::endl;
  for(size_t i = 0; i < N; ++i) {
    std::cout << "\t"
      << std::setw( 5) << i
      << std::setw(16) << std::scientific << vpot[i]
      << std::setw(16) << std::scientific << occ1[0][i]
      << std::setw(16) << std::scientific << occ2[0][i]
      << std::setw(16) << std::scientific << occp[i]
      << std::setw(16) << std::scientific << grad[i]
      << std::endl;
    vn += vpot[i]*occp[i];
    gn += grad[i]*grad[i];
  }
  std::cout << "\t--------------------------------------------------------------------------------------" << std::endl;
  std::cout << "\tEv - (v|n) = " << std::setw(16) << std::fixed << Ev-vn << std::endl;
  std::cout << "\t======================================================================================" << std::endl;
  //
  gn = sqrt(gn);
  if(gn < 1.0e-4) {
    input.restart = true;
    input.algorithm = 1;
  }
  else {
    input.restart = false;
    input.algorithm = 2;
  }
  //
  return grad;
}

int main (int argc, char* argv[])
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;
  using std::scientific;

#ifndef _SERIAL
  boost::mpi::environment env(argc,argv);
#endif
  Communicator world;

  std::string finp = "dmrg.conf";
  std::string fout;
  std::string prefx = ".";

  double t0 =-1.0;
  double U0 = 1.0;

  size_t Nimp = 1;

  bool dmrgonly = false;
  enum BOUNDARY { OBC, PBC, APBC };
  BOUNDARY bc = OBC;

  for(int iarg = 0; iarg < argc; ++iarg) {
    if(strcmp(argv[iarg],"-i") == 0) finp = argv[++iarg];
    if(strcmp(argv[iarg],"-o") == 0) fout = argv[++iarg];
    if(strcmp(argv[iarg],"-s") == 0) prefx = argv[++iarg];
    if(strcmp(argv[iarg],"-t") == 0) t0 = atof(argv[++iarg]);
    if(strcmp(argv[iarg],"-U") == 0) U0 = atof(argv[++iarg]);
    if(strcmp(argv[iarg],"-n") == 0) Nimp = atoi(argv[++iarg]);
    if(strcmp(argv[iarg],"-v") == 0) dmrgonly = true;
    if(strcmp(argv[iarg],"-obc") == 0) bc = OBC;
    if(strcmp(argv[iarg],"-pbc") == 0) bc = PBC;
    if(strcmp(argv[iarg],"-apbc") == 0) bc = APBC;
  }

  mpsxx::DMRGInput input(finp);

  //
  // assign cout as alias to ofst
  //
  std::streambuf *backup;
  backup = cout.rdbuf();
  std::ofstream ofst;
  if(fout.size() > 0) {
    std::ostringstream oss;
    oss << fout << "." << world.rank();
    ofst.open(oss.str().c_str());
    cout.rdbuf(ofst.rdbuf());
  }

  size_t N = input.N_sites;
  size_t M = input.N_roots;

  std::vector<double> t(N,t0);
  std::vector<size_t> reorder(N);
  for(size_t i = 0; i < N; ++i) reorder[i] = i;
  //
  cout << "\tHubbard model (";
  switch (bc) {
    case  OBC:
      cout << "OBC)" << endl;
      t[N-1] = 0.0; // Open boundary
      break;
    case APBC:
      cout << "Anti-";
      t[N-1] = -t0; // Anti-periodic boundary
    case  PBC:
      cout << "PBC)" << endl;
//    for(size_t i = 1; i < N; ++i) {
//      // snake ordering
//      size_t m = i%4;
//      size_t d = (i-1)/2;
//      switch(m) {
//        case 1ul:
//        case 2ul:
//          reorder[  m+d  ] = i; break;
//        case 0ul:
//        case 3ul:
//          reorder[N-i+d+1] = i; break;
//      }
//      // zig-zag ordering
//      size_t d = (i+1)/2;
//      if(i & 1)
//        reorder[  d] = i;
//      else
//        reorder[N-d] = i;
//    }
      break;
  }

  std::vector<double> U(N,U0);
  std::vector<double> vpot(N,0.0);

  input.restart = false;
  input.load_mpos = false;
  input.algorithm = 2;

  cout.precision(8);
  cout << "\tt = " << setw(12) << fixed << t0 << endl;
  cout << "\tU = " << setw(12) << fixed << U0 << endl;
//cout << "\tReordering: "; for(size_t i = 0; i < N-1; ++i) cout << reorder[i] << ","; cout << reorder[N-1] << endl;

  DMRG(input,t,U,vpot,reorder,prefx);

  double E0 = input.energy[0];

  std::vector<std::vector<double>> occ1 = compute_occup_1(input,reorder,prefx);
  std::vector<std::vector<double>> occ2 = compute_occup_2(input,reorder,prefx);

  cout << "\t======================================" << endl;
  for(size_t k = 0; k < M; ++k) {
    cout.precision(8);
    cout << "\t Root = " << setw(2) << k << endl;
    cout << "\t======================================" << endl;
    cout << "\t Site | Occupation #1 | Occupation #2 " << endl;
    cout << "\t--------------------------------------" << endl;
    for(size_t i = 0; i < N; ++i) {
      cout << "\t" << setw(5) << i << setw(16) << scientific << occ1[k][i] << setw(16) << scientific << occ2[k][i] << endl;
    }
    cout << "\t--------------------------------------" << endl;
    cout << "\tE0 = " << setw(16) << fixed << E0 << endl;
    cout << "\t======================================" << endl;
  }

  if(!dmrgonly) {

  // initial guess
  for(size_t i = Nimp; i < N; ++i) vpot[i] = U[i]/2;
  // turn-off 2e repulsions on bath sites
  for(size_t i = Nimp; i < N; ++i) U[i] = 0.0;

  boost::function<std::vector<double>(const std::vector<double>&)>
  Fn = boost::bind(LF_gradient,input,Nimp,t,U,_1,occ1[0],reorder,prefx);
  BFGS solver(Fn,vpot);

  } // !dmrgonly

  cout.rdbuf(backup);
  ofst.close();

  return 0;
}

