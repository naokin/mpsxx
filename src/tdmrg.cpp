#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

#include <boost/filesystem.hpp>
#include <btas/DENSE/TArray.h>

#include <time_stamp.h>

#include "mpidefs.h" // Communicator

#include "input.h"
#include "fileio.h"
#include "dmrg.hpp"
#include "mpogen_impl.hpp"
#include "fermion.hpp"
#include "point_group.h"
#include "parsing_integral.h"
#include "mpsgen_impl.hpp"

#include "mulas/mulas.hpp"
#include "mulas/muexp.hpp"
#include "mulas/hdot.hpp"
#include "mulas/load_from_disk.hpp"

/// Do DMRG sweeps with compressed MPOs and then, evaluate the energy in 1 sweep.
/// \param finp input  file name.
/// \param fout output file name, output to stream if it is empty.
/// \param fdmp FCIDUMP file name.
/// \param fodr orbital ordering, no-reorder if it is empty.
/// \param pfix scratch file prefix.
int tdmrg (
  const std::string& finp,
  const std::string& fout,
  const std::string& fdmp,
  const std::string& fodr,
  const std::string& pfix,
  const size_t& Nd, ///< order of time-step division
  const size_t& Np, ///< order of truncation
  const size_t& Mw) ///< max. # of bonds to be kept for MPO
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

  Communicator world;

  // Check file specifications
  assert(!finp.empty());
  assert(!fdmp.empty());
  assert(!pfix.empty());

  bool enable_swap_sweep = false;
  bool do_compress = false;

  size_t nprocs = world.size();

  /* FIXME */ assert(nprocs == 1); /* for the time, serial run only */

  size_t iprint = 0;

  mpsxx::DMRGInput inp(finp);

  //
  // assign cout as alias to ofst
  //
  std::streambuf *backup;
  backup = cout.rdbuf();
  std::ofstream ofst;
  if(!fout.empty() > 0) {
    std::ostringstream oss;
    oss << fout << "." << world.rank();
    ofst.open(oss.str().c_str());
    cout.rdbuf(ofst.rdbuf());
  }

  int Norbs;
  int Nelec;
  double Ecore;
  std::vector<int> irreps;
  btas::TArray<double,2> oneint;
  btas::TArray<double,4> twoint;
  std::vector<int> reorder;

  if(world.rank() == 0) {
    std::ifstream idmp(fdmp.c_str());
    if(!fodr.empty()) {
      std::ifstream iodr(fodr.c_str());
      parsing_reorder(iodr,reorder);
      parsing_fcidump(idmp,Norbs,Nelec,Ecore,irreps,oneint,twoint,reorder);
    }
    else {
      parsing_fcidump(idmp,Norbs,Nelec,Ecore,irreps,oneint,twoint);
    }
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,Norbs,0);
  boost::mpi::broadcast(world,Nelec,0);
  boost::mpi::broadcast(world,Ecore,0);
  boost::mpi::broadcast(world,irreps,0);
  boost::mpi::broadcast(world,oneint,0);
  boost::mpi::broadcast(world,twoint,0);
  boost::mpi::broadcast(world,reorder,0);
#endif

  inp.N_sites = Norbs;

  pout << "\tDEBUG :: Np = " << Np << endl;
  pout << "\tDEBUG :: Mw = " << Mw << endl;
  pout << "\tDEBUG :: M  = " << inp.N_max_states << endl;

  cout << "\t====================================================================================================" << endl;
  cout << "\t\tGenerating QC MPOs from 1- and 2-particle integrals"                                                << endl;
  cout << "\t====================================================================================================" << endl;
  cout << endl;

  // Set symmetry
       if(inp.symmetry == "D2h") { mpsxx::PointGroup::set(mpsxx::_D2h); }
  else if(inp.symmetry == "C2v") { mpsxx::PointGroup::set(mpsxx::_C2v); }
  else if(inp.symmetry == "C2h") { mpsxx::PointGroup::set(mpsxx::_C2h); }
  else if(inp.symmetry == "D2" ) { mpsxx::PointGroup::set(mpsxx::_D2 ); }
  else if(inp.symmetry == "Cs" ) { mpsxx::PointGroup::set(mpsxx::_Cs ); }
  else if(inp.symmetry == "C2" ) { mpsxx::PointGroup::set(mpsxx::_C2 ); }
  else if(inp.symmetry == "Ci" ) { mpsxx::PointGroup::set(mpsxx::_Ci ); }
  else if(inp.symmetry == "C1" ) { mpsxx::PointGroup::set(mpsxx::_C1 ); }
  else {
    pout << "Requested symmetry [" << inp.symmetry << "] is not supported." << endl;
    abort();
  }

  std::vector<mpsxx::PointGroup> orbsym(irreps.size());
  for(size_t i = 0; i < irreps.size(); ++i) orbsym[i] = mpsxx::PointGroup(irreps[i]);

  time_stamp ts;

  if(!inp.restart) {
    //
    // generate exact MPOs
    //
    std::vector<int> rmdocc(Norbs,0); // this is not used.
    mpsxx::mpogen_impl(nprocs,inp.N_sites,Ecore,orbsym,rmdocc,oneint,twoint,"hmpo",pfix,true,false,true);

    inp.name = "hmpo";

    world.barrier();

    pout << endl;
    pout.precision(2);
    pout << "\t\t\tWall time for MPOs construction (1st step): " << setw(8) << fixed << ts.lap() << endl;

    //
    // generate compressed MPOs
    //

    if(Mw > 0) {
      // just copy MPOs on scratch
      for(size_t i = 0; i < inp.N_sites; ++i) {
        boost::filesystem::path orig(mpsxx::getfile("hmpo",pfix,i));
        boost::filesystem::path dest(mpsxx::getfile("cmpo",pfix,i));
        assert(boost::filesystem::exists(orig));
        boost::filesystem::copy_file(orig,dest,boost::filesystem::copy_option::overwrite_if_exists);
      }
      mpsxx::compress_mpos<fermion<mpsxx::PointGroup>>(false,inp.N_sites,"cmpo",pfix,Mw);

      inp.name = "cmpo";

      world.barrier();

      pout << endl;
      pout.precision(2);
      pout << "\t\t\tWall time for MPOs construction (2nd step): " << setw(8) << fixed << ts.lap() << endl;
    }
  }

  //
  // dmrg optimization
  //
//inp.energy = mpsxx::dmrg<fermion<mpsxx::PointGroup>>(
//  inp.name,
//  inp.restart,

//  fermion<mpsxx::PointGroup>(
//    inp.N_elecs,
//    inp.N_spins,
//    mpsxx::PointGroup(inp.irrep)),

//  inp.algorithm,
//  inp.N_sites,
//  inp.N_max_states,
//  pfix,
//  inp.N_roots,
//  inp.tolerance,
//  inp.noise,
//  inp.shift,
//  inp.N_max_sweep_iter
//);

  //
  // Load MPOs from Disk to RAM
  //
  std::complex<double> ONE(1.0,0.0);

  pout << "\tDEBUG :: loading QC-MPOs from scratch..." << endl;
  mpsxx::MPSXX_MPO_array<std::complex<double>,fermion<mpsxx::PointGroup>> H;
  mpsxx::load_mpos_complex(inp.N_sites,inp.name,H,pfix);
  if(inp.shift != 0.0) {
    mpsxx::MPSXX_MPO_array<std::complex<double>,fermion<mpsxx::PointGroup>> Id;
    Id.resize(H.size());
    btas::Qshapes<fermion<mpsxx::PointGroup>> q0(1,fermion<mpsxx::PointGroup>::zero());
    btas::Dshapes    d0(1,1);
    for(size_t i = 0; i < H.size(); ++i) {
      Id[i].resize(fermion<mpsxx::PointGroup>::zero(),
                  btas::make_array(q0,H[i].qshape(1),H[i].qshape(2),q0),
                  btas::make_array(d0,H[i].dshape(1),H[i].dshape(2),d0),ONE);
    }

    std::complex<double> E0(inp.shift,0.0);
    mpsxx::axpy(-E0,Id,H,Mw); 
  }
//std::complex<double> dt = std::complex<double>(inp.tau,0.0);
  std::complex<double> dt = std::complex<double>(0.0,inp.tau); // real-time evoluation
  mpsxx::scal(-dt,H); // -H*dt

  pout.precision(6);
  pout << "\tDEBUG :: computing exp(-Hdt)... ( dt = " << setw(10) << fixed << dt << " ) " << endl;
  mpsxx::MPSXX_MPO_array<std::complex<double>,fermion<mpsxx::PointGroup>> expH;
  mpsxx::muexp(H,expH,Np,Mw);

  pout << endl;
  pout.precision(2);
  pout << "\t\t\tWall time for computing exp(-Hdt): " << setw(8) << fixed << ts.lap() << " sec." << endl;

  pout << "\tDEBUG :: loading the exact QC-MPOs from scratch..." << endl;
  mpsxx::load_mpos_complex(inp.N_sites,"hmpo",H,pfix);

  pout << "\tDEBUG :: generating random MPSs as an initial state..." << endl;
  fermion<mpsxx::PointGroup> qt(inp.N_elecs,inp.N_spins,mpsxx::PointGroup(inp.irrep));
  std::vector<fermion<mpsxx::PointGroup>> guess;
  if(!inp.restart) mpsxx::mpsgen_impl(inp.name,pfix,inp.N_sites,0,qt,guess,inp.N_max_states);

  pout << "\tDEBUG :: loading MPSs from scratch..." << endl;
  mpsxx::MPSXX_MPS_array<std::complex<double>,fermion<mpsxx::PointGroup>> C0;
  mpsxx::load_mpss_complex(inp.N_sites,C0,0,pfix);

  std::complex<double> norm;

  // Normalize C0
  norm = mpsxx::dotc(C0,C0);
//mpsxx::scal(ONE/sqrt(norm.real()),C0);
  pout.precision(8);
  pout << "\tDEBUG :: initial wavefunction norm = " << setw(12) << fixed << norm << endl;

  //
  // Imaginary time-evolution
  //
  double esav = 0.0;
  for(size_t t = 0; t < inp.N_max_sweep_iter; ++t) {
    // expH * C0
    mpsxx::MPSXX_MPS_array<std::complex<double>,fermion<mpsxx::PointGroup>> Ct;
    mpsxx::gemv(CblasNoTrans,ONE,expH,C0,ONE,Ct,inp.N_max_states);
    pout.precision(2);
    pout << "\tDEBUG :: 01 computed Ct = exp(-Hdt)*|C0> :: (" << setw(8) << fixed << ts.lap() << " sec.)" << endl;
    // Normalize Ct
    norm = mpsxx::dotc(Ct,Ct);
//  mpsxx::scal(ONE/sqrt(norm.real()),Ct);
    pout.precision(8);
    pout << "\tDEBUG :: wavefunction norm = " << setw(12) << fixed << norm << endl;
    C0.swap(Ct);
    pout.precision(2);
    pout << "\tDEBUG :: 02 normalize Ct (" << setw(8) << fixed << ts.lap() << " sec.)" << endl;
    // Energy evaluation
//  Ct.clear();
//  mpsxx::gemv(CblasNoTrans,ONE,H,C0,ONE,Ct,0);
//  double energy = mpsxx::dotc(C0,Ct);
    std::complex<double> eswp = mpsxx::hdot(C0,H,C0)/norm.real();
    pout.precision(2);
    pout << "\tDEBUG :: 03 computed <Ct|H|Ct> (" << setw(8) << fixed << ts.lap() << " sec.)" << endl;
    pout.precision(12);
    pout << "\t\tEnergy[" << setw(6) << t << "] = " << fixed << setw(16) << eswp << endl;
    // Check convergence
//  if(fabs(eswp.real()-esav) < inp.tolerance) break;
//  esav = eswp.real();
  }

  cout.rdbuf(backup);
  ofst.close();

  return 0;
}

// --- --- --- ---

int main (int argc, char* argv[])
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

#ifndef _SERIAL
  boost::mpi::environment env(argc,argv);
#endif
  Communicator world;

  std::string finp = "dmrg.conf";
  std::string fout;
  std::string fdmp = "FCIDUMP";
  std::string fodr;
  std::string pfix = ".";

  size_t Nd = 1;
  size_t Np = 2;
  size_t Mw = 0;

  for(int iarg = 0; iarg < argc; ++iarg) {
    if(strcmp(argv[iarg],"-i") == 0) finp = argv[++iarg];
    if(strcmp(argv[iarg],"-o") == 0) fout = argv[++iarg];
    if(strcmp(argv[iarg],"-s") == 0) pfix = argv[++iarg];
    if(strcmp(argv[iarg],"-f") == 0) fdmp = argv[++iarg];
    if(strcmp(argv[iarg],"-r") == 0) fodr = argv[++iarg];
    if(strcmp(argv[iarg],"-m") == 0) Mw = atoi(argv[++iarg]);
    if(strcmp(argv[iarg],"-d") == 0) Nd = atoi(argv[++iarg]);
    if(strcmp(argv[iarg],"-p") == 0) Np = atoi(argv[++iarg]);
  }

  return tdmrg(finp,fout,fdmp,fodr,pfix,Nd,Np,Mw);
}
