#ifndef __MPSXX_MPOGEN_K_MPOS_IMPL_HPP
#define __MPSXX_MPOGEN_K_MPOS_IMPL_HPP

#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>

#include <boost/filesystem.hpp>
#include <boost/serialization/vector.hpp>

#include <btas/DENSE/TArray.h>

#include "mpidefs.h"
#include "fileio.h"
#include "fermion.hpp"
#include "compress_mpos.hpp"

#include "kmpo_group.hpp"
#include "gen_qc_kmpos.hpp"

namespace mpsxx {

template<size_t N, class Symmetry>
void mpogen_kmpos_impl (
  const size_t& Norbs,
  const double& Ecore,
  const std::vector<Symmetry>& orbsym,
  const std::vector<int>& rmdocc,
  const btas::TArray<double,2>& oneint,
  const btas::TArray<double,4>& twoint,
  const std::string& opname,
  const std::string& prefix,
  const bool& enable_swap_sweep,
  const bool& hubbard,
  const bool& do_compress)
{
  Communicator world;
  size_t nproc = world.size();
  size_t iproc = world.rank();

  kmpo_group<N> grouphandler(Norbs);
  std::vector<GroupIndex> local_groups;

  // Doing load-balancing stuff.
  if(world.rank() == 0) {

    const auto& groups = grouphandler.groups();
    const auto& flopsc = grouphandler.flopsc();

    std::vector<double> lbtask(nproc,0);
    std::vector<std::vector<GroupIndex>> groups_list(nproc);

//  std::multimap<double,GroupIndex> lbmap;
//  for(auto ig = groups.begin(); ig != groups.end(); ++ig)
//    lbmap.insert(std::make_pair(grouphandler.get_cost(*ig),*ig));

//  for(auto it = lbmap.rbegin(); it != lbmap.rend(); ++it) {
//    auto ip = std::min_element(lbtask.begin(),lbtask.end());
//    *ip += it->first;
//    size_t i = std::distance(lbtask.begin(),ip);
//    groups_list[i].push_back(it->second);
//  }
    for(size_t i = 0; i < groups.size(); ++i) {
      groups_list[i%nproc].push_back(groups[i]);
      lbtask[i%nproc] += flopsc[i];
    }

    std::cout << "\t----------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "\tGroup list to be generated..." << std::endl;
    std::cout << "\t----------------------------------------------------------------------------------------------------" << std::endl;
    for(size_t i = 0; i < nproc; ++i) {
      std::cout << "\tProcess[" << std::setw(3) << i << "] :: ";
      for(size_t j = 0; j < groups_list[i].size(); ++j) {
        if(j % 10 == 0) std::cout << std::endl << "\t\t";
        std::cout << std::setw(6) << groups_list[i][j] << ",";
      }
      std::cout << std::endl << "\t\tEstmCost = " << lbtask[i] << std::endl;
    }

    local_groups = groups_list[0];
#ifndef _SERIAL
    for(size_t i = 1; i < nproc; ++i)
      world.send(i,i,groups_list[i]);
  }
  else {
    world.recv(0,iproc,local_groups);
#endif
  }

  // Generate MPOs for each group to be stored in local process.
  auto ig = local_groups.begin();
  while(ig != local_groups.end()) {
//for(size_t i = 0; i < groups.size(); ++i) {

    double e = 0.0; if(*ig == 0) e = Ecore;

    std::ostringstream oss;
    oss << "mpo-scratch-" << *ig;
    int info = gen_qc_kmpos(grouphandler,*ig,Norbs,e,orbsym,rmdocc,oneint,twoint,oss.str(),prefix,enable_swap_sweep,hubbard);

    if(info == 0 && do_compress)
      info = compress_mpos<fermion<Symmetry>>(Norbs,oss.str(),prefix);

    if(info == 1) {
      for(size_t s = 0; s < Norbs; ++s) removefile(getfile(oss.str(),prefix,s));
      ig = local_groups.erase(ig);
    }
    else {
      ++ig;
    }
  }

  // Gather all MPOs that locally generated on disk.
  for(size_t s = 0; s < Norbs; ++s) {

    std::vector<btas::QSTArray<double,4,fermion<Symmetry>>> mpo(local_groups.size());

    for(size_t i = 0; i < local_groups.size(); ++i) {

      std::ostringstream oss;
      oss << "mpo-scratch-" << local_groups[i];
      load(mpo[i],getfile(oss.str(),prefix,s));

      // remove mpo-scratch....
      removefile(getfile(oss.str(),prefix,s));
    }
    save(mpo,getfile(opname,prefix,s));
  }
}

} // namespace mpsxx

#endif // __MPSXX_MPOGEN_K_MPOS_IMPL_HPP
