#ifndef __MPSXX_DMRG_HPP
#define __MPSXX_DMRG_HPP

#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>

#include <time_stamp.h>

#include "mpidefs.h" // Communicator

#include "make_sweep.hpp"

namespace mpsxx {

/// The main dmrg routine.
/// \param input DMRGInput object which contains the diferent parameters that define this dmrg run
template<class Q>
std::vector<double> dmrg (
  const std::string& name,
  const bool& restart,
  const Q& qt,
  const size_t& D,
  const size_t& N,
  const size_t& M,
  const std::string& prefix = ".",
  const size_t& N_roots = 1,
  const double& tolerance = 1.0e-7,
  const double& noise = 0.0,
  const double& shift = 0.0,
  const size_t& MAX_ITER = 200)
{
  using std::endl;
  using std::setw;
  using std::setprecision;
  using std::fixed;
  using std::scientific;

  std::vector<double> esav; esav.reserve(N_roots);

  double noise0 = noise;
  size_t M0     = M;

  if(!restart && D > 1) M0 = 200;

  Communicator world;

  time_stamp ts;

  for(size_t iroot = 0; iroot < N_roots; ++iroot) {

    esav.push_back(0.0);

    unsigned char iswp = 3u; // 11
    bool conv = false;
    // Optimization with sweep algorithm
    for(size_t iter = 1; iter <= MAX_ITER && !conv; ++iter) {

      pout << "\t====================================================================================================" << endl;
      pout << "\t\tSWEEP ITERATION [ " << setw(4) << iter << " ] :: ROOT = " << setw(2) << iroot << endl;
      pout << "\t====================================================================================================" << endl;

      ts.start();

      if(iswp && iter%2 == 0) {
        if(iswp & 1u) {
          noise0 /= 10;
          if(noise0 < tolerance) {
            iswp &= 2u;
            noise0 = 0.0;
          }
        }
        if(iswp & 2u) {
          M0 *= 2;
          if(M0 >= M) {
            iswp &= 1u;
            M0 = M;
          }
        }
      }

      double eswp = make_sweep<Q>(name,D,N,M0,prefix,iroot,tolerance*0.1,noise0,shift);

      double edif = eswp-esav[iroot];
      pout << "\t====================================================================================================" << endl;
      pout << "\t\tSWEEP ITERATION [ " << setw(4) << iter << " ] :: ROOT = " << setw(2) << iroot << " FINISHED"
           << " ( " << fixed << setprecision(2) << setw(8) << ts.lap() << " sec. ) " << endl;
      pout.precision(16);
      pout << "\t\t\tSweep Energy = " << setw(24) << fixed << eswp << " ( delta E = ";
      pout.precision(2);
      pout << setw(8) << scientific << edif << " ) " << endl;
      pout << endl;
      if(world.rank() == 0) {
        esav[iroot] = eswp;
        if(iter > 1 && !iswp && fabs(edif) < tolerance) conv = true;
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,conv,0);
      boost::mpi::broadcast(world,esav,0);
#endif
    }
    // Stop by no convergence
    if(!conv) {
      pout << "\t+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-" << endl;
      pout << "\t\tNO CONVERGENCE MET FOR ROOT = " << setw(2) << iroot << endl;
      pout << "\t\tPROGRAM STOPPED..." << endl;
      break;
    }

    if(D == 1) continue;

    conv = false;
    // Optimization with sweep algorithm
    for(size_t iter = 1; iter <= MAX_ITER && !conv; ++iter) {

      pout << "\t====================================================================================================" << endl;
      pout << "\t\tSWEEP ITERATION [ " << setw(4) << iter << " ] :: ROOT = " << setw(2) << iroot << endl;
      pout << "\t====================================================================================================" << endl;

      ts.start();

      double eswp = make_sweep<Q>(name,1,N,M,prefix,iroot,tolerance*0.1,0.0,shift);

      double edif = eswp-esav[iroot];
      pout << "\t====================================================================================================" << endl;
      pout << "\t\tSWEEP ITERATION [ " << setw(4) << iter << " ] :: ROOT = " << setw(2) << iroot << " FINISHED"
           << " ( " << fixed << setprecision(2) << setw(8) << ts.lap() << " sec. ) " << endl;
      pout.precision(16);
      pout << "\t\t\tSweep Energy = " << setw(24) << fixed << eswp << " ( delta E = ";
      pout.precision(2);
      pout << setw(8) << scientific << edif << " ) " << endl;
      pout << endl;
      if(world.rank() == 0) {
        esav[iroot] = eswp;
        if(iter > 1 && fabs(edif) < tolerance) conv = true;
      }
#ifndef _SERIAL
      boost::mpi::broadcast(world,conv,0);
      boost::mpi::broadcast(world,esav,0);
#endif
    }
    // Stop by no convergence
    if(!conv) {
      pout << "\t+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-" << endl;
      pout << "\t\tNO CONVERGENCE MET FOR ROOT = " << setw(2) << iroot << endl;
      pout << "\t\tPROGRAM STOPPED..." << endl;
      break;
    }

    if(world.rank() == 0) {
      save(esav,getfile("esav",prefix,0));
    }
  }
  pout << "\t====================================================================================================" << endl;
  pout.precision(16);
  for(size_t iroot = 0; iroot < N_roots; ++iroot) {
    pout << "\t\t\tSweep Energy = " << setw(24) << fixed << esav[iroot] << endl;
  }

  return esav;
}

} // namespace mpsxx

#endif // __MPSXX_DMRG_HPP
