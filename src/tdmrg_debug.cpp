#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

#include <boost/filesystem.hpp>
#include <btas/DENSE/TArray.h>

#include <time_stamp.h>

#include "mpidefs.h" // Communicator

#include "input.h"
#include "fileio.h"
#include "mpogen_impl.hpp"
#include "fermion.hpp"
#include "point_group.h"
#include "parsing_integral.h"
#include "make_random_mpss.hpp"

#include "mulas/mulas.hpp"
#include "mulas/muexp.hpp"
#include "mulas/hdot.hpp"
#include "mulas/load_from_disk.hpp"

/// Do DMRG sweeps with compressed MPOs and then, evaluate the energy in 1 sweep.
/// \param finp input  file name.
/// \param fout output file name, output to stream if it is empty.
/// \param fdmp FCIDUMP file name.
/// \param fodr orbital ordering, no-reorder if it is empty.
/// \param pfix scratch file prefix.
int tdmrg (
  const std::string& finp,
  const std::string& fout,
  const std::string& fdmp,
  const std::string& fodr,
  const std::string& pfix,
  const size_t& Nd, ///< order of time-step division
  const size_t& Np, ///< order of truncation
  const size_t& Mw) ///< max. # of bonds to be kept for MPO
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

  Communicator world;

  // Check file specifications
  assert(!finp.empty());
  assert(!fdmp.empty());
  assert(!pfix.empty());

  bool enable_swap_sweep = false;
  bool do_compress = false;

  size_t nprocs = world.size();

  /* FIXME */ assert(nprocs == 1); /* for the time, serial run only */

  size_t iprint = 0;

  mpsxx::DMRGInput inp(finp);

  //
  // assign cout as alias to ofst
  //
  std::streambuf *backup;
  backup = cout.rdbuf();
  std::ofstream ofst;
  if(!fout.empty() > 0) {
    std::ostringstream oss;
    oss << fout << "." << world.rank();
    ofst.open(oss.str().c_str());
    cout.rdbuf(ofst.rdbuf());
  }

  int Norbs;
  int Nelec;
  double Ecore;
  std::vector<int> irreps;
  btas::TArray<double,2> oneint;
  btas::TArray<double,4> twoint;
  std::vector<int> reorder;

  if(world.rank() == 0) {
    std::ifstream idmp(fdmp.c_str());
    if(!fodr.empty()) {
      std::ifstream iodr(fodr.c_str());
      parsing_reorder(iodr,reorder);
      parsing_fcidump(idmp,Norbs,Nelec,Ecore,irreps,oneint,twoint,reorder);
    }
    else {
      parsing_fcidump(idmp,Norbs,Nelec,Ecore,irreps,oneint,twoint);
    }
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,Norbs,0);
  boost::mpi::broadcast(world,Nelec,0);
  boost::mpi::broadcast(world,Ecore,0);
  boost::mpi::broadcast(world,irreps,0);
  boost::mpi::broadcast(world,oneint,0);
  boost::mpi::broadcast(world,twoint,0);
  boost::mpi::broadcast(world,reorder,0);
#endif

  inp.N_sites = Norbs;

  cout << "\t====================================================================================================" << endl;
  cout << "\t\tGenerating QC MPOs from 1- and 2-particle integrals"                                                << endl;
  cout << "\t====================================================================================================" << endl;
  cout << endl;

  // Set symmetry
       if(inp.symmetry == "D2h") { mpsxx::PointGroup::set(mpsxx::_D2h); }
  else if(inp.symmetry == "C2v") { mpsxx::PointGroup::set(mpsxx::_C2v); }
  else if(inp.symmetry == "C2h") { mpsxx::PointGroup::set(mpsxx::_C2h); }
  else if(inp.symmetry == "D2" ) { mpsxx::PointGroup::set(mpsxx::_D2 ); }
  else if(inp.symmetry == "Cs" ) { mpsxx::PointGroup::set(mpsxx::_Cs ); }
  else if(inp.symmetry == "C2" ) { mpsxx::PointGroup::set(mpsxx::_C2 ); }
  else if(inp.symmetry == "Ci" ) { mpsxx::PointGroup::set(mpsxx::_Ci ); }
  else if(inp.symmetry == "C1" ) { mpsxx::PointGroup::set(mpsxx::_C1 ); }
  else {
    pout << "Requested symmetry [" << inp.symmetry << "] is not supported." << endl;
    abort();
  }

  std::vector<mpsxx::PointGroup> orbsym(irreps.size());
  for(size_t i = 0; i < irreps.size(); ++i) orbsym[i] = mpsxx::PointGroup(irreps[i]);

  time_stamp ts;

  if(!inp.restart) {
    //
    // generate exact MPOs
    //
    std::vector<int> rmdocc(Norbs,0); // this is not used.
    mpsxx::mpogen_impl(nprocs,inp.N_sites,Ecore,orbsym,rmdocc,oneint,twoint,"hmpo",pfix,true,true);

    inp.name = "hmpo";

    world.barrier();

    pout << endl;
    pout.precision(2);
    pout << "\t\t\tWall time for MPOs construction (1st step): " << setw(8) << fixed << ts.lap() << endl;

  }

  pout << "\tDEBUG :: generating random MPSs as an initial state..." << endl;
  fermion<mpsxx::PointGroup> qt(inp.N_elecs,inp.N_spins,mpsxx::PointGroup(inp.irrep));
  if(!inp.restart) mpsxx::make_random_mpss(inp.name,pfix,inp.N_sites,0,qt,inp.N_max_states);

  pout.precision(8);

  // REAL

  {

  pout << "\tDEUBG :: -- real object -- " << endl;
  pout << "\tDEBUG :: loading QC-MPOs from scratch..." << endl;
  mpsxx::MPSXX_MPO_array<double,fermion<mpsxx::PointGroup>> H;
  mpsxx::load_mpos(inp.N_sites,inp.name,H,pfix);

  pout << "\tDEBUG :: loading MPSs from scratch..." << endl;
  mpsxx::MPSXX_MPS_array<double,fermion<mpsxx::PointGroup>> C0;
  mpsxx::load_mpss(inp.N_sites,C0,0,pfix);

  pout << "\tDEBUG :: norm(C0) = " << mpsxx::dotc(C0,C0) << endl;
  pout << "\tDEBUG :: hdot(C0,H,C0) = " << mpsxx::hdot(C0,H,C0) << endl;
  pout << "\tDEBUG :: dotc(H,H) = " << mpsxx::dotc(H,H) << endl;

//for(size_t i = 0; i < inp.N_sites; ++i) {
//  pout << "\tDEBUG :: H[" << i << "] = " << endl << H[i] << endl;
//}


//double dt = inp.tau;
//mpsxx::scal(-dt,H); // -H*dt

//pout.precision(6);
//pout << "\tDEBUG :: computing exp(-Hdt)... ( dt = " << setw(10) << fixed << dt << " ) " << endl;
//mpsxx::MPSXX_MPO_array<double,fermion<mpsxx::PointGroup>> expH;
//mpsxx::muexp(H,expH,Np,Mw);

//mpsxx::MPSXX_MPO_array<double,fermion<mpsxx::PointGroup>> expH2;
//for(size_t d = 1; d < Nd; ++d) {
//  mpsxx::gemm(CblasNoTrans,CblasNoTrans,1.0,expH,expH,1.0,expH2,Mw);
//  expH.swap(expH2);
//  expH2.clear();
//}

//mpsxx::scal(-1.0/dt.real(),H); // back to H

//double norm;

  // Normalize C0
//norm = mpsxx::dotc(C0,C0);
//mpsxx::scal(1.0/sqrt(norm.real()),C0);

//pout << endl;
//pout.precision(2);
//pout << "\t\t\tWall time for computing exp(-Hdt): " << setw(8) << fixed << ts.lap() << " sec." << endl;

  }

  // complex

  {

  std::complex<double> ONE(1.0,0.0);

  pout << "\tDEUBG :: -- complex object -- " << endl;
  pout << "\tDEBUG :: loading QC-MPOs from scratch..." << endl;
  mpsxx::MPSXX_MPO_array<std::complex<double>,fermion<mpsxx::PointGroup>> H;
  mpsxx::load_mpos_complex(inp.N_sites,inp.name,H,pfix);

  pout << "\tDEBUG :: loading MPSs from scratch..." << endl;
  mpsxx::MPSXX_MPS_array<std::complex<double>,fermion<mpsxx::PointGroup>> C0;
  mpsxx::load_mpss_complex(inp.N_sites,C0,0,pfix);

  pout << "\tDEBUG :: norm(C0) = " << mpsxx::dotc(C0,C0) << endl;
  pout << "\tDEBUG :: hdot(C0,H,C0) = " << mpsxx::hdot(C0,H,C0) << endl;
  pout << "\tDEBUG :: dotc(H,H) = " << mpsxx::dotc(H,H) << endl;

//for(size_t i = 0; i < inp.N_sites; ++i) {
//  pout << "\tDEBUG :: H[" << i << "] = " << endl << H[i] << endl;
//}

//std::complex<double> dt = std::complex<double>(inp.tau,0.0);
//mpsxx::scal(-dt,H); // -H*dt

//pout.precision(6);
//pout << "\tDEBUG :: computing exp(-Hdt)... ( dt = " << setw(10) << fixed << dt << " ) " << endl;
//mpsxx::MPSXX_MPO_array<std::complex<double>,fermion<mpsxx::PointGroup>> expH;
//mpsxx::muexp(H,expH,Np,Mw);

//mpsxx::MPSXX_MPO_array<std::complex<double>,fermion<mpsxx::PointGroup>> expH2;
//for(size_t d = 1; d < Nd; ++d) {
//  mpsxx::gemm(CblasNoTrans,CblasNoTrans,ONE,expH,expH,ONE,expH2,Mw);
//  expH.swap(expH2);
//  expH2.clear();
//}

//mpsxx::scal(-ONE/dt.real(),H); // back to H

//std::complex<double> norm;

  // Normalize C0
//norm = mpsxx::dotc(C0,C0);
//mpsxx::scal(ONE/sqrt(norm.real()),C0);

//pout << "DEBUG :: norm(C0) = " << mpsxx::dotc(C0,C0) << endl;

//pout << endl;
//pout.precision(2);
//pout << "\t\t\tWall time for computing exp(-Hdt): " << setw(8) << fixed << ts.lap() << " sec." << endl;

  }

  cout.rdbuf(backup);
  ofst.close();

  return 0;
}

// --- --- --- ---

int main (int argc, char* argv[])
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

#ifndef _SERIAL
  boost::mpi::environment env(argc,argv);
#endif
  Communicator world;

  std::string finp = "dmrg.conf";
  std::string fout;
  std::string fdmp = "FCIDUMP";
  std::string fodr;
  std::string pfix = ".";

  size_t Nd = 1;
  size_t Np = 2;
  size_t Mw = 0;

  for(int iarg = 0; iarg < argc; ++iarg) {
    if(strcmp(argv[iarg],"-i") == 0) finp = argv[++iarg];
    if(strcmp(argv[iarg],"-o") == 0) fout = argv[++iarg];
    if(strcmp(argv[iarg],"-s") == 0) pfix = argv[++iarg];
    if(strcmp(argv[iarg],"-f") == 0) fdmp = argv[++iarg];
    if(strcmp(argv[iarg],"-r") == 0) fodr = argv[++iarg];
    if(strcmp(argv[iarg],"-m") == 0) Mw = atoi(argv[++iarg]);
    if(strcmp(argv[iarg],"-d") == 0) Nd = atoi(argv[++iarg]);
    if(strcmp(argv[iarg],"-p") == 0) Np = atoi(argv[++iarg]);
  }

  return tdmrg(finp,fout,fdmp,fodr,pfix,Nd,Np,Mw);
}
