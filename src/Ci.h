#ifndef __MPSXX_POINT_GROUP_CI_H
#define __MPSXX_POINT_GROUP_CI_H

#include <string>

#include "point_group.h"

namespace mpsxx {
namespace PG {

struct Ci {

  /// irrep #, the same as Molpro order.
  enum IRREP_ELEMENTS {
    Ag = 0,
    Au = 1,
    ER = 0xffffffff
  };

  static void set (size_t& N_rep, std::vector<IRREP>& table, std::vector<std::string>& label)
  {
    N_rep = 2;

    table = {
      Ag, Au,
      Au, Ag
    };

    label.resize(N_rep);
    label[0] = "Ag";
    label[1] = "Au";
  }

};

} // namespace PG
} // namespace mpsxx

#endif // __MPSXX_POINT_GROUP_CI_H
