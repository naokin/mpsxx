#ifndef __MPSXX_POINT_GROUP_C2_H
#define __MPSXX_POINT_GROUP_C2_H

#include <string>

#include "point_group.h"

namespace mpsxx {
namespace PG {

struct C2 {

  /// irrep #, the same as Molpro order.
  enum IRREP_ELEMENTS {
    A  = 0,
    B  = 1,
    ER = 0xffffffff
  };

  static void set (size_t& N_rep, std::vector<IRREP>& table, std::vector<std::string>& label)
  {
    N_rep = 2;

    table = {
      A, B,
      B, A
    };

    label.resize(N_rep);
    label[0] = "A";
    label[1] = "B";
  }

};

} // namespace PG
} // namespace mpsxx

#endif // __MPSXX_POINT_GROUP_C2_H
