#ifndef __MPSXX_POINT_GROUP_D2H_H
#define __MPSXX_POINT_GROUP_D2H_H

#include <string>

#include "point_group.h"

namespace mpsxx {
namespace PG {

struct D2h {

  /// irrep #, the same as Molpro order.
  enum IRREP_ELEMENTS {
    Ag  = 0,
    B3u = 1,
    B2u = 2,
    B1g = 3,
    B1u = 4,
    B2g = 5,
    B3g = 6,
    Au  = 7,
    ER = 0xffffffff
  };

  static void set (size_t& N_rep, std::vector<IRREP>& table, std::vector<std::string>& label)
  {
    N_rep = 8;

    table = {
      Ag , B3u, B2u, B1g, B1u, B2g, B3g, Au ,
      B3u, Ag , B1g, B2u, B2g, B1u, Au , B3g,
      B2u, B1g, Ag , B3u, B3g, Au , B1u, B2g,
      B1g, B2u, B3u, Ag , Au , B3g, B2g, B1u,
      B1u, B2g, B3g, Au , Ag , B3u, B2u, B1g,
      B2g, B1u, Au , B3g, B3u, Ag , B1g, B2u,
      B3g, Au , B1u, B2g, B2u, B1g, Ag , B3u,
      Au , B3g, B2g, B1u, B1g, B2u, B3u, Ag
    };

    label.resize(N_rep);
    label[0] = "Ag";
    label[1] = "B3u";
    label[2] = "B2u";
    label[3] = "B1g";
    label[4] = "B1u";
    label[5] = "B2g";
    label[6] = "B3g";
    label[7] = "Au";
  }

};

} // namespace PG
} // namespace mpsxx

#endif // __MPSXX_POINT_GROUP_D2H_H
