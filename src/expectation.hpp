#ifndef __MPSXX_EXPECTATION_HPP
#define __MPSXX_EXPECTATION_HPP

#include <iostream>
#include <iomanip>

#include <btas/QSPARSE/QSTArray.h>

#include "mpidefs.h"
#include "renormalize.hpp"
#include "fileio.h"

namespace mpsxx {

/// Compute overlap b/w two different states <i|j>
template<class Q>
double overlap (
  const size_t& N,
  const size_t& iroot,
  const size_t& jroot,
  const std::string& prefix = ".")
{
  using std::cout;
  using std::endl;
  using std::setw;

  btas::QSTArray<double,3,Q> mpsi;
  btas::QSTArray<double,3,Q> mpsj;
  btas::QSTArray<double,2,Q> lSij;

  btas::Qshapes<Q> qz(1,Q::zero());
  lSij.resize(Q::zero(),btas::make_array(qz,qz));
  lSij.insert(btas::shape(0ul,0ul),btas::TArray<double,2>(1,1));
  lSij.fill(1.0);

  for(size_t i = 0; i < N-1; ++i) {
    load(mpsi,getfile("lmps",prefix,i,iroot));
    load(mpsj,getfile("lmps",prefix,i,jroot));
    btas::QSTArray<double,2,Q> lSijTmp;
    renormalize(1,lSij,mpsi,mpsj,lSijTmp);
    lSij = lSijTmp;
  }
    load(mpsi,getfile("wave",prefix,N-1,iroot));
    load(mpsj,getfile("wave",prefix,N-1,jroot));
    btas::QSTArray<double,2,Q> tSij;
    renormalize(1,lSij,mpsi,mpsj,tSij);

  return tSij.find(0)->second->data()[0];
}

/// Compute expectation value <i|O|j>
template<class Q>
double expectation (
  const std::string& name,
  const size_t& N,
  const size_t& iroot,
  const size_t& jroot,
  const std::string& prefix = ".")
{
  using std::cout;
  using std::endl;
  using std::setw;

  Communicator world;

  std::vector<btas::QSTArray<double,4,Q>> mpo;
  std::vector<btas::QSTArray<double,3,Q>> lHij;
  btas::QSTArray<double,3,Q> mpsi;
  btas::QSTArray<double,3,Q> mpsj;

  btas::Qshapes<Q> qz(1,Q::zero());

  size_t Ng = 0;

  for(size_t i = 0; i < N-1; ++i) {

    load(mpo, getfile(name,prefix,i));
    load(mpsi,getfile("lmps",prefix,i,iroot));
    load(mpsj,getfile("lmps",prefix,i,jroot));

    if(i == 0) {
      Ng = mpo.size();
      lHij.resize(Ng);
      for(size_t g = 0; g < Ng; ++g) {
        lHij[g].resize(Q::zero(),btas::make_array(qz,qz,qz));
        lHij[g].insert(btas::shape(0ul,0ul,0ul),btas::TArray<double,3>(1,1,1));
        lHij[g].fill(1.0);
      }
    }
    else {
      assert(mpo.size() == Ng);
    }

    for(size_t g = 0; g < Ng; ++g) {
      btas::QSTArray<double,3,Q> lHijTmp;
      renormalize(1,mpo[g],lHij[g],mpsi,mpsj,lHijTmp);
      lHij[g] = lHijTmp;
    }
  }
    load(mpo, getfile(name,prefix,N-1));
    load(mpsi,getfile("wave",prefix,N-1,iroot));
    load(mpsj,getfile("wave",prefix,N-1,jroot));
    assert(mpo.size() == Ng);

    btas::QSTArray<double,3,Q> tHij;
    for(size_t g = 0; g < Ng; ++g) {
      renormalize(1,mpo[g],lHij[g],mpsi,mpsj,tHij);
    }

#ifndef _SERIAL
  double local = tHij.find(0)->second->data()[0];
  double value = 0.0;
  boost::mpi::all_reduce(world,local,value,std::plus<double>());
#else
  double value = tHij.find(0)->second->data()[0];
#endif

  return value;
}

} // namespace mpsxx

#endif // __MPSXX_EXPECTATION_HPP
