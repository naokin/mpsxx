#include <iomanip>
#include <fstream>

#include "mpidefs.h"
#include "input.h"

void mpsxx::DMRGInput::parse (const std::string& fname)
{
  if(fname.size() == 0) return;

  Communicator world;

  if(world.rank() == 0) {
    std::ifstream fin(fname.c_str());
    std::string entry;
    while(fin >> entry) {
      if(entry == "restart")
        this->restart = true;
      if(entry == "load_mpos")
        this->load_mpos = true;
      if(entry == "symmetry" || entry == "symm" || entry == "sym")
        fin >> this->symmetry;
      if(entry == "spin")
        fin >> this->N_spins;
      if(entry == "elec")
        fin >> this->N_elecs;
      if(entry == "irrep") {
        size_t itmp; fin >> itmp;
        assert(itmp > 0);
        this->irrep = itmp-1;
      }
      if(entry == "ionic") {
        size_t itmp; fin >> itmp;
        assert(itmp > 0);
        this->IonCore = itmp-1;
      }
      if(entry == "N" || entry == "sites")
        fin >> this->N_sites;
      if(entry == "M" || entry == "max_states")
        fin >> this->N_max_states;
      if(entry == "nroots")
        fin >> this->N_roots;
      if(entry == "tole" || entry == "tolerance")
        fin >> this->tolerance;
      if(entry == "noise")
        fin >> this->noise;
      if(entry == "shift")
        fin >> this->shift;
      if(entry == "tau")
        fin >> this->tau;
      if(entry == "onesite" || entry == "onedot")
        this->algorithm = 1;
      if(entry == "twosite" || entry == "twodot")
        this->algorithm = 2;
      if(entry == "maxiter")
        fin >> this->N_max_sweep_iter;
    }
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,*this,0);
#endif
}

std::ostream& operator<< (std::ostream& ost, const mpsxx::DMRGInput& input)
{
  using std::endl;
  using std::setw;
  using std::boolalpha;

  ost << "\t\tRestart : " << boolalpha << input.restart << endl;
  ost << "\t\tMPOs    : " << (input.load_mpos ? "LOAD" : "MAKE") << endl;
  ost << "\t\t# sites : " << setw(4) << input.N_sites << endl;
  ost << "\t\t# elecs : " << setw(4) << input.N_elecs << endl;
  ost << "\t\t# spins : " << setw(4) << input.N_spins << endl;
  ost << "\t\tPG Symm : " << input.symmetry << " (irrep = " << input.irrep << ")" << endl;
  ost << "\t\tM       : " << setw(4) << input.N_max_states << endl;
  ost << "\t\t# roots : " << setw(4) << input.N_roots << endl;
  for(size_t i = 0; i < input.energy.size(); ++i) {
  ost << "\t\tE[" << setw(2) << i << "]  : " << input.energy[i] << endl;
  }

  return ost;
}

