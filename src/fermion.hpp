#ifndef __MPSXX_FERMION_WITH_SYMMETRY_HPP
#define __MPSXX_FERMION_WITH_SYMMETRY_HPP

#include <iostream>
#include <iomanip>
#include <vector>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/base_object.hpp>

/// A model quantum number class to provide the concepts of quantum/symmetry class
template<class Symmetry>
class fermion {

private:

  friend class boost::serialization::access;

  template <class Archive>
  void serialize(Archive& ar, const unsigned int version)
  { ar & p_ & s_ & base_; }

public:

  static fermion zero () { return fermion(0,0,Symmetry::zero()); }

  fermion ()
  : p_(0), s_(0), base_() { }

  fermion (const int& p, const int& s, const Symmetry& base = Symmetry::zero())
  : p_(p), s_(s), base_(base) { }

  fermion (const fermion& x)
  : p_(x.p_), s_(x.s_), base_(x.base_) { }

// FIXME: In C++11, copy assignment should be implemented with move assignment?
//        For the time, I used implicit copy assignment.
//fermion& operator= (const fermion& x)
//{
//  p_ = x.p_;
//  s_ = x.s_;
//  base_ = x.base_;
//  return *this;
//}

  fermion& operator*= (const fermion& x)
  {
    p_ += x.p_;
    s_ += x.s_;
    base_ *= x.base_;
    return *this;
  }

  fermion& operator+= (const fermion& x)
  {
    p_ += x.p_;
    s_ += x.s_;
    base_ *= x.base_;
    return *this;
  }

  fermion& operator-= (const fermion& x)
  {
    p_ -= x.p_;
    s_ -= x.s_;
    base_ *= x.base_;
    return *this;
  }

  /// test whether x is a part of this
  /// 'involve' function has different meaning from 'equal' operator for non-abelian case
  bool involve (const fermion& x) const
  { return (p_ == x.p_ && s_ == x.s_ && this->base() == x.base()); }

  fermion conj () const { return fermion(-p_,-s_,base_); }

  fermion operator+ () const { return fermion(+p_,+s_,base_); }

  fermion operator- () const { return fermion(-p_,-s_,base_); }

  const int& p () const { return p_; }

  const int& s () const { return s_; }

  const Symmetry& base () const { return base_; }

  bool parity () const { return (p_ & 1); }

private:

  int p_; ///< particle quantum number

  int s_; ///< spin quantum number

  Symmetry base_; ///< base symmetry (e.g. point group)

}; // class fermion

template<class Symmetry>
inline fermion<Symmetry> operator* (const fermion<Symmetry>& x, const fermion<Symmetry>& y)
{ fermion<Symmetry> t(x); t *= y; return t; }

template<class Symmetry>
inline fermion<Symmetry> operator+ (const fermion<Symmetry>& x, const fermion<Symmetry>& y)
{ fermion<Symmetry> t(x); t += y; return t; }

template<class Symmetry>
inline fermion<Symmetry> operator- (const fermion<Symmetry>& x, const fermion<Symmetry>& y)
{ fermion<Symmetry> t(x); t -= y; return t; }

template<class Symmetry>
inline bool operator== (const fermion<Symmetry>& x, const fermion<Symmetry>& y)
{ return (x.base() == y.base() && x.p() == y.p() && x.s() == y.s()); }

template<class Symmetry>
inline bool operator!= (const fermion<Symmetry>& x, const fermion<Symmetry>& y)
{ return (x.base() != y.base() || x.p() != y.p() || x.s() != y.s()); }

template<class Symmetry>
inline bool operator<  (const fermion<Symmetry>& x, const fermion<Symmetry>& y)
{
  if(x.base() == y.base())
    return (x.p() == y.p()) ? (x.s() < y.s()) : (x.p() < y.p());
  else
    return x.base() < y.base();
}

template<class Symmetry>
inline bool operator>  (const fermion<Symmetry>& x, const fermion<Symmetry>& y)
{
  if(x.base() == y.base())
    return (x.p() == y.p()) ? (x.s() > y.s()) : (x.p() > y.p());
  else
    return x.base() > y.base();
}

template<class Symmetry>
inline std::ostream& operator<< (std::ostream& ost, const fermion<Symmetry>& x)
{ return ost << "{" << x.base() << ":" << x.p() << ":" << x.s() << "}"; }

#endif // __MPSXX_FERMION_WITH_SYMMETRY_HPP
