#ifndef __MPSXX_MPOGEN_OP_PRODUCT_H
#define __MPSXX_MPOGEN_OP_PRODUCT_H

#include <vector>

#include "bit_operator_type.h"

namespace mpsxx {
namespace mpogen {

std::vector<BIT_OPERATOR_TYPE> op_product (
  const BIT_OPERATOR_TYPE& l_op,
  const BIT_OPERATOR_TYPE& s_op,
  const std::vector<size_t>& r_indxs,
        bool swap_sweep_dir = false);

} // namespace mpogen
} // namespace mpsxx

#endif // __MPSXX_MPOGEN_OP_PRODUCT_H
