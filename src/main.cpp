#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

#include <btas/DENSE/TArray.h>

#include <time_stamp.h>

#include "mpidefs.h" // Communicator

#include "input.h"
#include "dmrg.hpp"
#include "mpsgen_impl.hpp"
#include "mpogen_impl.hpp"
#include "mpogen_kmpos_impl.hpp"
#include "fermion.hpp"
#include "point_group.h"
#include "parsing_integral.h"

void print_help ()
{
  std::cout << "\t-i : followed by input file name" << std::endl;
  std::cout << "\t-o : followed by output file name (by default, use standard output)" << std::endl;
  std::cout << "\t-f : followed by integral file name (by default, FCIDUMP)" << std::endl;
  std::cout << "\t-s : followed by file prefix (by default, current directory)" << std::endl;
  std::cout << "\t-r : followed by reordering file" << std::endl;
  std::cout << "\t-n : followed by number of division of MPOs (by default, number of procs)" << std::endl;
  std::cout << "\t   : if 0 is given, k-MPOs are generated." << std::endl;
  std::cout << "\t-x : to enable sweep swap in MPOs construction" << std::endl;
  std::cout << "\t-c : to enable MPOs compression" << std::endl;
  std::cout << "\t-mpogen" << std::endl;
  std::cout << "\t   : program will terminate after MPO construction" << std::endl;
  std::cout << "\t-v : verbose print" << std::endl;
  return;
}

int main (int argc, char* argv[])
{
  using std::cout;
  using std::endl;
  using std::setw;
  using std::fixed;

#ifndef _SERIAL
  boost::mpi::environment env(argc,argv);
#endif
  Communicator world;

  std::string fin = "dmrg.conf";
  std::string fout;
  std::string fdump = "FCIDUMP";
  std::string fordr;

  std::string prefx = ".";

  bool enable_swap_sweep = false;
  bool do_compress = false;
  bool mpogen_only = false;

  size_t divide = world.size();
  size_t knmpos = 0;

  size_t iprint = 0;

  for(int iarg = 0; iarg < argc; ++iarg) {
    if(strcmp(argv[iarg],"-i") == 0) fin = argv[++iarg];
    if(strcmp(argv[iarg],"-o") == 0) fout = argv[++iarg];
    if(strcmp(argv[iarg],"-s") == 0) prefx = argv[++iarg];
    if(strcmp(argv[iarg],"-f") == 0) fdump = argv[++iarg];
    if(strcmp(argv[iarg],"-r") == 0) fordr = argv[++iarg];
    if(strcmp(argv[iarg],"-x") == 0) enable_swap_sweep = true;
    if(strcmp(argv[iarg],"-c") == 0) do_compress = true;
    if(strcmp(argv[iarg],"-v") == 0) iprint = 1;
    if(strcmp(argv[iarg],"-n") == 0) divide = atoi(argv[++iarg]);
    if(strcmp(argv[iarg],"-k") == 0) knmpos = atoi(argv[++iarg]);
    if(strcmp(argv[iarg],"-mpogen") == 0) mpogen_only = true;

    if(strcmp(argv[iarg],"--help") == 0) { print_help(); return 0; }
  }

  mpsxx::DMRGInput input(fin);

  //
  // assign cout as alias to ofst
  //
  std::streambuf *backup;
  backup = cout.rdbuf();
  std::ofstream ofst;
  if(fout.size() > 0) {
    std::ostringstream oss;
    oss << fout << "." << world.rank();
    ofst.open(oss.str().c_str());
    cout.rdbuf(ofst.rdbuf());
  }

  int Norbs;
  int Nelec;
  double Ecore;
  std::vector<int> irreps;
  btas::TArray<double,2> oneint;
  btas::TArray<double,4> twoint;

  pout << "\t****************************************************************************************************" << endl;
  pout << "\t\t\t\tMPSXX::PROTOTYPE::MPO GENERATOR FOR QC "                                                        << endl;
  pout << "\t****************************************************************************************************" << endl;
  pout << endl;
  pout << "\t====================================================================================================" << endl;
  pout << "\t\tLoading integral information from FCIDUMP "                                                         << endl;
  pout << "\t====================================================================================================" << endl;
  pout << endl;

  std::vector<int> reorder;
  if(world.rank() == 0) {
    std::ifstream ifst_dump(fdump.c_str());
    if(!fordr.empty()) {
      std::ifstream ifst_ordr(fordr.c_str());
      parsing_reorder(ifst_ordr,reorder,input.IonCore);
      parsing_fcidump(ifst_dump,Norbs,Nelec,Ecore,irreps,oneint,twoint,reorder);
    }
    else {
      parsing_fcidump(ifst_dump,Norbs,Nelec,Ecore,irreps,oneint,twoint);
    }
  }
#ifndef _SERIAL
  boost::mpi::broadcast(world,Norbs,0);
  boost::mpi::broadcast(world,Nelec,0);
  boost::mpi::broadcast(world,Ecore,0);
  boost::mpi::broadcast(world,irreps,0);
  boost::mpi::broadcast(world,oneint,0);
  boost::mpi::broadcast(world,twoint,0);
  boost::mpi::broadcast(world,reorder,0);
#endif

//if(reorder.size() > 0) {
//  if(input.IonCore >= 0 && input.IonCore < Norbs)
//    input.IonCore = reorder[input.IonCore];
//}

  input.N_sites = Norbs;

  pout << "\t====================================================================================================" << endl;
  pout << "\t\tGenerating QC MPOs from 1- and 2-particle integrals"                                                << endl;
  pout << "\t====================================================================================================" << endl;
  pout << endl;

  // Set symmetry
       if(input.symmetry == "D2h") { mpsxx::PointGroup::set(mpsxx::_D2h); }
  else if(input.symmetry == "C2v") { mpsxx::PointGroup::set(mpsxx::_C2v); }
  else if(input.symmetry == "C2h") { mpsxx::PointGroup::set(mpsxx::_C2h); }
  else if(input.symmetry == "D2" ) { mpsxx::PointGroup::set(mpsxx::_D2 ); }
  else if(input.symmetry == "Cs" ) { mpsxx::PointGroup::set(mpsxx::_Cs ); }
  else if(input.symmetry == "C2" ) { mpsxx::PointGroup::set(mpsxx::_C2 ); }
  else if(input.symmetry == "Ci" ) { mpsxx::PointGroup::set(mpsxx::_Ci ); }
  else if(input.symmetry == "C1" ) { mpsxx::PointGroup::set(mpsxx::_C1 ); }
  else {
    pout << "Requested symmetry [" << input.symmetry << "] is not supported." << endl;
    abort();
  }

  std::vector<mpsxx::PointGroup> orbsym(irreps.size());
  for(size_t i = 0; i < irreps.size(); ++i) orbsym[i] = mpsxx::PointGroup(irreps[i]);

  std::vector<int> rmdocc(Norbs,0);
//if(input.IonCore >= 0 && input.IonCore < Norbs)
//  rmdocc[input.IonCore] = 1;

  time_stamp ts;

  if(mpogen_only || !(input.restart || input.load_mpos)) {
    if(knmpos == 0 && divide > 0) {
      mpsxx::mpogen_impl(divide,Norbs,Ecore,orbsym,rmdocc,oneint,twoint,"mpo",prefx,enable_swap_sweep,false,do_compress);
    }
    else {
      if     (knmpos == 1)
        mpsxx::mpogen_kmpos_impl<1>(Norbs,Ecore,orbsym,rmdocc,oneint,twoint,"mpo",prefx,enable_swap_sweep,false,do_compress);
      else if(knmpos == 2)
        mpsxx::mpogen_kmpos_impl<2>(Norbs,Ecore,orbsym,rmdocc,oneint,twoint,"mpo",prefx,enable_swap_sweep,false,do_compress);
      else
        abort();
    }

    world.barrier();

    pout << endl;
    pout.precision(2);
    pout << "\t\t\tWall time for MPOs construction: " << setw(8) << fixed << ts.lap() << endl;
  }

  if(!mpogen_only) {

  input.name = "mpo";

  fermion<mpsxx::PointGroup> qt(input.N_elecs,input.N_spins,mpsxx::PointGroup(input.irrep));

  // Calc. occupation guess
  std::vector<fermion<mpsxx::PointGroup>> guess(orbsym.size(),fermion<mpsxx::PointGroup>::zero());
  {
    std::multimap<double,size_t> orbmap;
    for(size_t i = 0; i < orbsym.size(); ++i)
      orbmap.insert(std::make_pair(oneint(i,i),i));
    //
    size_t N_docc = (input.N_elecs-input.N_spins)/2;
    size_t N_socc =  input.N_spins;
    //
    auto it = orbmap.begin();
    // doubly occupied
    for(size_t i = 0; i < N_docc; ++i, ++it)
      guess[it->second] = fermion<mpsxx::PointGroup>(2,0,mpsxx::PointGroup::zero());
    // singly occupied
    for(size_t i = 0; i < N_socc; ++i, ++it)
      guess[it->second] = fermion<mpsxx::PointGroup>(1,1,orbsym[it->second]);
  }

  // Build initial MPSs
  if(!input.restart) {
    for(size_t iroot = 0; iroot < input.N_roots; ++iroot) {
      mpsxx::mpsgen_impl(input.name,prefx,input.N_sites,iroot,qt,guess,input.N_max_states);
    }
  }

  //
  // dmrg optimization
  //
  input.energy = mpsxx::dmrg<fermion<mpsxx::PointGroup>>(
    input.name,
    input.restart,
    qt,
    input.algorithm,
    input.N_sites,
    input.N_max_states,
    prefx,
    input.N_roots,
    input.tolerance,
    input.noise,
    input.shift,
    input.N_max_sweep_iter
  );

  world.barrier();

  pout << endl;
  pout.precision(2);
  pout << "\t\t\tWall time for DMRG optimization: " << setw(8) << fixed << ts.lap() << endl;
  pout << "\t\t\tWall time for Total calculation: " << setw(8) << fixed << ts.elapsed() << endl;

  } // !mpogen_only

  cout.rdbuf(backup);
  ofst.close();

  return 0;
}
