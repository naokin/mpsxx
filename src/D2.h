#ifndef __MPSXX_POINT_GROUP_D2_H
#define __MPSXX_POINT_GROUP_D2_H

#include <string>

#include "point_group.h"

namespace mpsxx {
namespace PG {

struct D2 {

  /// irrep #, the same as Molpro order.
  enum IRREP_ELEMENTS {
    A1 = 0,
    B3 = 1,
    B2 = 2,
    B1 = 3,
    ER = 0xffffffff
  };

  static void set (size_t& N_rep, std::vector<IRREP>& table, std::vector<std::string>& label)
  {
    N_rep = 4;

    table = {
      A1, B3, B2, B1,
      B3, A1, B1, B2,
      B2, B1, A1, B3,
      B1, B2, B3, A1
    };

    label.resize(N_rep);
    label[0] = "A1";
    label[1] = "B3";
    label[2] = "B2";
    label[3] = "B1";
  }

};

} // namespace PG
} // namespace mpsxx

#endif // __MPSXX_POINT_GROUP_D2_H
